def skus = []

Map product = api.product()
if (api.isDebugMode()) {
    product = api.find("P", Filter.equal("sku", "B00099"))?.find()
}
def markets = product?.attribute5.replaceAll(']', "").substring(1)
List marketList = markets?.tokenize(", ")

if (!api.global.rawMaterialData) {
    api.global.rawMaterialData = api.stream(
            "PX20",
            "sku",
            ["sku", "attribute1", "attribute3", "attribute4", "attribute8", "attribute9"],
            Filter.equal("name", "RawMaterialPrice")
    )?.withCloseable { it.collect() }?.groupBy { it.sku }
}


def defaultOptions = api.find(
        "PX20",
        0,
        api.getMaxFindResultsLimit(),
        "attribute7",
        ["attribute7"],
        Filter.equal("name", "DefaultOptions"),
        Filter.equal("sku", product?.sku),
        Filter.equal("attribute3", "external"),
        Filter.like("attribute5", "%material%")
)


List defaultMaterial = defaultOptions?.attribute7

api.global.rawMaterialData?.subMap(defaultMaterial)?.each { String sku, List data ->
    data?.each { it ->
        for (market in marketList) {
            def SecondaryKey = market + "|" + sku + "|" + it.attribute1 + "|" + it.attribute3 + "|" + it.attribute4 + "|" + it.attribute8 + "|" + it.attribute9
            skus?.add(SecondaryKey)
        }
    }
}

return skus