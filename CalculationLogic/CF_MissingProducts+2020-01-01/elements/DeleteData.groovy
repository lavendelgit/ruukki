api.global.tableid = api.findLookupTable("MissingProducts")?.getAt("id")
api.massDelete("MLTV2", Filter.equal("lookupTable.id", api.global.tableid))