Filter filters = Filter.and(Filter.or(
        Filter.like("key1", "L%")
        , Filter.like("key1", "C%")
       ),
        Filter.equal("key4","ComponentProductPrices"))

def missingSkus = api.findLookupTableValues("MissingData",filters)
def result = missingSkus?.collect{ it ->
       return [
               "lookupTableId"  : api.global.tableid,
               "lookupTableName": "MissingProducts",
               "key1"           : it.key1,
               "key2"           : it.key2,
               "attribute1" : it.attribute2,
               "attribute2" : api.product("Type",it.key1)
       ]
}

if(result) {
       api.addOrUpdate("MLTV2", result)
}
return
