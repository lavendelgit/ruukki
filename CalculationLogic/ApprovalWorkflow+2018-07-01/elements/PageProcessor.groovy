import groovy.transform.Field

@Field final String DEFAULT_INSTANCE_NAME = 'defaultPageProcessor'

def setInstanceName(String name) {
    api.global.pageProcessorInstance = name

    return this
}

def getInstanceName() {
    return api.global.pageProcessorInstance ?: DEFAULT_INSTANCE_NAME
}

def processPages(def pageProcessor = null) {
    def paging = libs.ApprovalWorkflow.Paging
    pageProcessor = pageProcessor ?: getPageProcessor()
    List pageData = paging.next()

    while (pageData && shouldProcessPage()) {
        if (pageProcessor instanceof Closure) {
            pageProcessor(pageData)
        } else if (pageProcessor instanceof String) {
            libs.ApprovalWorkflow.ConfigurationManager.runScript(pageProcessor, pageData)
        }

        pageData = paging.next()
    }
}

String getPageProcessorKey() {
    return "pageProcessor_${getInstanceName()}"
}

def setPageProcessor(def pageProcessor) {
    String key = getPageProcessorKey()
    api.global[key] = pageProcessor

    return this
}

def getPageProcessor() {
    return api.global[getPageProcessorKey()]
}

String getPageProcessorFromPageKey() {
    return "pageProcessorFromPage_${getInstanceName()}"
}

String getPageProcessorToPageKey() {
    return "pageProcessorToPage${getInstanceName()}"
}

def setProcessingPages(Integer fromPage = 0, Integer toPage = null) {
    String fromKey = getPageProcessorFromPageKey()
    String toKey = getPageProcessorToPageKey()
    api.global[fromKey] = fromPage
    api.global[toKey] = toPage

    return this
}

boolean shouldProcessPage() {
    def paging = libs.ApprovalWorkflow.Paging

    Integer currentPage = paging.getCurrentPageIndex()
    Integer fromPage = getProcessFromPage()
    Integer toPage = getProcessToPage() ?: currentPage

    return fromPage <= currentPage && toPage >= currentPage
}

Integer getProcessFromPage() {
    return api.global[getPageProcessorFromPageKey()] ?: 0
}

Integer getProcessToPage() {
    return api.global[getPageProcessorToPageKey()]
}