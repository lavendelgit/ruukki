Object customerGroup(Map lookupConfig, Map approvable) {
    return getByConfig(lookupConfig, approvable)?.customerFieldValue
}

Object productGroup(Map lookupConfig, Map approvable) {
    return getByConfig(lookupConfig, approvable)?.productFieldValue
}

Object customer(Map lookupConfig, Map approvable) {
    String attribute = getLookupValueFromConfig(lookupConfig)
    return api.customer(attribute, approvable.customerId as String)
}

Object product(Map lookupConfig, Map approvable) {
    String attribute = getLookupValueFromConfig(lookupConfig)
    return api.product(attribute, approvable.sku as String)
}

private getByConfig(Map lookupConfig, Map approvable) {
    String attribute = getLookupValueFromConfig(lookupConfig)
    return approvable[attribute]
}

private getLookupValueFromConfig(Map lookupConfig) {
    Map columns = libs.ApprovalWorkflow.ConfigurationManager.TABLE_CONFIG.lookupConfigTable.columns
    return lookupConfig[columns.lookupValue]
}