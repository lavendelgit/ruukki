import groovy.transform.Field

@Field final int DEFAULT_MAX_ITEMS_PER_PAGE = 200
@Field final String DEFAULT_INSTANCE_NAME = 'defaultPagingInstance'

/**
 *  Attempt to load data of the next page using data provider
 *  It simplify trigger data provider with the page index and number of items per page as parameters
 *  This method will not throw exception in case of data returned by data provider is null,
 *  or do any checking if the last page is reach. Checking should be done in page processor implementation
 *
 * @return List of next page's data returned by data provider, or empty list.
 * */
def setInstanceName(String name) {
    api.global.pagingDataProviderInstance = name

    return this
}

def getInstanceName() {
    return api.global.pagingDataProviderInstance ?: DEFAULT_INSTANCE_NAME
}

List next() {
    def dataProvider = getDataProvider()
    Integer nextPage = getCurrentPageIndex() + 1
    Integer numberOfItemsPerPage = getNumberOfItemsPerPage()
    List data

    if (dataProvider instanceof Closure) {
        data = dataProvider(nextPage, numberOfItemsPerPage)
    } else if (dataProvider instanceof String) {
        data = libs.ApprovalWorkflow.ConfigurationManager.runScript(dataProvider, nextPage, numberOfItemsPerPage)
    }

    if (data) {
        setCurrentPageIndex(nextPage)
    }

    return data ?: []
}

String getPagingProviderKey() {
    return "pagingDataProvider_${getInstanceName()}"
}

/**
 * Set data provider using to load data in page
 * @param dataProvider Closure or path of groovy library in form 'libs.Library.Element.function'.
 *          Data provider function should accept 2 parameters : page index to load and number of items per page expected
 * */
def setDataProvider(def dataProvider) {
    String key = getPagingProviderKey()
    api.global[key] = dataProvider

    return this
}

def getDataProvider() {
    return api.global[getPagingProviderKey()]
}

String getPagingDataProviderMaxItemsPerPageKey() {
    return "pagingDataProviderMaxItemsPerPage_${getInstanceName()}"
}

def setNumberOfItemsPerPage(Integer maxItem) {
    String key = getPagingDataProviderMaxItemsPerPageKey()
    api.global[key] = maxItem

    return this
}

Integer getNumberOfItemsPerPage() {
    return api.global[getPagingDataProviderMaxItemsPerPageKey()] ?: DEFAULT_MAX_ITEMS_PER_PAGE
}

String getPagingDataProviderCurrentPageIndexKey() {
    return "pagingDataProviderCurrentPageIndex_${getInstanceName()}"
}

def setCurrentPageIndex(Integer currentPageIndex) {
    String key = getPagingDataProviderCurrentPageIndexKey()
    api.global[key] = currentPageIndex

    return this
}

Integer getCurrentPageIndex() {
    Integer current = api.global[getPagingDataProviderCurrentPageIndexKey()]

    return current >= 0 ? current : -1
}