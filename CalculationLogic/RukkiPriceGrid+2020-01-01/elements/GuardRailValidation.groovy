Map GAURDRAIL_FLAGS = [
        RED   : "images/library/Traffic_red.png",
        YELLOW: "images/library/Traffic_yellow.png",
        GREEN : "images/library/Traffic_green.png"
]
String gaurdRailFlag = 'GREEN'

if (out.NewStartPrice == null || out.NewTargetPrice == null
        || out.NewFloorPrice == null
        || out.NewBasePrice == null) {
    gaurdRailFlag = 'RED'
} else if (isInRedZone("BasePriceDeltaPercent", out.BasePriceDeltaPercent)
        || isInRedZone("FloorPriceDeltaPercent", out.FloorPriceDeltaPercent)
        || isInRedZone("TargetPriceDeltaPercent", out.TargetPriceDeltaPercent)
        || isInRedZone("StartPriceDeltaPercent", out.StartPriceDeltaPercent)) {
    gaurdRailFlag = 'RED'
} else if (isInYellowZone("BasePriceDeltaPercent", out.BasePriceDeltaPercent)
        || isInYellowZone("FloorPriceDeltaPercent", out.FloorPriceDeltaPercent)
        || isInYellowZone("TargetPriceDeltaPercent", out.TargetPriceDeltaPercent)
        || isInYellowZone("StartPriceDeltaPercent", out.StartPriceDeltaPercent)) {
    gaurdRailFlag = 'YELLOW'
}

return api.attributedResult("").withRawCSS("background-image:url(\"${GAURDRAIL_FLAGS[gaurdRailFlag]}\");background-repeat: no-repeat;background-position:center")


boolean isInYellowZone(String pricePoint, BigDecimal deltaPercent) {
    return deltaPercent < getGaurdRailPercent(pricePoint, "Green") && deltaPercent >= getGaurdRailPercent(pricePoint, "Orange")
}

boolean isInRedZone(String pricePoint, BigDecimal deltaPercent) {
    return deltaPercent < getGaurdRailPercent(pricePoint, "Orange")
}

BigDecimal getGaurdRailPercent(String pricePoint, String color) {
    return libs.SharedLib.CacheUtils.getOrSet("GAURDRAIL_ENTRIES", {
        return api.namedEntities(api.findLookupTableValues("GaurdRailConfig")).collectEntries { Map row ->
            [(row.pricepoint + "." + row.Color): row.delta_percent]
        }
    })?.getAt(pricePoint + "." + color) as BigDecimal
}