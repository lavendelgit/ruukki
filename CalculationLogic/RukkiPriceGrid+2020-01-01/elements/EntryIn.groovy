if (out.HasMissingPrices == "Y") {
    //If no prices but market entry available
    return "PRODUCT MASTER"
}

String marketStr = api.product("attribute5")
List markets = marketStr ? libs.RukkiCommonLib.CommonUtils.marketStringToList(marketStr) : []
if (markets.indexOf(out.Market) == -1) {
    return "PRODUCT PRICES"
}


return "OK"