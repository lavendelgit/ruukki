import net.pricefx.server.dto.calculation.AttributedResult

return decorateSKUCell(out.sku)

AttributedResult decorateSKUCell(String sku) {
    String bgColor = "#2F7ED8"
    if (out.EntryIn == "PRODUCT MASTER") {
        bgColor = "#FF9900"
    } else if (out.EntryIn == "PRODUCT PRICES") {
        bgColor = "#C7B805"
    }

    return api.attributedResult(sku).withRawCSS("font-weight:bold;background-color:${bgColor};color:#FFFFFF")
}