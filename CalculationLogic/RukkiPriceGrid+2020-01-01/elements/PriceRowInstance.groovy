if (!out.sku) {
    api.abortCalculation()
    return
}
String market = api.isDebugMode() ? "NO" : api.getSecondaryKey()
def product = api.isDebugMode() ? api.find("P", Filter.equal("sku", out.sku))?.find() : api.product()

return libs.RukkiCommonLib.PriceGridManager
           .newPriceGridRow()
           .forProduct(product)
           .withMarket(market)
           .init()