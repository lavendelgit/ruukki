BigDecimal sealingTotal = api.global.Product?.getAt("SEALING")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "E0%"),
                Filter.equal("key3", "AccessoriesPriceRule")]

BigDecimal sealingMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique().size()

if (sealingTotal == null) {
    sealingTotal = BigDecimal.ZERO
}
if (sealingMissing == null) {
    sealingMissing = BigDecimal.ZERO
}
BigDecimal sealingAvailable = sealingTotal > BigDecimal.ZERO ? sealingTotal - sealingMissing : BigDecimal.ZERO

Map sealing = ["Missing": sealingMissing, "Available": sealingAvailable]

return sealing