BigDecimal bespokeCladdingTotal = api.global.Product?.getAt("BESPOKE_CLADDING")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "L00%"),
                Filter.equal("key3", "UserInputPriceCalculations")]

BigDecimal bespokeCladdingMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique()?.size()

if (bespokeCladdingTotal == null) {
    bespokeCladdingTotal = BigDecimal.ZERO
}
if (bespokeCladdingMissing == null) {
    bespokeCladdingMissing = BigDecimal.ZERO
}

BigDecimal bespokeCladdingAvailable = bespokeCladdingTotal > BigDecimal.ZERO ? bespokeCladdingTotal - bespokeCladdingMissing : 0

Map bespokeCladding = ["Missing": bespokeCladdingMissing, "Available": bespokeCladdingAvailable]

return bespokeCladding