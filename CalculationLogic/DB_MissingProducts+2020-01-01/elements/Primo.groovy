BigDecimal primoTotal = api.global.Product?.getAt("PRIMO")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "Z000%"),
                Filter.equal("key3", "UserInputPriceCalculations")]

BigDecimal primoMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique()?.size()

if (primoTotal == null) {
    primoTotal = BigDecimal.ZERO
}
if (primoMissing == null) {
    primoMissing = BigDecimal.ZERO
}

BigDecimal primoAvailable = primoTotal > BigDecimal.ZERO ? primoTotal - primoMissing : 0

Map primo = ["Missing": primoMissing, "Available": primoAvailable]

return primo