BigDecimal fireWallTotal = api.global.Product?.getAt("FIREWALL")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "G0%"),
                Filter.equal("key3", "AccessoriesPriceRule")]

BigDecimal fireWallMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique().size()

if (fireWallTotal == null) {
    fireWallTotal = BigDecimal.ZERO
}
if (fireWallMissing == null) {
    fireWallMissing = BigDecimal.ZERO
}
BigDecimal fireWallAvailable = fireWallTotal > BigDecimal.ZERO ? fireWallTotal - fireWallMissing : BigDecimal.ZERO

Map fireWall = ["Missing": fireWallMissing, "Available": fireWallAvailable]

return fireWall