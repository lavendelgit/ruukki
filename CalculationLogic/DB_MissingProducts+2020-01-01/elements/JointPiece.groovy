BigDecimal jointpieceTotal = api.global.Product?.getAt("JOINT_PIECE")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "A003%"),
                Filter.equal("key3", "WeightBasedRule"),
                Filter.equal("attribute2", "Joint piece")]

BigDecimal jointpieceMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique().size()

if (jointpieceTotal == null) {
    jointpieceTotal = BigDecimal.ZERO
}
if (jointpieceMissing == null) {
    jointpieceMissing = BigDecimal.ZERO
}
BigDecimal jointpieceAvailable = jointpieceTotal > BigDecimal.ZERO ? jointpieceTotal - jointpieceMissing : BigDecimal.ZERO

Map jointpiece = ["Missing": jointpieceMissing, "Available": jointpieceAvailable]

return jointpiece
