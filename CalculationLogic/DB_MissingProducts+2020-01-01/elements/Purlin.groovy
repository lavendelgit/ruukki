BigDecimal purlinTotal = api.global.Product?.getAt("PURLIN")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "U%"),
                Filter.equal("key3", "WeightBasedRule")]

BigDecimal purlinMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique().size()

if (purlinTotal == null) {
    purlinTotal = BigDecimal.ZERO
}
if (purlinMissing == null) {
    purlinMissing = BigDecimal.ZERO
}
BigDecimal purlinAvailable = purlinTotal > BigDecimal.ZERO ? purlinTotal - purlinMissing : BigDecimal.ZERO

Map purlin = ["Missing": purlinMissing, "Available": purlinAvailable]

return purlin