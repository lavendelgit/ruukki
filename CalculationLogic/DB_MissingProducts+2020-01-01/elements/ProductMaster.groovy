ArrayList variantProductGroups = ["PANEL", "LOAD_BEARING_PROFILE", "PROFILE", "PURLIN", "FIREWALL", "FASTENER"]

if (!api.global.Product) {
    Iterator streamedData = api.stream("P", null, ["sku", "attribute1", "attribute3", "attribute4", "attribute5"]
            , Filter.isNotNull("attribute4")
            , Filter.equal("attribute7", "approved"))
    api.global.Product = libs.RukkiCommonLib.MissingProductsUtils.removeBaseProductsFromProductMarketMaster(streamedData, variantProductGroups)
}

return