BigDecimal sheetTotal = api.global.Product?.getAt("SHEET")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "FS%"),
                Filter.equal("key3", "WeightBasedRule")]

BigDecimal sheetMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique().size()

if (sheetTotal == null) {
    sheetTotal = BigDecimal.ZERO
}
if (sheetMissing == null) {
    sheetMissing = BigDecimal.ZERO
}
BigDecimal sheetAvailable = sheetTotal > BigDecimal.ZERO ? sheetTotal - sheetMissing : BigDecimal.ZERO

Map sheet = ["Missing": sheetMissing, "Available": sheetAvailable]

return sheet