BigDecimal designProfTotal = api.global.Product?.getAt("DESIGN_PROFILE")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "D%"),
                Filter.equal("key3", "WeightBasedRule")]

BigDecimal designProfMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique().size()

if (designProfTotal == null) {
    designProfTotal = BigDecimal.ZERO
}
if (designProfMissing == null) {
    designProfMissing = BigDecimal.ZERO
}
BigDecimal designProfAvailable = designProfTotal > BigDecimal.ZERO ? designProfTotal - designProfMissing : BigDecimal.ZERO

Map designProf = ["Missing": designProfMissing, "Available": designProfAvailable]

return designProf