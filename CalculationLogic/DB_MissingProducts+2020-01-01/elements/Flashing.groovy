BigDecimal flashingTotal = api.global.Product?.getAt("FLASHING")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "A%"),
                Filter.equal("key3", "WeightBasedRule"),
                Filter.like("attribute2", "%flashing%")]

BigDecimal flashingMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique().size()

if (flashingTotal == null) {
    flashingTotal = BigDecimal.ZERO
}
if (flashingMissing == null) {
    flashingMissing = BigDecimal.ZERO
}
BigDecimal flashingAvailable = flashingTotal > BigDecimal.ZERO ? flashingTotal - flashingMissing : BigDecimal.ZERO

Map flashing = ["Missing": flashingMissing, "Available": flashingAvailable]

return flashing