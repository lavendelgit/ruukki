BigDecimal fastenerTotal = api.global.Product?.getAt("FASTENER")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "F0%"),
                Filter.equal("key3", "AccessoriesPriceRule")]

BigDecimal fastenerMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique().size()

if (fastenerTotal == null) {
    fastenerTotal = BigDecimal.ZERO
}
if (fastenerMissing == null) {
    fastenerMissing = BigDecimal.ZERO
}
BigDecimal fastenerAvailable = fastenerTotal > BigDecimal.ZERO ? fastenerTotal - fastenerMissing : 0

Map fastener = ["Missing": fastenerMissing, "Available": fastenerAvailable]

return fastener