BigDecimal otherTotal = api.global.Product?.getAt("OTHER")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "O%"),
                Filter.equal("key3", "AccessoriesPriceRule")]

BigDecimal otherMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique().size()

if (otherTotal == null) {
    otherTotal = BigDecimal.ZERO
}
if (otherMissing == null) {
    otherMissing = BigDecimal.ZERO
}
BigDecimal otherAvailable = otherTotal > BigDecimal.ZERO ? otherTotal - otherMissing : 0

Map other = ["Missing": otherMissing, "Available": otherAvailable]

return other