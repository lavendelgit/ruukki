BigDecimal libertaTotal = api.global.Product?.getAt("LIBERTA")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "L000%"),
                Filter.equal("key3", "materialBasedRule")]

BigDecimal libertaMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique()?.size()

if (libertaTotal == null) {
    libertaTotal = BigDecimal.ZERO
}
if (libertaMissing == null) {
    libertaMissing = BigDecimal.ZERO
}

BigDecimal libertaAvailable = libertaTotal > BigDecimal.ZERO ? libertaTotal - libertaMissing : 0

Map liberta = ["Missing": libertaMissing, "Available": libertaAvailable]

return liberta