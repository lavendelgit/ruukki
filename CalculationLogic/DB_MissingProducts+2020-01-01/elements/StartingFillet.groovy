BigDecimal startingFilletTotal = api.global.Product?.getAt("STARTING_FILLET")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "A%"),
                Filter.equal("key3", "WeightBasedRule"),
                Filter.equal("attribute2", "Starting fillet")]

BigDecimal startingFilletMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique().size()

if (startingFilletTotal == null) {
    startingFilletTotal = BigDecimal.ZERO
}
if (startingFilletMissing == null) {
    startingFilletMissing = BigDecimal.ZERO
}
BigDecimal startingFilletAvailable = startingFilletTotal > BigDecimal.ZERO ? startingFilletTotal - startingFilletMissing : 0

Map startingFillet = ["Missing": startingFilletMissing, "Available": startingFilletAvailable]

return startingFillet