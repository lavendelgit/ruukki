BigDecimal supportStudTotal = api.global.Product?.getAt("SUPPORTSTUD")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "R%"),
                Filter.equal("key3", "WeightBasedRule"),
                Filter.like("attribute2", "Support stud%")]

BigDecimal supportStudMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique().size()

if (supportStudTotal == null) {
    supportStudTotal = BigDecimal.ZERO
}
if (supportStudMissing == null) {
    supportStudMissing = BigDecimal.ZERO
}
BigDecimal supportStudAvailable = supportStudTotal > BigDecimal.ZERO ? supportStudTotal - supportStudMissing : BigDecimal.ZERO

Map supportStud = ["Missing": supportStudMissing, "Available": supportStudAvailable]

return supportStud