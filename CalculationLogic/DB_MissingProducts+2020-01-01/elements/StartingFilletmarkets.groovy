List filters = [Filter.like("key1", "A%"),
                Filter.equal("key3", "WeightBasedRule"),
                Filter.equal("attribute2", "Starting fillet")]

Map data = api.findLookupTableValues("MissingData", *filters)?.groupBy { it.key2 }

Map missingMarkets = ["EE": data?.getAt("EE")?.collect { it.key1 }?.unique()?.size(),
                      "FI": data?.getAt("FI")?.collect { it.key1 }?.unique()?.size(),
                      "SE": data?.getAt("SE")?.collect { it.key1 }?.unique()?.size(),
                      "NO": data?.getAt("NO")?.collect { it.key1 }?.unique()?.size(),
                      "EX": data?.getAt("EX")?.collect { it.key1 }?.unique()?.size(),
                      "LV": data?.getAt("LV")?.collect { it.key1 }?.unique()?.size(),
                      "LT": data?.getAt("LT")?.collect { it.key1 }?.unique()?.size(),
                      "UK": data?.getAt("UK")?.collect { it.key1 }?.unique()?.size(),
                      "PL": data?.getAt("PL")?.collect { it.key1 }?.unique()?.size(),
                      "CZ": data?.getAt("CZ")?.collect { it.key1 }?.unique()?.size(),
                      "SK": data?.getAt("SK")?.collect { it.key1 }?.unique()?.size(),
                      "RO": data?.getAt("RO")?.collect { it.key1 }?.unique()?.size()]

return missingMarkets