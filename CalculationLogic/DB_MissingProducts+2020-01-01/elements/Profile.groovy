BigDecimal profileTotal = api.global.Product?.getAt("PROFILE")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "S%"),
                Filter.equal("key3", "WeightBasedRule")]

BigDecimal profileMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique().size()

if (profileTotal == null) {
    profileTotal = BigDecimal.ZERO
}
if (profileMissing == null) {
    profileMissing = BigDecimal.ZERO
}
BigDecimal profileAvailable = profileTotal > BigDecimal.ZERO ? profileTotal - profileMissing : BigDecimal.ZERO

Map profile = ["Missing": profileMissing, "Available": profileAvailable]
return profile