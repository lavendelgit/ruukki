BigDecimal lamellaTotal = api.global.Product?.getAt("CLADDING_LAMELLA")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "C000%"),
                Filter.equal("key3", "materialBasedRule")]

BigDecimal lamellaMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique()?.size()

if (lamellaTotal == null) {
    lamellaTotal = BigDecimal.ZERO
}
if (lamellaMissing == null) {
    lamellaMissing = BigDecimal.ZERO
}
BigDecimal lamellaAvailable = lamellaTotal > BigDecimal.ZERO ? lamellaTotal - lamellaMissing : 0

Map lamella = ["Missing": lamellaMissing, "Available": lamellaAvailable]

return lamella
