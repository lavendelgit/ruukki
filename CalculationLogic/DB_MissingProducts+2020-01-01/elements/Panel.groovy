BigDecimal panelTotal = api.global.Product?.getAt("PANEL")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "P%"),
                Filter.equal("key3", "PanelsRule")
]

BigDecimal panelMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique().size()

if (panelTotal == null) {
    panelTotal = BigDecimal.ZERO
}
if (panelMissing == null) {
    panelMissing = BigDecimal.ZERO
}
BigDecimal panelAvailable = panelTotal > BigDecimal.ZERO ? panelTotal - panelMissing : BigDecimal.ZERO

Map panel = ["Missing": panelMissing, "Available": panelAvailable]

return panel
