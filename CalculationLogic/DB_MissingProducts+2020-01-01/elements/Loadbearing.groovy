BigDecimal loadBearingTotal = api.global.Product?.getAt("LOAD_BEARING_PROFILE")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "B%"),
                Filter.equal("key3", "WeightBasedRule")]

BigDecimal bearingMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique().size()

if (loadBearingTotal == null) {
    loadBearingTotal = BigDecimal.ZERO
}
if (bearingMissing == null) {
    bearingMissing = BigDecimal.ZERO
}
BigDecimal bearingAvailable = loadBearingTotal > BigDecimal.ZERO ? loadBearingTotal - bearingMissing : BigDecimal.ZERO

Map bearing = ["Missing": bearingMissing, "Available": bearingAvailable]

return bearing

