Map chartdefinition = [
        chart      : [type: 'column'],
        title      : [text : 'Data Availability Chart',
                      align: 'left'],
        xAxis      : [categories: ['Panel', 'LoadBearing', 'Profile', 'Purlin', 'DesignProfile', 'Flashing', 'JointPiece',
                                   'SupportStud', 'Sheet', 'StartingFillet', 'Fastener', 'Other', 'Isolation', 'Sealing', 'FireWall', 'Lamella', 'Liberta','BespokeCladding','Primo']],
        yAxis      : [min        : 0,
                      title      : [text: 'Count Products'],
                      stackLabels: [enabled: true]],

        tooltip    : [headerFormat: '<b>{point.x}</b><br/>',
                      pointFormat : '{series.name}: {point.y}<br/>Total: {point.stackTotal}'],
        plotOptions: [column: [stacking  : 'normal',
                               dataLabels: [enabled: true]]],
        series     : [[name : 'Available',
                       color: '#548235',
                       data : [out.Panel?.Available, out.Loadbearing?.Available, out.Profile?.Available, out.Purlin?.Available,
                               out.DesignProfile?.Available, out.Flashing?.Available, out.JointPiece?.Available,
                               out.SupportStud?.Available, out.Sheet?.Available, out.StartingFillet?.Available, out.Fastener?.Available,
                               out.Other?.Available, out.Isolation?.Available, out.Sealing?.Available, out.FireWall?.Available,
                               out.Lamella?.Available, out.Liberta?.Available,out.BespokeCladding?.Available,out.Primo?.Available]],
                      [name : 'Missing',
                       color: '#FF8F8F',
                       data : [out.Panel?.Missing, out.Loadbearing?.Missing, out.Profile?.Missing, out.Purlin?.Missing,
                               out.DesignProfile?.Missing, out.Flashing?.Missing, out.JointPiece?.Missing,
                               out.SupportStud?.Missing, out.Sheet?.Missing, out.StartingFillet?.Missing, out.Fastener?.Missing,
                               out.Other?.Missing, out.Isolation?.Missing, out.Sealing?.Missing, out.FireWall?.Missing,
                               out.Lamella?.Missing, out.Liberta?.Missing,out.BespokeCladding?.Missing,out.Primo?.Missing

                       ]
                      ]
        ]
]
api.buildHighchart(chartdefinition)