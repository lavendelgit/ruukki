BigDecimal isolationTotal = api.global.Product?.getAt("ISOLATION")?.collect { it.key }?.size()

List filters = [Filter.like("key1", "I0%"),
                Filter.equal("key3", "AccessoriesPriceRule")]

BigDecimal isolationMissing = api.findLookupTableValues("MissingData", *filters)?.collect { it.key1 }?.unique().size()

if (isolationTotal == null) {
    isolationTotal = BigDecimal.ZERO
}
if (isolationMissing == null) {
    isolationMissing = BigDecimal.ZERO
}
BigDecimal isolationAvailable = isolationTotal > BigDecimal.ZERO ? isolationTotal - isolationMissing : BigDecimal.ZERO

Map isolation = ["Missing": isolationMissing, "Available": isolationAvailable]

return isolation
