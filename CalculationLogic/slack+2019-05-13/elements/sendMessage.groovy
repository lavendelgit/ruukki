def webhookCall ( String message ) {
   
  /*
    Object httpCall(String url,
                  String body,
                  String methodType,
                  String contentType,
                  Map... properties)

    Returns:
    Map containing two elements 'responseBody' and 'statusCode'
  */  
  
  try {

    def host         = 'https://hooks.slack.com/services/...'
    def payload      = [ text : message ]
    def payload_json = api.jsonEncode(payload)
    def result       = api.httpCall(host, payload_json, 'POST', 'text/plain', [Accept: 'text/plain'])
        
    if ( result.statusCode == '200' ) {
		return true
    }
    
    return false

  } catch(e) {

    api.logWarn("Error calling slack", e)
    
    return false
  }

}
