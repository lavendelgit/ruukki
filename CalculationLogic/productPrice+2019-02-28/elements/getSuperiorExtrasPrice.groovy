// Superior extras are either a fixed price or a percentage to add to the STF price
// if it's fixed, we simply ahd the fixed value to TARGET, FLOOR and START
// if it's a percentage, we multiply the TARGET price by the precentage and we add
// the result to the TARGET, FLOOR and START
// If there are several superior extras, first we add the fixed extras, then we calculate the
// percentage extras on top of the new baseprice which include the fixed extras


def stf_price = out.getSTFPrice
// FOR CHG004, if we have special pricing, we dont do EXTRAS
if (api.global.product.specialPricing == true) {
    return stf_price
}

if (!stf_price || stf_price.size() < 1) {
    def error = "Cannot compute extras prices for product ${api.global.product.productCode} due to missing price modifiers."
    api.local.warnings.add(error)
    return null
}

if (api.global.product.isAccessory == false) {

    def extras_fixed_sum
    def extras_percentage_sum
    def extra_prices_fixed
    def extra_prices_percentage

    try {

        def filters = [
                Filter.equal("sku", api.global.product.productCode)
                , Filter.equal("attribute1", api.global.product.market)
                , Filter.in("attribute2", ["percentage", "fixed"])
                , Filter.equal("name", "SuperiorExtras")
        ]

        def extras = api.find("PX", *filters)

        if (extras) {

            api.local.warnings.add('Calculating prices with extras')

            // Sum fixed extras up and default to zero if none found
            extras_fixed_sum = extras.findAll { it.attribute2 == 'fixed' }.sum { it.attribute3 } ?: 0
            // Sum precentage extras and default to null if none found
            extras_percentage_sum = extras.findAll { it.attribute2 == 'percentage' }.sum { it.attribute3 } ?: null

            // Calculate the price with fixed extras
            extra_prices_fixed = stf_price.collect {
                [
                        segment: it.segment
                        , type : it.type
                        , price: it.price + extras_fixed_sum
                ]
            }

            if (extras_percentage_sum) {

                // Collect the segment-wise target price for all segments
                def target_prices = extra_prices_fixed.findAll { it.type == 'target' }.groupBy { it.segment }

                if (target_prices) {
                    // Calculate the price with percentage extras
                    extra_prices_percentage = extra_prices_fixed.collect {

                        def target_price = target_prices[(it.segment)].price.getAt(0) ?: 0

                        [
                                segment: it.segment
                                , type : it.type
                                , price: it.price + (target_price * extras_percentage_sum)
                        ]
                    }

                    api.local.warnings.add('Returning extras prices')

                    return extra_prices_percentage

                } else {
                    // If we didnt find any target prices to calculate the percentage extras
                    // we fall back on original stf
                    api.local.warnings.add('Returning fixed only')
                    return stf_price
                }

            } else {
                // Dont add percentage extras if there are none
                api.local.warnings.add('Returning fixed only')
                return extra_prices_fixed

            }

        } else {
            def error = "No extras found for product ${api.global.product.productCode} in market ${api.global.product.market}."
            api.local.warnings.add(error)
            return stf_price
        }

    } catch (e) {
        def error = "Failed to get product extras for product ${api.global.product.productCode} in market ${api.global.product.market}"
        api.local.warnings.add(error)
        return stf_price
    }


} else {
    // Technically, we never ask this module to return STF prices for accessories (see getFinalSTFPrice)
    // but a safety check doesn't hurt if the data in the tables is faulty
    def error = "Forbidden to load extras for an accessory ${api.global.product.productCode}"
    api.local.warnings.add(error)

    return stf_price

}