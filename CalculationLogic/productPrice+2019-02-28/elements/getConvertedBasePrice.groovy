if (api.global.product.currency != 'EUR') {

    try {


        def final_base_price = api.getElement('getFinalBasePrice')

        if (!final_base_price) {
            api.local.warnings.add("No final base price to convert for product ${api.global.product.productCode} and currency ${api.global.product.currency}.")
            return null
        }

        def converted_base_price = final_base_price * api.global.conversionFactor

        return converted_base_price

    } catch (e) {

        api.local.warnings.add("Error getting converted base price for product ${api.global.product.productCode} with currency ${api.global.product.currency} ${e}.")
        return null

    }

}


  
