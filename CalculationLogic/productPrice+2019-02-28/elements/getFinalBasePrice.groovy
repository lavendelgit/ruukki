try {

    def freight_price = out.getFreightPrice
    def base_price = out.getProductPrices
    def options_price = out.getOptionsPrice

    api.local.warnings.add("Options price ${options_price}")
    api.local.warnings.add("Base price ${base_price.baseprice}")
    api.local.warnings.add("Freight price ${freight_price}")


    if (base_price != null && options_price != null && freight_price != null) {

        def final_base_price = base_price.baseprice.add(options_price)
        final_base_price = final_base_price.add(freight_price).setScale(3, BigDecimal.ROUND_HALF_UP)
        api.local.warnings.add("Final base price ${final_base_price}")
        return final_base_price

    } else {

        api.local.warnings.add("Error getting final price for product ${api.global.product.productCode}.")
        return null
    }


} catch (e) {

    api.local.warnings.add("Error getting final price for product ${api.global.product.productCode} with currency ${api.global.product.currency} --------- ${e}.")

    return null

}