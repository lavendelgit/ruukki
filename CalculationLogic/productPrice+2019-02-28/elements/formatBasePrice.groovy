try {

    def final_base_price = api.global.product.currency == 'EUR' ? out.getFinalBasePrice : out.getConvertedBasePrice


    if (!final_base_price) {
        api.local.warnings.add("No final base price to format for product ${api.global.product.productCode}")
        return null
    }

    def final_base_price_formatted = final_base_price.setScale(3, BigDecimal.ROUND_HALF_UP)

    return final_base_price_formatted


} catch (e) {

    api.local.warnings.add("Error formatting final base price for product ${api.global.product.productCode}")

    return null
}