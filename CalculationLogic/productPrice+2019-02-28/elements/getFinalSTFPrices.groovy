// Accessories only have STF, not Superior Extras, so we get the prices accordingly
try {

    def multiplicated_prices = api.global.product.isAccessory == false ? out.getSuperiorExtrasPrice : out.getSTFPrice

    return multiplicated_prices

} catch (e) {

    api.local.warnings.add("Error getting final STF prices for product ${api.global.product.productCode} ${e}.")

    return null
}
 