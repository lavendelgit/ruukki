if (api.global.product.isAccessory == true) {

    return new BigDecimal("0")

}

if (api.global.product.isAccessory == false) {

    // Only get default options if there aren't any options already
    api.global.product.options = api.global.product.options.size() < 1 ? out.getDefaultOptions : api.global.product.options

    if (api.global.product.options.size() == 0) {

        if (api.global.product.isProductWithoutGroup) {

            return new BigDecimal("0")

        } else {

            def error = "getOptionsPrice - No options found for product ${api.global.product.productCode}."
            api.local.warnings.add(error)

            return new BigDecimal("0")

        }

    } else {

        try {

            BigDecimal options_price

            // For special options cases, we need to convert back from received currency to EUR before we can sum them inside the othre options prices
            // the conversion back to requested currency is handled later in the logic but all the monetary values in pricefx tables are always in EUR
            //
            def options_special = api.global.product.options.findAll { it.special == true }

            if (options_special && api.global.product.currency != 'EUR') {

                options_special_converted_to_eur = api.global.product.options.collect {
                    it << [
                            price: it.special == true ? (it.price * (1 / api.global.conversionFactor)) : it.price
                    ]
                }

                options_price = options_special_converted_to_eur.sum { it.price }

                return options_price
            }

            // Else we just summ the prices of all options
            options_price = api.global.product.options.sum { it.price }

            return options_price

        } catch (e) {

            def error = "Missing some options prices for product ${api.global.product.productCode}."

            api.local.warnings.add(error)

            return new BigDecimal("0")

        }
    }

}
