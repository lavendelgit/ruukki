// Get the Quantity segments STF

// If we are doing special pricing, the QuantitySegment Price Modifier is 0


// Get the Quantity segments STF

// If we are doing special pricing, the QuantitySegment Price Modifier is 0

if (api.global.product.specialPricing == true) {
    return new BigDecimal('0')
}

// If we are missing the quantity raise an error and return 0
if (!api.global.product.quantity) {
    def error = "Received empty quantity, cannot lookup the quantity segments markup."
    api.local.warnings.add(error)
    return new BigDecimal('0')
}

// Cast anything incoming even though we require integer
def quantity_value = api.global.product.quantity.toInteger()

if (quantity_value > 100000) {
    def error = "Requested amount of ${quantity_value} is too large."
    api.local.warnings.add(error)
    return new BigDecimal('0')
}

// CHG004 allow to make request for prices at three levels:
// Product variant: we use the productCode (Code, Part-Id)
// Product group: we use the baseProduct (not an actual Product)
// Product category: we use the productCategory
try {
    def lookup_table_name = 'PM_QuantitySegments'
    def market_filter = Filter.equal("key1", api.global.product.market)
    // Find out which Product Property needs to be used
    // First try lookup with product variant, the lowest granularity
    def product_reference_filter = Filter.equal("key2", api.global.product.productCode)
    def filters = [
            market_filter
            , Filter.greaterOrEqual('attribute2', quantity_value) // Start quantity
            , Filter.lessOrEqual('attribute1', quantity_value) // End quantity
            , product_reference_filter
    ]
    // Get the filtered rowset
    def lookup_resuts = api.findLookupTableValues(
            lookup_table_name
            , *filters
    )

    if (!lookup_resuts) {
        api.local.warnings.add("No modifiers at product variant level, trying again with base product.")
        // We try again with the base product if we found nothing for the product variant
        product_reference_filter = Filter.equal("key2", api.global.product.baseProduct)
        filters.pop()
        filters.push(product_reference_filter)
        lookup_resuts = api.findLookupTableValues(
                lookup_table_name
                , *filters
        )
        if (!lookup_resuts) {
            api.local.warnings.add("No modifiers at base product level, trying again with category.")
            // And if there's nothing for base product we try with category
            product_reference_filter = Filter.equal("key2", api.global.product.productCategory)
            filters.pop()
            filters.push(product_reference_filter)
            lookup_resuts = api.findLookupTableValues(
                    lookup_table_name
                    , *filters
            )
            if (!lookup_resuts) {
                api.local.warnings.add("No modifiers at product category level, trying again with defaults.")
                // If we still cannot find any entries for the above filter
                // we try again with a default reference
                if (!lookup_resuts) {
                    product_reference_filter = Filter.equal("key2", 'default')
                    filters = filters[0..0]
                    filters.push(product_reference_filter)
                    lookup_resuts = api.findLookupTableValues(
                            lookup_table_name
                            , *filters
                    )
                }
            }
        }
    }
    // Fetch the price modifier for the requested quantity segment
    requested_values = lookup_resuts.getAt(0)

    // If we don't have any price modifiers at this point
    // we return 0 and add a warning
    if (!requested_values) {
        def error = "Failed to get quantity segments markup for the requested amount of ${api.global.product.quantity} ${api.global.product.unitOfMeasure}."
        api.local.warnings.add(error)
        return new BigDecimal('0')
    }

    // Keep the requested segment to be returned in the response
    requested_segment = requested_values['key3']
    api.global.product.quantitySegment = requested_segment

    // Fetch the default modifiers (Top and Bottom) using the same filters
    // Fetch the modifier for the bottom segment
    def bottom_markup = api.findLookupTableValues(
            lookup_table_name
            , Filter.equal('QuantityStart', 1)
            , product_reference_filter
            , market_filter
    )[0]

    if (bottom_markup) {
        bottom_markup = bottom_markup['attribute3']
    } else {
        bottom_markup = new BigDecimal('0')
    }

    // Fetch the modifier for the top segment
    def top_markup = api.findLookupTableValues(
            lookup_table_name
            , Filter.equal('QuantityEnd', 99999)
            , product_reference_filter
            , market_filter
    )[0]

    if (top_markup) {
        top_markup = top_markup['attribute3']
    } else {
        top_markup = new BigDecimal('0')
    }

    // Build a return data structure
    def segments_markups = [
            [
                    name    : requested_segment
                    , markup: requested_values['attribute3']
            ],
            [
                    name    : 'A1'
                    , markup: bottom_markup
            ],
            [
                    name    : 'A8'
                    , markup: top_markup
            ]
    ]

    segments_markups = segments_markups.unique()

    api.local.warnings.add("Segments markup - ${segments_markups}")

    return segments_markups

} catch (err) {

    def error = "Failed to get quantity segments markup for the requested amount of ${api.global.product.quantity} ${api.global.product.unitOfMeasure}. - ${err}"
    api.local.warnings.add(error)
    return new BigDecimal('0')

}



