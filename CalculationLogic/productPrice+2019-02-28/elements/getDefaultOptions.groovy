// If there are no options passed from hybris, get the default options


// Accessories don't have options


if (api.global.product.isAccessory == false) {

    options = libs.optionswithoutcache.getDefaults.main(api.global.product)

    // Return an empty list if there is any error or empty options so the logic downstream can handle it
    if (options instanceof String || !options) {

        api.local.warnings.add(options)

        return []

    }

    // Only return default options which have a selectedByDefault flag

    def options_default = options.findAll { it.default == true && it.selectedByDefault == true }

    return options_default

}


