// Get the inputs via the endpoint
// when testing use the stringUserEntry block


//RUUK-41 change start
//Getting the current partition and setting the global variable accordingly


def String Currpart = api.currentPartitionName()
if (Currpart == "brcp5de644e3") {
    api.global.env = 'prod'
} else if (Currpart == "bynfo_ruukki_qa") {
    api.global.env = 'dev'
}
//RUUK-41 changes end

api.global.product = [:]

api.global.customerClass = api.input("customerClass")
api.global.customerSegment = api.input("customerSegment")
api.global.lineItemNumber = api.input("lineItemNumber")

api.global.product.currency = api.input("currency")
api.global.product.quoteDate = api.input("creationDate")
api.global.product.productCode = api.input("productCode")
api.global.product.unitOfMeasure = api.input("unitOfMeasure")
api.global.product.language = api.input("language")
api.global.product.market = api.input("market")
api.global.product.freightPrice = api.input("freightPrice")
api.global.product.quantity = api.input("quantity")
api.global.product.options = api.input("selectedOptions") ?: [:]
// Jira ticket RUUK-7 Changes : Begin
api.global.product.pricingDate = api.input("pricingDate") ?: null
// Jira ticket RUUK-7 Changes : End
// Define a warning placeholder
api.local.warnings = []

// Not for options!
api.global.product.each {
    if ((!it.value || it.value == null) && it.key != 'options') {
        api.local.warnings.add("Missing input value for ${it.key}.")
    }
}

