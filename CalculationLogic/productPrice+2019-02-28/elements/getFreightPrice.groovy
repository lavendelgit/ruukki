if (!api.global.product.freightPrice | api.global.product.freightPrice == 0) {

    return new BigDecimal('0')

}

// Cast the received value
def freight_price = new BigDecimal(api.global.product.freightPrice)

// Convert back to EUR if the user has another currency context
if (api.global.product.currency != 'EUR') {

    try {

        def converted_freight_price = freight_price * (1 / api.global.conversionFactor)

        return converted_freight_price ?: new BigDecimal('0')


    } catch (e) {

        api.local.warnings.add("Error getting converted freight price for product ${api.global.product.productCode} from currency ${api.global.product.currency} ${e}.")
        return null

    }

}

// If we didn't find any freight
return freight_price ?: new BigDecimal('0')