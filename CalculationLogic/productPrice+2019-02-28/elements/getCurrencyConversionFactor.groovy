if (api.global.product.currency != 'EUR') {

    try {

        def conversion_factor = api.vLookup('Rates', 'ConversionFactor', 'EUR', api.global.product.currency)

        // If we fail to get a conversion factor, use 1 and add a warning
        if (!conversion_factor) {

            api.local.warnings.add("Error getting conversion factor for currency ${api.global.product.currency}. The prices returned are in EUR.")
            api.global.product.currency = 'EUR'
            api.global.conversionFactor = 1

        }

        api.global.conversionFactor = conversion_factor

        return true


    } catch (e) {

        // If we fail to get a conversion factor, use 1 and add a warning
        api.local.warnings.add("Error getting conversion factor for currency ${api.global.product.currency}. The prices returned are in EUR.")
        api.global.product.currency = 'EUR'
        api.global.conversionFactor = 1

    }

}


        