if (api.global.product.currency != 'EUR') {

    try {

        def multi_prices = out.getFinalSTFPrices

        if (!multi_prices) {

            def error = "No STF prices to convert for product ${api.global.product.productCode} with currency ${api.global.product.currency}."
            api.local.warnings.add(error)

            return null

        }

        def converted_stf_prices = multi_prices.collect {
            [
                    segment: it.segment
                    , type : it.type
                    , price: it.price * api.global.conversionFactor
            ]
        }

        return converted_stf_prices

    } catch (e) {

        def error = "Error getting converted STF prices for product ${api.global.product.productCode} with currency ${api.global.product.currency} ${e}. Prices are in EUR."
        api.local.warnings.add(error)

        return null
    }

}


  
