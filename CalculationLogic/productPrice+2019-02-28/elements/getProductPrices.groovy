
/*
General remarks
----------------
- We use api.global.product variable for storing Product-related information
- This Pricing Logic isn't called within a specific Product context in the API so we always check out the Product first
- In the latest Groovy API release, we do not need to use api.getElement() to refer to the output of a logic element but we keep this because
  the code is clearer this way (see https://qa.pricefx.eu/pricefx-api/groovy/3.6/net/pricefx/formulaengine/scripting/PublicGroovyAPI.html#getElement-java.lang.String-)
*/

try {

    // For the ProductPrices, we can have the Product reference either in the SKU field or in the attribute1
    // (the productCode field), this was requested by the business on 04/12/2019 as it's confusing to have a redundant
    // field in the table and technically, the attribute1 field is not a required field
    // Also check that we take the price which is valid for the requested date
    // there can be offers for which the calculation date is not today

    def today = new Date()
    def filters = [
            Filter.equal("name", "ComponentsProductPrices")
            , Filter.or(
            Filter.equal("attribute1", api.global.product.productCode),
            Filter.equal("sku", api.global.product.productCode)
    )
            , Filter.equal("attribute3", api.global.product.market)
    ]
    // Jira ticket RUUK-7 Changes : Begin
    def priceDateInput = api.global.product.pricingDate
    def priceDate
    def priceDateFormatted

    if (priceDateInput) {
        priceDate = Date.parse("yyyy-MM-dd", priceDateInput)
        priceDateFormatted = priceDate.format("yyyy-MM-dd'T'HH:mm:ss.SSS")
        filters << Filter.or(Filter.lessOrEqual("attribute11", priceDateFormatted),Filter.isNull("attribute11"))
    }
    def price_rows = api.find("PX20", 0, 1, "-attribute11", *filters)
// Jira ticket RUUK-7 Changes : End


    if ( price_rows.size() > 0 ) {

        // Until we start using ValidFrom column, we only have a single price row
        def valid_price_row = price_rows.getAt(0)

        //def date_diff = null
        //def valid_price_row = null
        //def quote_date = Date.parse("yyyy-MM-dd", api.global.product.quoteDate)

        // ValidFrom field is not being taken into use yet
        // The valid price row will be the row with the closest
        // positive days difference between the price validity date
        // and the quotation date
        /*for (price_row in price_rows) {

              def valid_from_date = Date.parse("yyyy-MM-dd", price_row.attribute11)
            def difference_to_quote =  quote_date - valid_from_date

            if (difference_to_quote < 0) {
              // Currently, quotes cannot be done with future prices
              continue
            } else if (difference_to_quote == 0) {
                  // If we have a new price on quote day, we use it
                valid_price_row = price_row
                  break;
            } else {
              // Overwrite the row with the smallest difference
              if (!date_diff || (difference_to_quote < date_diff)) {
                  date_diff = difference_to_quote
                  valid_price_row = price_row
              }
            }
        }*/

        // Keep base price and stf prices separate so we can do list operations more easily
        def prices = {}

        prices.baseprice = valid_price_row.attribute4.setScale(3, BigDecimal.ROUND_HALF_UP)

        api.local.warnings.add("Original base price ${prices.baseprice}")

        // CHG005 add the cogs price, note we keep the cogs price separated from other prices
        // because it's not actually used in the calculation logic but just comes out of the api
        api.global.product.cogsPrice = valid_price_row.attribute10
        api.global.product.lastUpdateDate = valid_price_row.lastUpdateDate
        // CHG004 If we have the special prices in the product prices, we use these instead of normal STF
        // This logic allows to bypasses the need for QuantitySegment Price Modifiers altogether
        if ((valid_price_row.attribute8 != null) || (valid_price_row.attribute9 != null)) {

            // We must ensure we have both highest and lowest categories before continuing
            if ((valid_price_row.attribute8 != null) && (valid_price_row.attribute9 != null)) {
                // we use the following notation in the keys so we can use the
                // values downstream by splitting on '_'
                prices.stfprices = [
                        target_A1 : valid_price_row.attribute8
                        , target_A8 : valid_price_row.attribute9
                        , floor_A1  : valid_price_row.attribute7
                        , floor_A8  : valid_price_row.attribute7
                        , start_A1  : valid_price_row.attribute9
                        , start_A8  : valid_price_row.attribute9
                ]

                api.global.product.specialPricing = true

            } else {

                def error = "Trying to use special pricing for product ${api.global.product.productCode} and market ${api.global.product.market} combination but missing the second target price."
                api.local.warnings.add(error)
                return null

            }

        } else {
            // Else we use the normal prices
            prices.stfprices = [
                    start  : valid_price_row.attribute5
                    , target : valid_price_row.attribute6
                    , floor  : valid_price_row.attribute7
            ]

        }

        // Add a warning if we don't have all prices for normal pricing
        if (prices.stfprices.size() != 3 ) {
            def error = "Expecting extactly 3 prices for product ${api.global.product.productCode} and market ${api.global.product.market} but received ${prices.stfprices.size()}."
            api.local.warnings.add(error)
        }

        return prices

    }

    def error = "No price records found for product ${api.global.product.productCode} and market ${api.global.product.market} combination."
    api.local.warnings.add(error)

    return null

} catch (e) {

    def error = "Error retrieving prices for product ${api.global.product.productCode} and market ${api.global.product.market} combination - ${e}"
    api.local.warnings.add(error)

    return null

}

