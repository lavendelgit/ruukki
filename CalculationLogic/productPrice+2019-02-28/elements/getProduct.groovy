api.global.product_without_group = ['LIBERTA', 'DESIGN_PROFILE', 'CLADDING_LAMELLA', 'PROFILE', 'PURLIN', 'LOAD_BEARING_PROFILE']

/*
   NOTE THESE AREN'T VALID ANYMORE FOR CHG004 BECAUSE WE ARE USING CATEGORY

  B - LOAD_BEARING_PROFILE - HAS BASE PRODUCT AND USED AS "LOAD_BEARING_PROFILE" IN STF TABLE
  C - CLADDING_LAMELLA     - NO BASE PRODUCT AT ALL
  D - DESIGN_PROFILE       - NO BASE PRODUCT AT ALL
  L - LIBERTA              - NO BASE PRODUCT AT ALL
  S - PROFILE              - NO BASE PRODUCT AT ALL
  U - PURLIN               - HAS BASE PRODUCT AND USED AS "PURLIN" IN STF TABLE
*/

// Retrieve a reference to the product so we can have the Product Group
def sku_filter = Filter.equal("sku", api.global.product.productCode)
def code_filter = Filter.equal("attribute2", api.global.product.productCode)
try {

    def product_rows = api.find("P", sku_filter, code_filter)

    if (product_rows.size() > 1) {

        api.global.product.isAccessory = false
        api.global.product.baseProduct = ''
        api.global.product.productType = ''
        api.global.product.productCategory = ''
        api.global.product.productGroup = null

        def error = "Duplicate entries in product table for product ${api.global.product.productCode}"
        api.local.warnings.add(error)
        return

    }

    def product = product_rows.getAt(0)

    // Set a flag if it's an accessories so we can route the options pricing logic as needed
    // downstream because accessories only require
    api.global.product.isAccessory = product.attribute6.contains('accessories_') ? true : false
    api.global.product.baseProduct = product.attribute1 ? product.attribute1 : ''
    api.global.product.productType = product.attribute4 ?: ''
    api.global.product.productCategory = product.attribute6 ?: ''
    api.global.product.specialPricing = false
    api.global.product.isProductWithoutGroup = api.global.product_without_group.any { it.contains(product.attribute4) }

    // For normal product without a group or for accessories, the 'productGroup' attribute we are using is the type of product
    // For normal products with proper base product, it's the base product code
    api.global.product.productGroup = api.global.product.isProductWithoutGroup || api.global.product.isAccessory == true ? product.attribute4 : product.attribute1 ? product.attribute1 : null

    if (!api.global.product.productGroup) {
        def error = "No product group for product ${api.global.product.productCode}"
        api.local.warnings.add(error)
    }

} catch (e) {

    // Set null if no base product in the table
    api.global.product.productGroup = null
    def error = "No product group for product ${api.global.product.productCode}"
    api.local.warnings.add(error)

}
