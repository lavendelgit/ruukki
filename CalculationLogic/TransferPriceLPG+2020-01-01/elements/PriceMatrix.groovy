import net.pricefx.formulaengine.scripting.portlets.ResultMatrix

String sku = out.Product?.sku
String baseProduct = out.Product?.attribute1
String productCategory = out.Product?.attribute6
String market = out.Market

Filter productCodeFilter = Filter.equal("key2", sku)
Filter baseproductFilter = Filter.equal("key2", baseProduct)
Filter productCategoryFilter = Filter.equal("key2", productCategory)
Filter marketFilter = Filter.equal("key1", market)
if (!api.global.basemarkups) {
    api.global.basemarkups = api.findLookupTableValues("Base_Sales_Markups", "-key3")?.groupBy {
        it.key1 + "_" + it.key2
    }
}
List basemarkups = api.global.basemarkups?.getAt(market + "_" + sku)
if (!basemarkups) {
    basemarkups = api.global.basemarkups?.getAt(market + "_" + baseProduct)
}
if (!basemarkups) {
    basemarkups = api.global.basemarkups?.getAt(market + "_" + productCategory)
}
Filter segmentFilter = Filter.equal("key3", "A1")


if (!api.global.pmQtySegments) {
    api.global.pmQtySegments = api.findLookupTableValues("PM_QuantitySegments", segmentFilter)?.collectEntries {
        [(it.key1 + "_" + it.key2): it]
    }
}

Map pmQuantityMarkupdata = api.global.pmQtySegments?.getAt(market + "_" + sku)
if (!pmQuantityMarkupdata) {
    pmQuantityMarkupdata = api.global.pmQtySegments?.getAt(market + "_" + baseProduct)
}
if (!pmQuantityMarkupdata) {
    pmQuantityMarkupdata = api.global.pmQtySegments?.getAt(market + "_" + productCategory)
}

List key = out.SecondaryKeyList

String materialKey = key[1]
String thicknessKey = key[2]
String coatingKey = key[3]
String colorKey = key[4]

BigDecimal targetPrice
BigDecimal startPrice
BigDecimal floorPrice
BigDecimal A1_targetPrice
BigDecimal A8_targetPrice
BigDecimal A1_startPrice
BigDecimal A8_startPrice
BigDecimal A1_floorPrice
BigDecimal A8_floorPrice
BigDecimal transferPrice
BigDecimal cogsPrice
BigDecimal basePrice

if (!api.global.weightedRuleGroups) {
    api.global.weightedRuleGroups = api.findLookupTableValues("PricingRule")?.collectEntries {
        [(it.name): it.value?.tokenize(",")?.collect { it.trim() }]
    }
}
List WeightedRuleGroups = api.global.weightedRuleGroups?.getAt("WeightBasedRule")


String productGroup = out.Product?.attribute4
boolean isWeightedRule = WeightedRuleGroups.contains(productGroup)

if (!api.global.weightMarkupsData) {
    api.global.weightMarkupsData = api.stream("PX20",
            "-attribute5",
            ["sku", "attribute2", "attribute3", "attribute4", "attribute5", "attribute6", "attribute7"],
            Filter.equal("name", "ProductWeightMarkups"))?.withCloseable { it.collect() }?.groupBy { it.sku }
}
List weightMarkupsData = api.global.weightMarkupsData?.getAt(sku)

List width = weightMarkupsData?.attribute6
List plant = weightMarkupsData?.attribute7

List rawMaterialData = api.find("PX20", 0, "-attribute6",
        Filter.equal("name", "RawMaterialPrice"),
        Filter.equal("sku", materialKey),
        Filter.or(Filter.equal("ThicknessKey", thicknessKey),
                Filter.equal("ThicknessKey", "*")),
        Filter.or(Filter.equal("CoatingKey", coatingKey),
                Filter.equal("CoatingKey", "*")),
        Filter.or(Filter.equal("ColorKey", colorKey),
                Filter.equal("ColorKey", "*")),
        Filter.or(Filter.in("Width", width),
                Filter.equal("Width", "*")),
        Filter.or(Filter.in("ProductionPlant", plant),
                Filter.equal("ProductionPlant", "*"))

)

List rawMaterialCogs = api.find("PX20", 0, "-attribute6",
        Filter.equal("name", "RawMaterialCOGS"),
        Filter.equal("sku", materialKey),
        Filter.or(Filter.equal("ThicknessKey", thicknessKey),
                Filter.equal("ThicknessKey", "*")),
        Filter.or(Filter.equal("CoatingKey", coatingKey),
                Filter.equal("CoatingKey", "*")),
        Filter.or(Filter.equal("ColorKey", colorKey),
                Filter.equal("ColorKey", "*")),
        Filter.or(Filter.in("Width", width),
                Filter.equal("Width", "*")),
        Filter.or(Filter.in("ProductionPlant", plant),
                Filter.equal("ProductionPlant", "*"))
)

List result = []
if (isWeightedRule) {
    weightMarkupsData.each { weightData ->
        rawMaterialData.each { rawMaterial ->
            basemarkups.each { basemarkup ->
                BigDecimal weight = weightData?.attribute4 as BigDecimal
                BigDecimal processingCost = weightData?.attribute2 as BigDecimal
                BigDecimal pgScrap = weightData?.attribute3 as BigDecimal
                BigDecimal steelcost = rawMaterial?.attribute7 as BigDecimal
                BigDecimal rmbase = (steelcost && weight) ? (steelcost / 1000) * weight : null
                cogsPrice = (rmbase && pgScrap && processingCost) ? (rmbase * (1 + pgScrap)) + processingCost : null
                basePrice = cogsPrice ? cogsPrice * (1.0 + basemarkup?.attribute1) : null
                transferPrice = (rmbase && pgScrap && processingCost) ? (rmbase * (1.0 + pgScrap)) + (processingCost * basemarkup?.attribute3) : null
                if (market != "FI") {
                    targetPrice = basePrice ? basePrice * (1.0 + basemarkup?.attribute5) : null
                    startPrice = targetPrice ? targetPrice * (1.0 + basemarkup?.attribute2) : null
                    floorPrice = targetPrice ? targetPrice * (1.0 - basemarkup?.attribute4) : null
                } else {
                    A1_targetPrice = basePrice ? basePrice * (1.0 + pmQuantityMarkupdata?.attribute3) : null
                    A8_targetPrice = basePrice ? basePrice * (1.0 + basemarkup?.attribute5) : null
                    A1_startPrice = A1_targetPrice ? A1_targetPrice * (1.0 + basemarkup?.attribute2) : null
                    A8_startPrice = A8_targetPrice ? A8_targetPrice * (1.0 + basemarkup?.attribute2) : null
                    A1_floorPrice = A1_targetPrice ? A1_targetPrice * (1.0 - basemarkup?.attribute4) : null
                    A8_floorPrice = A8_targetPrice ? A8_targetPrice * (1.0 - basemarkup?.attribute4) : null
                }

                result << ["cogsPrice"             : cogsPrice,
                           "basePrice"             : basePrice,
                           "rawMaterialCost"       : (rmbase && pgScrap) ? (rmbase * (1 + pgScrap))?.setScale(2, BigDecimal.ROUND_HALF_UP) : null,
                           "processingCost"        : processingCost,
                           "startPrice"            : startPrice,
                           "floorPrice"            : floorPrice,
                           "targetPrice"           : targetPrice,
                           "A1_target"             : A1_targetPrice,
                           "A8_target"             : A8_targetPrice,
                           "A1_start"              : A1_startPrice,
                           "A8_start"              : A8_startPrice,
                           "A1_floor"              : A1_floorPrice,
                           "A8_floor"              : A8_floorPrice,
                           "transferPrice"         : transferPrice,
                           "weightMarkupsValidFrom": cogsPrice ? weightData?.attribute5 : null,
                           "rawMaterialValidFrom"  : cogsPrice ? rawMaterial?.attribute6 : null,
                           "baseMarkupsValidFrom"  : cogsPrice ? basemarkup?.key3 : null]
            }
        }

        rawMaterialCogs.each { rawMaterial ->
            basemarkups.each { basemarkup ->
                BigDecimal weight = weightData?.attribute4 as BigDecimal
                BigDecimal processingCost = weightData?.attribute2 as BigDecimal
                BigDecimal pgScrap = weightData?.attribute3 as BigDecimal
                BigDecimal steelcost = rawMaterial?.attribute7 as BigDecimal
                BigDecimal rmbase = (steelcost && weight) ? (steelcost / 1000) * weight : null
                cogsPrice = (rmbase && pgScrap && processingCost) ? (rmbase * (1 + pgScrap)) + processingCost : null
                basePrice = cogsPrice ? cogsPrice * (1.0 + basemarkup?.attribute1) : null
                transferPrice = (rmbase && pgScrap && processingCost) ? (rmbase * (1.0 + pgScrap)) + (processingCost * basemarkup?.attribute3) : null
                if (market != "FI") {
                    targetPrice = basePrice ? basePrice * (1.0 + basemarkup?.attribute5) : null
                    startPrice = targetPrice ? targetPrice * (1.0 + basemarkup?.attribute2) : null
                    floorPrice = targetPrice ? targetPrice * (1.0 - basemarkup?.attribute4) : null
                } else {
                    A1_targetPrice = basePrice ? basePrice * (1.0 + pmQuantityMarkupdata?.attribute3) : null
                    A8_targetPrice = basePrice ? basePrice * (1.0 + basemarkup?.attribute5) : null
                    A1_startPrice = A1_targetPrice ? A1_targetPrice * (1.0 + basemarkup?.attribute2) : null
                    A8_startPrice = A8_targetPrice ? A8_targetPrice * (1.0 + basemarkup?.attribute2) : null
                    A1_floorPrice = A1_targetPrice ? A1_targetPrice * (1.0 - basemarkup?.attribute4) : null
                    A8_floorPrice = A8_targetPrice ? A8_targetPrice * (1.0 - basemarkup?.attribute4) : null
                }

                result << ["newCogsPrice"             : cogsPrice,
                           "newBasePrice"             : basePrice,
                           "rawMaterialCost"       : (rmbase && pgScrap) ? (rmbase * (1 + pgScrap))?.setScale(2, BigDecimal.ROUND_HALF_UP) : null,
                           "processingCost"        : processingCost,
                           "startPrice"            : startPrice,
                           "floorPrice"            : floorPrice,
                           "targetPrice"           : targetPrice,
                           "A1_target"             : A1_targetPrice,
                           "A8_target"             : A8_targetPrice,
                           "A1_start"              : A1_startPrice,
                           "A8_start"              : A8_startPrice,
                           "A1_floor"              : A1_floorPrice,
                           "A8_floor"              : A8_floorPrice,
                           "transferPrice"         : transferPrice,
                           "weightMarkupsValidFrom": cogsPrice ? weightData?.attribute5 : null,
                           "rawMaterialValidFrom"  : cogsPrice ? rawMaterial?.attribute6 : null,
                           "baseMarkupsValidFrom"  : cogsPrice ? basemarkup?.key3 : null]
            }
        }
    }


    ResultMatrix matrix = api.newMatrix("cogsPrice", "basePrice", "rawMaterialCost", "processingCost", "startPrice", "floorPrice", "targetPrice", "A1_target", "A8_target", "A1_start", "A8_start", "A1_floor",
            "A8_floor", "weightMarkupsValidFrom", "rawMaterialValidFrom", "normalTargetMarkupsValidFrom", "baseMarkupsValidFrom", "transferPrice")

    result.each {
        matrix.addRow(it)
    }

    return matrix
}
