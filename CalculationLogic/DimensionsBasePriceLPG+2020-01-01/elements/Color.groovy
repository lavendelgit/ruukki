def secondaryKey = out.SecondaryKey
def key = secondaryKey?.tokenize("|")

def color = key[5]
return color