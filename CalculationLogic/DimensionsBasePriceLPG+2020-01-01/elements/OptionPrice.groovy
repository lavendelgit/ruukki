//def secondaryKey = api.getElement("Debug")=="No" ? api.getSecondaryKey() : "EE|100-3800/100-225|materialCC|thickness12|coatingPowdercoat|*"
def secondaryKey = out.SecondaryKey

def key = secondaryKey?.tokenize("|")
api.global.market = key[0]
api.global.OptionKey = key[1]
api.global.MaterialKey = key[2]
api.global.ThicknessKey = key[3]
api.global.CoatingKey = key[4]
api.global.ColorKey = key[5]

def length = api.global.OptionKey?.split("/")[0].split("-")[0]  as BigDecimal
length = length + 5.0
def width = api.global.OptionKey?.split("/")[1].split("-")[0]   as BigDecimal
width = width + 5.0
def quantity = api.findLookupTableValues("CacheOptions", Filter.equal("name", "LPG_Quantity"))?.getAt(0)?.attribute5 as BigDecimal

def optionPrice = libs.claddingCOGS.COGSPrice.getOptionsPriceList(out.productID?.sku, length, width, api.global.MaterialKey, api.global.ThicknessKey, api.global.CoatingKey, api.global.ColorKey, null, quantity)
return optionPrice