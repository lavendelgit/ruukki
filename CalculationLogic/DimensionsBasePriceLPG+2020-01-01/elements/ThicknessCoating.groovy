def secondaryKey = out.SecondaryKey
def key = secondaryKey?.tokenize("|")

def thickness = key[3]
def coating = key[4]

return thickness+","+coating