def marketFilter = Filter.equal("key1", api.global.market)
def productCodeFilter = Filter.equal("key2", out.productID?.attribute2)
def baseProductFilter = Filter.equal("key2", out.productID?.attribute1)
def productCategoryFilter = Filter.equal("key2", out.productID?.attribute6)
def baseMarkup = libs.claddingCOGS.BaseMarkup.getBaseMarkupList(marketFilter, productCodeFilter, baseProductFilter, productCategoryFilter, null)
List result = []
out.OptionPrice?.each { it ->
    baseMarkup?.each { markup ->
        result << [
                "cogsPrice"            : it.cogsPrice?.setScale(3, BigDecimal.ROUND_HALF_UP),
                "rawMaterialPrice"     : it.rawMaterialPrice?.setScale(3, BigDecimal.ROUND_HALF_UP),
                "rawMaterialValidFrom" : it.rawMaterialValidFrom,
                "claddingCostValidFrom": it.claddingCostValidFrom,
                "inputArea"            : it.inputArea,
                "baseMarkupsValidFrom" : markup?.key3,
                "basePrice"            : ((it.cogsPrice ?: 0) * (1.0 + (markup?.attribute1 )))?.setScale(3, BigDecimal.ROUND_HALF_UP),
                "transferPrice"        : (((it.processingCost ?: 0) * (markup?.attribute3 )) + (it.rawMaterialPrice ?: 0))?.setScale(3, BigDecimal.ROUND_HALF_UP)
        ]
    }
}
def matrix = api.newMatrix("rawMaterialPrice","cogsPrice", "basePrice", "transferPrice","rawMaterialValidFrom", "claddingCostValidFrom","baseMarkupsValidFrom", "inputArea")
result.each{
    matrix.addRow(it)
}
return matrix
