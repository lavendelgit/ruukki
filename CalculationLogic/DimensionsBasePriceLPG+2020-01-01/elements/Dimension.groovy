def secondaryKey = out.SecondaryKey
def key = secondaryKey?.tokenize("|")

def dimension = key[1]
return dimension