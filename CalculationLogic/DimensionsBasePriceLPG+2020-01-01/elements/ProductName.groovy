def sku = out.productID?.sku
def name = api.find("P",Filter.equal("sku",sku))?.getAt(0)?.attribute3
return name