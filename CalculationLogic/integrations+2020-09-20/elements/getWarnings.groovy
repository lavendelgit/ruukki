// Log any warnings to the request to enable
// troubleshooting from caller
if (api.global.env != 'prod') {
  if ( api.local.warnings.size() > 0 ) {
    api.local.warnings.each { w ->
      api.addWarning("${w}")
    }
  }
}