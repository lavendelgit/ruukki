/*
*
*    The endpoint for requesting data in bulk.
*	Accepts a json body with following fields
*
*      {
*          "tableName": "optionsTranslations",
*      }*

*/
api.local.env = 'production'
api.local.warnings = []
api.local.page_size = api.input("pageSize") ?: 100
api.local.table_name = api.input("tableName")