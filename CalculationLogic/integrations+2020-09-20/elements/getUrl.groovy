// Build the url to be called by the external system
def table = api.getElement('getObjectReference')
def url

try {
  def root
  def partition = api.currentPartitionName()

  // Get url root depending on the environment
  if (api.local.env == "dev") {
    root = "https://staging-eu.pricefx.eu/pricefx/${partition}"
  }

  if (api.local.env == "production") {
    root = "https://www.pricefx.eu/pricefx/${partition}"
  }
  
  // For Price Paramters table, add the internal id of the table
  if (table.type == "MLTV2") {    
    url = "${root}/lookuptablemanager.fetch/${table.id}"
  }
  
  // For Product Extensions we use the fetch api
  if (table.type == "PX") {
  	url = "${root}/fetch/${table.type}"
  }

} catch (e) {
  
  msg = "Failed to build url -- ${e}"
  api.local.warnings.add(msg)
  url = "ERROR"

}
  
return url