// Build the body of the request to be used by the external system
def body = []
def table = api.getElement('getObjectReference')


try {
  // This will build the json body of the first request the 
  // integration system will make to pricefx to retrieve data
  
  // For Product Extensions, we use the name as a filter
  if (table.type == "PX" ) {
    body = [
      startRow: 0,
      endRow: api.local.page_size,
      data:[
        name: table.name
      ]
    ]
  }

  // For Price Parameters we don't use any filter
  // the id of the table will be passed in the url
  if (table.type == "MLTV2" ) {
    body = [
      startRow: 0,
      endRow: api.local.page_size
    ]
  }

} catch (e) {
  
  msg = "Failed to build body -- ${e}"
  api.local.warnings.add(msg)
  body = "ERROR"
  
}


return body