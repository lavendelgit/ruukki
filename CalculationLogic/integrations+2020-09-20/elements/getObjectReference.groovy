// Get the object and resource type of the requested table
def table_details = [:]

def getColumnMapping(tablename, tabletype, tableid) {
  
  def body = ["data": tabletype == "MLTV2" ? ["lookupTableId": tableid] : ["name": tablename]]
  def url = "/fetch/" + (tabletype == "MLTV2" ? "MLTVM" : "PXAM")
  def output = api.boundCall(api.local.env, url, api.jsonEncode(body))
  
  if (output.statusCode.toInteger() > 203) {
    api.local.warnings.add(output)
    return "Failed to retrieve column mappings."
  }
  
  return output.responseBody.response.data.findAll{
    !it.label.contains("attribute")
  }.unique()
  .collect {
    it << [
      "attribute1": it.sku
    ]
  }
  .collect{
    [
      "${it.fieldName}": it.label
    ]
  }
}

// The table references are currently hardcoded but we could improve
// this by reading from a Price Parameter table and allowing power users
// to enable integrations for any table on the partition
def tables = [
    [
     	name: "optionsTranslations",
    	type: "MLTV2",
      	id: null,
      	columns : null
    ],
    [
    	name: "DefaultOptions",
    	type: "PX",
      	id: null,
      	columns : null
    ],
	[
    	name: "AvailableOptions",
     	type: "PX",
      	id: null,
      	columns : null
    ]
]

try {
  	// Obtain the table configuration by matching the name
  	table_details = tables.find {
  		it.name == api.local.table_name
	}
  
    // For PriceParameter tables we need the id of the table
    if (table_details.type == "MLTV2") {
      def table_id = api.findLookupTable(table_details.name).id
      table_details.id = table_id
    }
  	// Obtain the column label:attribute mappings for the table
  	table_details.columns = getColumnMapping(table_details.name, table_details.type, table_details.id)
  	
  
} catch (e) {
  
  msg = "Failed to get object reference for table ${api.local.table_name} --- ${e}"
  api.local.warnings.add(msg)
  
}

return table_details