if ( api.isSyntaxCheck() ) {  
  return
}

def url = api.getElement("getUrl")
def body = api.getElement("getBody")
def columns = api.getElement('getObjectReference').columns

// Build the output object
def output = [
  
  url: url,
  body: body,
  columns: columns

]

return output