Map panelsMaster = api.global.panelsMaster
List panelBasedRulePP = out.PanelsPP as List
List panelsBSM = out.BaseSalesMarkups as List
List result = []

panelsMaster?.each { sku, value ->
    if (sku in panelBasedRulePP) {
        for (market in value?.markets) {
            if (market + ":" + sku in panelsBSM || market + ":" + value?.baseProduct in panelsBSM || market + ":" + value?.category in panelsBSM) {
                continue
            }
            if (sku + ":" + market + ":" + value?.category in api.local.panelBasedMissedProducts) {
                continue
            }
            api.local.panelBasedMissedProducts << sku + ":" + market + ":" + value?.category
            result += ["lookupTableId"  : api.global.tableid,
                       "lookupTableName": "MissingData",
                       "key1"           : sku,
                       "key2"           : market,
                       "key3"           : "PanelsRule",
                       "key4"           : "PanelsPrices,BaseSalesMarkup",
                       "attribute1"     : value?.category,
                       "attribute2"     : api.product("Name", sku)]
        }
    }
}

return result