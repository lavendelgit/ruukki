Map accessoryRuleMaster = api.global.accessoriesPriceBasedRuleMaster
List accessoriesPriceRuleCPP = out.AccessoriesPriceRuleCPP as List
List accessoriesPriceRuleBSM = out.BaseSalesMarkups as List
List accessoriesPriceRulePMQ = out.PMQuantitySegments as List
api.local.accessoriesBasedMissedProducts = []
List result = []

accessoryRuleMaster?.each { sku, value ->
    if (sku in accessoriesPriceRuleCPP) {
        for (market in value?.markets) {
            if (market + ":" + sku in accessoriesPriceRuleBSM || market + ":" + value?.baseProduct in accessoriesPriceRuleBSM || market + ":" + value?.category in accessoriesPriceRuleBSM) {
                continue
            }
            if (market + ":" + sku in accessoriesPriceRulePMQ || market + ":" + value?.baseProduct in accessoriesPriceRulePMQ || market + ":" + value?.category in accessoriesPriceRulePMQ) {
                continue
            }
            if (sku + ":" + market + ":" + value?.category in api.local.accessoriesBasedMissedProducts) {
                continue
            }
            api.local.accessoriesBasedMissedProducts << sku + ":" + market + ":" + value?.category
            result += ["lookupTableId"  : api.global.tableid,
                       "lookupTableName": "MissingData",
                       "key1"           : sku,
                       "key2"           : market,
                       "key3"           : "AccessoriesPriceRule",
                       "key4"           : "AccessoriesBasedMissingProduct",
                       "attribute1"     : value?.category,
                       "attribute2"     : api.product("Name", sku)]
        }
    }
}

return result