return ["WeightBasedRule"            : ["LOAD_BEARING_PROFILE", "PURLIN", "PROFILE", "DESIGN_PROFILE", "FLASHING", "JOINT_PIECE", "SUPPORTSTUD", "SHEET", "STARTING_FILLET"],
        "AccessoriesPriceRule"       : ["OTHER", "FASTENER", "ISOLATION", "SEALING", "FIREWALL"],
        "materialBasedRule"          : ["CLADDING_LAMELLA", "LIBERTA"],
        "componentProductPricesFetch": [""],
        "UserInputPriceCalculations" : ["PRIMO", "BESBOKE_CLADDING", "BESPOKE_CLADDING"],
        "PanelsPricing"              : ["PANEL"]]
