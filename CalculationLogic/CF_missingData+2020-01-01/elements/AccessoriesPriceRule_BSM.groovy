Map accessoriesPriceBasedRuleMaster = api.global.accessoriesPriceBasedRuleMaster
List accessoriesPriceRuleBSM = out.BaseSalesMarkups as List
List result = []

accessoriesPriceBasedRuleMaster?.each { sku, value ->
    for (market in value?.markets) {
        if (market + ":" + sku in accessoriesPriceRuleBSM || market + ":" + value?.baseProduct in accessoriesPriceRuleBSM || market + ":" + value?.category in accessoriesPriceRuleBSM) {
            continue
        }
        if (sku + ":" + market + ":" + value?.category in api.local.accessoriesBasedMissedProducts) {
            continue
        }
        api.local.accessoriesBasedMissedProducts << sku + ":" + market + ":" + value?.category
        result += ["lookupTableId"  : api.global.tableid,
                   "lookupTableName": "MissingData",
                   "key1"           : sku,
                   "key2"           : market,
                   "key3"           : "AccessoriesPriceRule",
                   "key4"           : "BaseSalesMarkup",
                   "attribute1"     : value?.category,
                   "attribute2"     : api.product("Name", sku)]
    }
}

return result