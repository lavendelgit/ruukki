Map productMaster = api.global.userInputPriceRuleMaster
List userInputPriceRuleBasedRulePMQ = out.PMQuantitySegments as List
List result = []

productMaster?.each { sku, value ->
    for (market in value?.markets) {
        if (market + ":" + sku in userInputPriceRuleBasedRulePMQ || market + ":" + value?.baseProduct in userInputPriceRuleBasedRulePMQ || market + ":" + value?.category in userInputPriceRuleBasedRulePMQ) {
            continue
        }
        if (sku + ":" + market + ":" + value?.category in api.local.userInputPriceMissedProducts) {
            continue
        }
        api.local.userInputPriceMissedProducts << sku + ":" + market + ":" + value?.category
        result += ["lookupTableId"  : api.global.tableid,
                   "lookupTableName": "MissingData",
                   "key1"           : sku,
                   "key2"           : market,
                   "key3"           : "UserInputPriceCalculations",
                   "key4"           : "PM_QuantitySegments",
                   "attribute1"     : value?.category,
                   "attribute2"     : api.product("Name", sku)]
    }
}

return result