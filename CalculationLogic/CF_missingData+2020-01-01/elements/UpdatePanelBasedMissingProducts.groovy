List panelsMissingProducts = out.PanelsMissingProducts ?: []
List panelsMissing_PP_BSM = out.PanelsMissing_PP_BSM ?: []
List panelsMissing_PP_PMQ = out.PanelsMissing_PP_PMQ ?: []
List panelsMissing_BSM_PMQ = out.PanelsMissing_BSM_PMQ ?: []
List panelsMissing_PP = out.PanelsMissing_PP ?: []
List panelsMissing_BSM = out.PanelsMissing_BSM ?: []
List panelsMissing_PMQ = out.PanelsMissing_PMQ ?: []
if (panelsMissingProducts) {
    api.addOrUpdate("MLTV4", panelsMissingProducts)
}
if (panelsMissing_PP_BSM) {
    api.addOrUpdate("MLTV4", panelsMissing_PP_BSM)
}
if (panelsMissing_PP_PMQ) {
    api.addOrUpdate("MLTV4", panelsMissing_PP_PMQ)
}
if (panelsMissing_BSM_PMQ) {
    api.addOrUpdate("MLTV4", panelsMissing_BSM_PMQ)
}
if (panelsMissing_PP) {
    api.addOrUpdate("MLTV4", panelsMissing_PP)
}
if (panelsMissing_BSM) {
    api.addOrUpdate("MLTV4", panelsMissing_BSM)
}
if (panelsMissing_PMQ) {
    api.addOrUpdate("MLTV4", panelsMissing_PMQ)
}