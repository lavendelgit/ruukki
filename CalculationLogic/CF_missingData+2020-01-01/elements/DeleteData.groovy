api.global.tableid = api.findLookupTable("MissingData")?.getAt("id")
api.massDelete("MLTV4", Filter.equal("lookupTable.id", api.global.tableid))