List weightBasedMissingProducts = out.WeightBasedMissingProducts ?: []
List weightBasedMissing_PWM_BSM = out.WeightBasedMissing_PWM_BSM ?: []
List weightBasedMissing_PWM_PMQ = out.WeightBasedMissing_PWM_PMQ ?: []
List weightBasedMissing_BSM_PMQ = out.WeightBasedMissing_BSM_PMQ ?: []
List weightBasedMissing_PWM = out.WeightBasedMissing_PWM ?: []
List weightBasedMissing_BSM = out.WeightBasedMissing_BSM ?: []
List weightBasedMissing_PMQ = out.WeightBasedMissing_PMQ ?: []
if (weightBasedMissingProducts) {
    api.addOrUpdate("MLTV4", weightBasedMissingProducts)
}
if (weightBasedMissing_PWM_BSM) {
    api.addOrUpdate("MLTV4", weightBasedMissing_PWM_BSM)
}
if (weightBasedMissing_PWM_PMQ) {
    api.addOrUpdate("MLTV4", weightBasedMissing_PWM_PMQ)
}
if (weightBasedMissing_BSM_PMQ) {
    api.addOrUpdate("MLTV4", weightBasedMissing_BSM_PMQ)
}
if (weightBasedMissing_PWM) {
    api.addOrUpdate("MLTV4", weightBasedMissing_PWM)
}
if (weightBasedMissing_BSM) {
    api.addOrUpdate("MLTV4", weightBasedMissing_BSM)
}
if (weightBasedMissing_PMQ) {
    api.addOrUpdate("MLTV4", weightBasedMissing_PMQ)
}