Map panelsMaster = api.global.panelsMaster
List panelsPMQ = out.PMQuantitySegments as List
List result = []

panelsMaster?.each { sku, value ->
    for (market in value?.markets) {
        if (market + ":" + sku in panelsPMQ || market + ":" + value?.baseProduct in panelsPMQ || market + ":" + value?.category in panelsPMQ) {
            continue
        }
        if (sku + ":" + market + ":" + value?.category in api.local.panelBasedMissedProducts) {
            continue
        }
        api.local.panelBasedMissedProducts << sku + ":" + market + ":" + value?.category
        result += ["lookupTableId"  : api.global.tableid,
                   "lookupTableName": "MissingData",
                   "key1"           : sku,
                   "key2"           : market,
                   "key3"           : "PanelsRule",
                   "key4"           : "PM_QuantitySegments",
                   "attribute1"     : value?.category,
                   "attribute2"     : api.product("Name", sku)]
    }
}

return result