List productgroups = out.PricingRules?.PanelsPricing
List panelPriceData = []
api.global.panelsMaster = out.ProductMaster?.subMap(productgroups)?.values()?.collectEntries()
List panelRule = api.global.panelsMaster?.keySet() as List

List filters = [Filter.like("sku", "P%")]

Iterator panelData = api.stream("PX20", null, ["sku"], true, Filter.equal("name", "PanelsPrices"),
        Filter.or(*filters))
panelPriceData = panelData?.collect { it.sku }
panelData?.close()

List missingData = (panelRule - panelPriceData)

return missingData