List userInputPriceRuleBasedMissingProducts = out.UserInputPriceRuleBasedMissingProducts ?: []
List userInputPriceRuleMissing_BSM = out.UserInputPriceRuleMissing_BSM ?: []
List userInputPriceRuleMissing_PMQ = out.UserInputPriceRuleMissing_PMQ ?: []
if (userInputPriceRuleBasedMissingProducts) {
    api.addOrUpdate("MLTV4", userInputPriceRuleBasedMissingProducts)
}
if (userInputPriceRuleMissing_BSM) {
    api.addOrUpdate("MLTV4", userInputPriceRuleMissing_BSM)
}
if (userInputPriceRuleMissing_PMQ) {
    api.addOrUpdate("MLTV4", userInputPriceRuleMissing_PMQ)
}