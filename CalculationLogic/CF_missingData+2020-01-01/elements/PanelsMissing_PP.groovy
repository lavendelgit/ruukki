Map panelsMaster = api.global.panelsMaster
List panelsPP = out.PanelsPP as List
List result = []

panelsMaster?.each { sku, value ->
    if (sku in panelsPP) {
        for (market in value?.markets) {
            if (sku + ":" + market + ":" + value?.category in api.local.panelBasedMissedProducts) {
                continue
            }
            api.local.panelBasedMissedProducts << sku + ":" + market + ":" + value?.category
            result += ["lookupTableId"  : api.global.tableid,
                       "lookupTableName": "MissingData",
                       "key1"           : sku,
                       "key2"           : market,
                       "key3"           : "PanelsRule",
                       "key4"           : "PanelsPrices",
                       "attribute1"     : value?.category,
                       "attribute2"     : api.product("Name", sku)]
        }
    }
}

return result