List productgroups = out.PricingRules?.materialBasedRule
List claddingData = []

api.global.materialBasedRuleMaster = out.ProductMaster?.subMap(productgroups)?.values()?.collectEntries()
List materialBasedRule = api.global.materialBasedRuleMaster?.keySet() as List

List filters = [Filter.like("sku", "C%")
                , Filter.like("sku", "L%")]

Iterator claddingDataStream = api.stream("PX30", null, ["sku"], true,
        Filter.equal("name", "CladdingProcessingCost"), Filter.or(*filters))
claddingData = claddingDataStream?.collect { it.sku }
claddingDataStream?.close()

List missingData = (materialBasedRule - claddingData)

return missingData