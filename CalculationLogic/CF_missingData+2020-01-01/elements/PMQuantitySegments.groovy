List availableProducts = api.stream("MLTV3", null, Filter.equal("lookupTable.uniqueName", "PM_QuantitySegments"))?.withCloseable {
    it.collect { it.key1 + ":" + it.key2 }
}
return availableProducts?.unique()