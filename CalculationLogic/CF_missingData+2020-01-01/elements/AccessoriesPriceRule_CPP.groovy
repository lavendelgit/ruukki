Map accessoriesPriceBasedRuleMaster = api.global.accessoriesPriceBasedRuleMaster
List accessoriesBasedRuleCPP = out.AccessoriesPriceRuleCPP as List
List result = []

accessoriesPriceBasedRuleMaster?.each { sku, value ->
    if (sku in accessoriesBasedRuleCPP) {
        for (market in value?.markets) {
            if (sku + ":" + market + ":" + value?.category in api.local.accessoriesBasedMissedProducts) {
                continue
            }
            api.local.accessoriesBasedMissedProducts << sku + ":" + market + ":" + value?.category
            result += ["lookupTableId"  : api.global.tableid,
                       "lookupTableName": "MissingData",
                       "key1"           : sku,
                       "key2"           : market,
                       "key3"           : "AccessoriesPriceRule",
                       "key4"           : "ComponentProductPrices",
                       "attribute1"     : value?.category,
                       "attribute2"     : api.product("Name", sku)]
        }
    }
}

return result