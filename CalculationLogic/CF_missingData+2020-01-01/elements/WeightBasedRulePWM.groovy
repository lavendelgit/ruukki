List weightData = []
List productgroups = out.PricingRules?.WeightBasedRule

api.global.weightBasedRuleMaster = out.ProductMaster?.subMap(productgroups)?.values()?.collectEntries()
List weightBasedRule = api.global.weightBasedRuleMaster?.keySet() as List

List filters = [Filter.like("sku", "D%")
                , Filter.like("sku", "B%")
                , Filter.like("sku", "A%")
                , Filter.like("sku", "R%")
                , Filter.like("sku", "FS%")
                , Filter.like("sku", "U%")
                , Filter.like("sku", "S%")]

Iterator productweightData = api.stream("PX20", null, ["sku"], true,
        Filter.equal("name", "ProductWeightMarkups"), Filter.or(*filters))
weightData = productweightData?.collect { it.sku }
productweightData?.close()

List missingData = (weightBasedRule - weightData)

return missingData