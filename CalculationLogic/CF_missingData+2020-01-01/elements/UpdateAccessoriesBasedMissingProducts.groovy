List accessoriesPriceRuleMissingProducts = out.AccessoriesPriceRuleMissingProducts ?: []
List accessoriesPriceRule_CPP_BSM = out.AccessoriesPriceRule_CPP_BSM ?: []
List accessoriesPriceRule_CPP_PMQ = out.AccessoriesPriceRule_CPP_PMQ ?: []
List accessoriesPriceRule_BSM_PMQ = out.AccessoriesPriceRule_BSM_PMQ ?: []
List accessoriesPriceRule_CPP = out.AccessoriesPriceRule_CPP ?: []
List accessoriesPriceRule_BSM = out.AccessoriesPriceRule_BSM ?: []
List accessoriesPriceRule_PMQ = out.AccessoriesPriceRule_PMQ ?: []
if (accessoriesPriceRuleMissingProducts) {
    api.addOrUpdate("MLTV4", accessoriesPriceRuleMissingProducts)
}
if (accessoriesPriceRule_CPP_BSM) {
    api.addOrUpdate("MLTV4", accessoriesPriceRule_CPP_BSM)
}
if (accessoriesPriceRule_CPP_PMQ) {
    api.addOrUpdate("MLTV4", accessoriesPriceRule_CPP_PMQ)
}
if (accessoriesPriceRule_BSM_PMQ) {
    api.addOrUpdate("MLTV4", accessoriesPriceRule_BSM_PMQ)
}
if (accessoriesPriceRule_CPP) {
    api.addOrUpdate("MLTV4", accessoriesPriceRule_CPP)
}
if (accessoriesPriceRule_BSM) {
    api.addOrUpdate("MLTV4", accessoriesPriceRule_BSM)
}
if (accessoriesPriceRule_PMQ) {
    api.addOrUpdate("MLTV4", accessoriesPriceRule_PMQ)
}