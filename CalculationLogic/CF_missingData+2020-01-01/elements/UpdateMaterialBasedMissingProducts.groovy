List materialBasedMissingProducts = out.MaterialBasedMissingProducts ?: []
List materialBasedRuleMissing_CPC_BSM = out.MaterialBasedRuleMissing_CPC_BSM ?: []
List materialBasedRuleMissing_CPC_PMQ = out.MaterialBasedRuleMissing_CPC_PMQ ?: []
List materialBasedRuleMissing_BSM_PMQ = out.MaterialBasedRuleMissing_BSM_PMQ ?: []
List materialBasedRuleMissing_CPC = out.MaterialBasedRuleMissing_CPC ?: []
List materialBasedRuleMisssing_BSM = out.MaterialBasedRuleMisssing_BSM ?: []
List materialBasedRuleMissing_PMQ = out.MaterialBasedRuleMissing_PMQ ?: []
if (materialBasedMissingProducts) {
    api.addOrUpdate("MLTV4", materialBasedMissingProducts)
}
if (materialBasedRuleMissing_CPC_BSM) {
    api.addOrUpdate("MLTV4", materialBasedRuleMissing_CPC_BSM)
}
if (materialBasedRuleMissing_CPC_PMQ) {
    api.addOrUpdate("MLTV4", materialBasedRuleMissing_CPC_PMQ)
}
if (materialBasedRuleMissing_BSM_PMQ) {
    api.addOrUpdate("MLTV4", materialBasedRuleMissing_BSM_PMQ)
}
if (materialBasedRuleMissing_CPC) {
    api.addOrUpdate("MLTV4", materialBasedRuleMissing_CPC)
}
if (materialBasedRuleMisssing_BSM) {
    api.addOrUpdate("MLTV4", materialBasedRuleMisssing_BSM)
}
if (materialBasedRuleMissing_PMQ) {
    api.addOrUpdate("MLTV4", materialBasedRuleMissing_PMQ)
}