Map weightBasedRuleMaster = api.global.weightBasedRuleMaster
List weightBasedRuleBSM = out.BaseSalesMarkups as List
List weightBasedRulePMQ = out.PMQuantitySegments as List
List result = []

weightBasedRuleMaster?.each { sku, value ->
    for (market in value?.markets) {
        if (market + ":" + sku in weightBasedRuleBSM || market + ":" + value?.baseProduct in weightBasedRuleBSM || market + ":" + value?.category in weightBasedRuleBSM) {
            continue
        }
        if (market + ":" + sku in weightBasedRulePMQ || market + ":" + value?.baseProduct in weightBasedRulePMQ || market + ":" + value?.category in weightBasedRulePMQ) {
            continue
        }
        if (sku + ":" + market + ":" + value?.category in api.local.weightBasedMissedProducts) {
            continue
        }
        api.local.weightBasedMissedProducts << sku + ":" + market + ":" + value?.category
        result += ["lookupTableId"  : api.global.tableid,
                   "lookupTableName": "MissingData",
                   "key1"           : sku,
                   "key2"           : market,
                   "key3"           : "WeightBasedRule",
                   "key4"           : "BaseSalesMarkup,PM_QuantitySegments",
                   "attribute1"     : value?.category,
                   "attribute2"     : api.product("Name", sku)]
    }
}

return result