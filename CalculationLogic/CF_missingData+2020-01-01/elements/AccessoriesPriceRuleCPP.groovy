List productgroups = out.PricingRules?.AccessoriesPriceRule
api.global.accessoriesPriceBasedRuleMaster = out.ProductMaster?.subMap(productgroups)?.values()?.collectEntries()
List accessoryRule = []
List pricesData = []
api.global.accessoriesPriceBasedRuleMaster?.each { sku, values ->
    accessoryRule += values?.markets.collect { market -> sku + ":" + market
    }
}

List filters = [Filter.like("sku", "O%"),
                Filter.like("sku", "F%"),
                Filter.like("sku", "I%"),
                Filter.like("sku", "G%"),
                Filter.like("sku", "E%")]

Iterator productpricesData = api.stream("PX20", "-lastUpdateDate", ["sku", "attribute3"], true, Filter.equal("name", "ComponentsProductPrices"),
        Filter.isNotEmpty("attribute10"), Filter.or(*filters))
pricesData = productpricesData?.collect { it.sku + ":" + it.attribute3 }
productpricesData?.close()

List missingData = (accessoryRule - pricesData)

return missingData