Map productMaster = api.global.userInputPriceRuleMaster
List userInputPriceRuleBasedRuleBSM = out.BaseSalesMarkups as List
List userInputPriceRuleBasedRulePMQ = out.PMQuantitySegments as List
api.local.userInputPriceMissedProducts = []
List result = []

productMaster?.each { sku, value ->
    for (market in value?.markets) {
        if (market + ":" + sku in userInputPriceRuleBasedRuleBSM || market + ":" + value?.baseProduct in userInputPriceRuleBasedRuleBSM || market + ":" + value?.category in userInputPriceRuleBasedRuleBSM) {
            continue
        }
        if (market + ":" + sku in userInputPriceRuleBasedRulePMQ || market + ":" + value?.baseProduct in userInputPriceRuleBasedRulePMQ || market + ":" + value?.category in userInputPriceRuleBasedRulePMQ) {
            continue
        }
        if (sku + ":" + market + ":" + value?.category in api.local.userInputPriceMissedProducts) {
            continue
        }
        api.local.userInputPriceMissedProducts << sku + ":" + market + ":" + value?.category
        result += ["lookupTableId"  : api.global.tableid,
                   "lookupTableName": "MissingData",
                   "key1"           : sku,
                   "key2"           : market,
                   "key3"           : "UserInputPriceCalculations",
                   "key4"           : "UserInputPriceBasedMissingProduct",
                   "attribute1"     : value?.category,
                   "attribute2"     : api.product("Name", sku)]
    }
}

return result