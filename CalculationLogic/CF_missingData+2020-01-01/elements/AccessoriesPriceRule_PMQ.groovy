Map accessoriesPriceBasedRuleMaster = api.global.accessoriesPriceBasedRuleMaster
List accessoriesPriceRulePMQ = out.PMQuantitySegments as List
List result = []

accessoriesPriceBasedRuleMaster?.each { sku, value ->
    for (market in value?.markets) {
        if (market + ":" + sku in accessoriesPriceRulePMQ || market + ":" + value?.baseProduct in accessoriesPriceRulePMQ || market + ":" + value?.category in accessoriesPriceRulePMQ) {
            continue
        }
        if (sku + ":" + market + ":" + value?.category in api.local.accessoriesBasedMissedProducts) {
            continue
        }
        api.local.accessoriesBasedMissedProducts << sku + ":" + market + ":" + value?.category
        result += ["lookupTableId"  : api.global.tableid,
                   "lookupTableName": "MissingData",
                   "key1"           : sku,
                   "key2"           : market,
                   "key3"           : "AccessoriesPriceRule",
                   "key4"           : "PM_QuantitySegments",
                   "attribute1"     : value?.category,
                   "attribute2"     : api.product("Name", sku)]
    }
}

return result