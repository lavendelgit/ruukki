Map materialBasedRuleMaster = api.global.materialBasedRuleMaster
List materialBasedRuleCPC = out.MaterialBasedRuleCPC as List
List materialBasedRuleBSM = out.BaseSalesMarkups as List
List result = []

materialBasedRuleMaster?.each { sku, value ->
    if (sku in materialBasedRuleCPC) {
        for (market in value?.markets) {
            if (market + ":" + sku in materialBasedRuleBSM || market + ":" + value?.baseProduct in materialBasedRuleBSM || market + ":" + value?.category in materialBasedRuleBSM) {
                continue
            }
            if (sku + ":" + market + ":" + value?.category in api.local.materialBasedMissedProducts) {
                continue
            }
            api.local.materialBasedMissedProducts << sku + ":" + market + ":" + value?.category
            result += ["lookupTableId"  : api.global.tableid,
                       "lookupTableName": "MissingData",
                       "key1"           : sku,
                       "key2"           : market,
                       "key3"           : "materialBasedRule",
                       "key4"           : "CladdingProcessingCost,BaseSalesMarkup",
                       "attribute1"     : value?.category,
                       "attribute2"     : api.product("Name", sku)]
        }
    }
}

return result