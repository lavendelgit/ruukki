Map materialBasedRuleMaster = api.global.materialBasedRuleMaster
List materialBasedRulePMQ = out.PMQuantitySegments as List
List result = []

materialBasedRuleMaster?.each { sku, value ->
    for (market in value?.markets) {
        if (market + ":" + sku in materialBasedRulePMQ || market + ":" + value?.baseProduct in materialBasedRulePMQ || market + ":" + value?.category in materialBasedRulePMQ) {
            continue
        }
        if (sku + ":" + market + ":" + value?.category in api.local.materialBasedMissedProducts) {
            continue
        }
        api.local.materialBasedMissedProducts << sku + ":" + market + ":" + value?.category
        result += ["lookupTableId"  : api.global.tableid,
                   "lookupTableName": "MissingData",
                   "key1"           : sku,
                   "key2"           : market,
                   "key3"           : "materialBasedRule",
                   "key4"           : "PM_QuantitySegments",
                   "attribute1"     : value?.category,
                   "attribute2"     : api.product("Name", sku)]
    }
}

return result