LinkedHashMap prodMarketMaster = [:]

ArrayList variantProductGroups = ["PANEL", "LOAD_BEARING_PROFILE", "PROFILE", "PURLIN", "FIREWALL", "FASTENER"]

Iterator streamedData = api.stream("P", null, ["sku", "attribute1", "attribute3", "attribute4", "attribute5", "attribute6"],
        Filter.isNotNull("attribute4"),
        Filter.isNotNull("attribute5"),
        Filter.isNotEmpty("attribute5"),
        Filter.equal("attribute7", "approved"))

prodMarketMaster = libs?.RukkiCommonLib?.MissingProductsUtils?.removeBaseProductsFromProductMarketMaster(streamedData, variantProductGroups)

return prodMarketMaster