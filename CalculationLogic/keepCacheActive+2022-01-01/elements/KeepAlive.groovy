def cacheDetails = api.findLookupTableValues("CacheOptions", api.filter("name", "=", "OptionsPrice"))
def logicName = cacheDetails[0].getAt("attribute1")
def timeZone = cacheDetails[0].getAt("attribute2")
def startTime = cacheDetails[0].getAt("attribute3")
def endTime = cacheDetails[0].getAt("attribute4")
def count = cacheDetails[0].getAt("attribute5")

def now = new DateTime()
def cacheTimeZone = api.getTimeZone(timeZone)
def currentTime = now.withZone(cacheTimeZone).toString("HH:mm:ss")
int numOfCacheCalls = 0

if ((currentTime >= startTime) && (currentTime <= endTime))  {
    def reqBody = [
            [
                    "data": [
                            "logicName"      : "productPrice",
                            "market"         : "PL",
                            "productCode"    : "P00001",
                            "quantity"       : 10.0,
                            "unitOfMeasure"  : "M2",
                            "customerClass"  : "111",
                            "customerSegment": "RC21",
                            "selectedOptions": [],
                            "currency"       : "PLN",
                            "language"       : "en",
                            "creationDate"   : "",
                            "freightPrice"   : 0.0,
                            "lineItemNumber" : 1,
                            "pricingDate"    : "2021-01-17"
                    ]
            ]
    ]
    for(i=0;i < count;i++) {
       def bc = api.boundCall("production", "/formulamanager.executeformulaservice/" + logicName, api.jsonEncode(reqBody) as String, true)
        numOfCacheCalls = numOfCacheCalls + 1
    }
//    api.trace("bc", bc)
   api.logInfo("*** Keep Threads Active Response *** "+" with " +numOfCacheCalls+ " number of Cache calls made with PP Start time as "+startTime+", PP End Time as "+endTime+" and Current Time as "+currentTime)
}
else{
    api.logInfo("********* Keep Threads beyond end time  *********** PP Start time: "+startTime+" *** PP End Time: "+endTime+" *** Current Time: "+currentTime)

}