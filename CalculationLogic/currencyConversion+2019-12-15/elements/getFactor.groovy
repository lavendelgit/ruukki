def main(String SourceCurrency, String DestinationCurrency) {
  
  if ( api.isSyntaxCheck() ) {
    return
  }

  
  try {
    
    
      //api.logInfo('CURRENCY CONVERSION')
    
      def conversion_factor = api.vLookup( 'Rates', 'ConversionFactor', SourceCurrency, DestinationCurrency )
    
      // If we fail to get a conversion factor, return 1
      if ( !conversion_factor ) {
        
          //api.logInfo("Failed to get conversion fACTOR -- ${conversion_factor}")

          return new BigDecimal("1")

      }
        
      return conversion_factor
    
  } catch (e) {
    
    //api.logInfo("Currency conversion module has failed  - ${e}")
    
     return new BigDecimal("1")
    
  }

}
