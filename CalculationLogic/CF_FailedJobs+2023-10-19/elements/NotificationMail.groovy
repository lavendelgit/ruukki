import java.time.LocalDate
import java.time.DayOfWeek

Calendar calendar = Calendar.getInstance()
Filter filter = Filter.and(Filter.equal("status", "FAILED"),
        Filter.notEqual("jobName", "keepCacheActive"),
        Filter.greaterOrEqual("lastUpdateDate", calendar.getTime() - 1))
List jobNames = api.find("JST", filter)?.jobName
jobNames = jobNames?.unique()

if (!jobNames || jobNames?.size() <= 0) {
    return
}

def headerMessage = "<p><br/>Dear team,<br/><br/>Please find below the list of jobs that was failed in RUUKKI Production environment.</p>"
def footerMessage = "<br/><br/><i>Please do not reply to this email as it is an automated email from Pricefx</i><br/><br/>Best Regards,<br/>Pricefx team!"
String jobNamesInBullets = "<ol>\n"
jobNames.each {
    jobNamesInBullets += "<li>" + it + "</li>"
}
jobNamesInBullets += "</ol>"
def finalMessage = headerMessage + jobNamesInBullets + footerMessage

String currentDay = LocalDate.now().dayOfWeek as String
if (currentDay == "SATURDAY" || currentDay == "SUNDAY") {
    return
} else {
    List userGroups = api.find("U", Filter.equal("groups.uniqueName", "NotifyUsers"))?.email
    userGroups?.each {
        api.sendEmail(it, "Job Failure Notification", finalMessage)
    }
    return
}
