if (!api.local.priceLogic) {
    return
}
// RUUK-112 changes
if (api.local.componentProductPrices || api.local.isPanelPricing) {

    if (api.local.product.isAccessory == true) {
        return new BigDecimal("0")
    }
    // Only get default options if there aren't any options already
    api.local.product.options = api.local.product.options.size() < 1 ? out.getDefaultOptions : api.local.product.options

    if (api.local.product.options.size() == 0) {

        if (api.local.product.isProductWithoutGroup) {

            return new BigDecimal("0")

        } else {

            def error = "getOptionsPrice - No options found for product ${api.local.product.productCode}."
            api.local.warnings.add(error)

            return new BigDecimal("0")

        }

    } else {

        try {

            BigDecimal options_price

            // For special options cases, we need to convert back from received currency to EUR before we can sum them inside the othre options prices
            // the conversion back to requested currency is handled later in the logic but all the monetary values in pricefx tables are always in EUR
            //
            def options_special = api.local.product.options?.findAll { it.special == true }
            // RUUK-34 code change starts
            // if special == true and optionGroupKey = dimensions, we are returning the weighted average

            def options_dimensions = options_special?.findAll { it.optionGroupKey == "dimensions" }
            if (options_special) {

                def options_special_converted_to_eur = api.local.product.options.collect {
                    it << [
                            price         : (it.special == true && api.local.product.currency != 'EUR') ? (it.price * (1 / api.local.conversionFactor)) : it.price,
                            qty           : it.qty,
                            optionGroupKey: it.optionGroupKey
                    ]
                }

                if (options_dimensions) {
                    def options_dimensions_multiply_by_qty = options_special_converted_to_eur.collect {
                        it << [
                                price: it.optionGroupKey == "dimensions" ? (it.price * it.qty) / options_dimensions.sum { it.qty } : it.price
                        ]
                    }
                    def weightedAvg = options_dimensions_multiply_by_qty.sum { it.price }
                    return weightedAvg
                }

                options_price = options_special_converted_to_eur.sum { it.price }

                return options_price
            }
// RUUK-34 userstory changes end here
            // Else we just summ the prices of all options
            options_price = api.local.product.options.sum { it.price }

            return options_price

        } catch (e) {

            def error = "Missing some options prices for product ${api.local.product.productCode}."

            api.local.warnings.add(error)

            return new BigDecimal("0")

        }
    }
} else {
    if (api.local.isCladding || api.local.isUserinputCalc ) {
        options_price = api.local.product.options?.findAll { it.optionGroupKey == "external" && it.price > 0 }.collect { it.price }.sum()
        return options_price ?: 0.0
    } else if (api.local.isCladding && out.SpecialMaterial == true) {
        return new BigDecimal("0")
    } else if (api.local.isWeightedRule || api.local.isAccessoryPriceRule) {
        return new BigDecimal("0")
    }
}


