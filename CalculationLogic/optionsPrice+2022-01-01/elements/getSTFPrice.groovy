if (!api.local.priceLogic) {
    return
}


if (api.local.componentProductPrices) {

    try {

        def final_base_price = out.getFinalBasePrice

        def stf_multipliers = out.getTotalSTF

        if (!stf_multipliers) {

            def error = "Cannot compute STF prices with missing multipliers for product ${api.local.product.productCode}."
            api.local.warnings.add(error)
            return null
        }

        if (!final_base_price) {

            def error = "Missing base price for product ${api.local.product.productCode}."
            api.local.warnings.add(error)
            return null

        }

        // Muliply the total price for each stf
        def stf_prices = stf_multipliers.collect {
            [segment: it.segment
             , type : it.type
             , price: it.markup * final_base_price]
        }

        return stf_prices

    } catch (e) {

        def error = "Error calculating STF prices for product ${api.local.product.productCode} - ${e}."
        api.local.warnings.add(error)

        return null

    }


} else {

    try {

        // Options prices is always 0 for accessories
        def final_base_price = out.getFinalBasePrice
        def stf_multipliers = out.getTotalSTF


        if (!stf_multipliers) {

            def error = "Cannot compute STF prices with missing multipliers for product ${api.local.product.productCode}."
            api.local.warnings.add(error)
            return null
        }

        if (final_base_price == null) {
            def error = "Missing base price for product ${api.local.product.productCode}."
            api.local.warnings.add(error)
            return null
        }
        List stf_prices = []

        if (api.local.product.market != "FI") {
            Map segmentGroup = stf_multipliers.groupBy { it.segment }

            segmentGroup.each { segment, rows ->
                Map typeGroup = rows.groupBy { it.type }

                def target = typeGroup.target?.getAt(0)
                def salesTargetPrice
                if (out.CladdingPrices) {
                    BigDecimal freight_price = out?.getFreightPrice
                    if (api.local.claddingBasePrice != null && freight_price != null) {
                        api.local.claddingBasePrice = api.local.claddingBasePrice.add(freight_price).setScale(3, BigDecimal.ROUND_HALF_UP)
                    }
                    salesTargetPrice = (api.local.claddingBasePrice < out.CladdingPrices?.stfprices?.basePrice) ? (out.CladdingPrices?.stfprices?.target) * (1 + (target?.markup)) : (api.local.claddingBasePrice) * (1 + (target?.markup)) * ((1.0 + (api.local.basemarkupdata?.attribute5)))
                }
                if (out.AccessoriesPrices) {
                    salesTargetPrice = (out.getFinalBasePrice < out.AccessoriesPrices?.stfprices?.basePrice) ? (out.AccessoriesPrices?.stfprices?.target) * (1 + (target?.markup)) : (out.getFinalBasePrice) * (1 + (target?.markup)) * ((1.0 + (api.local.basemarkupdata?.attribute5)))
                }
                if (out.getProductPrices && !api.local.componentProductPrices) {
                    if(api.local.isWeightedRule && out.SpecialMaterial == false)
                    {
                        BigDecimal freight_price = out?.getFreightPrice
                        if (api.local.weightBasedBasePrice != null && freight_price != null) {
                            api.local.weightBasedBasePrice = api.local.weightBasedBasePrice.add(freight_price).setScale(3, BigDecimal.ROUND_HALF_UP)
                        }
                        salesTargetPrice = (api.local.weightBasedBasePrice < out.getProductPrices?.stfprices?.basePrice) ? (out.getProductPrices?.stfprices?.target) * (1 + (target?.markup)) : (api.local.weightBasedBasePrice) * (1 + (target?.markup)) * ((1.0 + (api.local.basemarkupdata?.attribute5)))
                    }
                    else {
                        salesTargetPrice = (out.getFinalBasePrice < out.getProductPrices?.stfprices?.basePrice) ? (out.getProductPrices?.stfprices?.target) * (1 + (target?.markup)) : (out.getFinalBasePrice) * (1 + (target?.markup)) * ((1.0 + (api.local.basemarkupdata?.attribute5)))
                    }
                }
                if (out.UserinputPrices) {
                    salesTargetPrice = (out.getFinalBasePrice < out.UserinputPrices?.stfprices?.basePrice) ? (out.UserinputPrices?.stfprices?.target) * (1 + (target?.markup ?: 0.0)) : (out.getFinalBasePrice) * (1 + (target?.markup ?: 0.0)) * ((1.0 + (api.local.basemarkupdata?.attribute5 ?: 0.0)))
                }
                if (out.PanelsPrices) {
                    BigDecimal freight_price = out?.getFreightPrice
                    if (api.local.panelBasePrice != null && freight_price != null) {
                        api.local.panelBasePrice = api.local.panelBasePrice.add(freight_price).setScale(3, BigDecimal.ROUND_HALF_UP)
                    }
                    salesTargetPrice = (api.local.panelBasePrice < out.PanelsPrices?.stfprices?.basePrice) ? (out.PanelsPrices?.stfprices?.target) * (1 + (target?.markup ?: 0.0)) : (api.local.panelBasePrice) * (1 + (target?.markup ?: 0.0)) * ((1.0 + (api.local.basemarkupdata?.attribute5 ?: 0.0)))
                }

                stf_prices << [segment: segment,
                               type   : "target",
                               price  : salesTargetPrice]
                def floor = typeGroup.floor?.getAt(0)
                stf_prices << [segment: segment,
                               type   : "floor",
                               price  : (salesTargetPrice ?: 0.0) * (1 - (floor?.markup ?: 0.0))]
                def start = typeGroup.start?.getAt(0)
                stf_prices << [segment: segment,
                               type   : "start",
                               price  : (salesTargetPrice ?: 0.0) * (1 + (start?.markup ?: 0.0))]
            }
        } else {

            if ((api.local.isCladding && api.local.optionsPrice?.rawMaterialPrice != 0.0 && api.local.optionsPrice?.rawMaterialPrice != null)) {

                out.CladdingPrices?.stfprices?.each { it ->

                    stf_prices << [segment: it.key.split('_')[1],
                                   type   : it.key.split('_')[0],
                                   price  : it.value]
                }
            } else if (api.local.accessoryCogs) {
                out.AccessoriesPrices.stfprices?.each { it ->
                    stf_prices << [segment: it.key.split('_')[1],
                                   type   : it.key.split('_')[0],
                                   price  : it.value]
                }
            } else if (api.local.isCladding && out.SpecialMaterial == true && api.local.product.rawMaterialCost == 0.0 && api.local.product.processingCost == 0.0) {
                out.CladdingPrices?.stfprices?.each { it ->

                    stf_prices << [segment: it.key.split('_')[1],
                                   type   : it.key.split('_')[0],
                                   price  : it.value]
                }
            } else if (api.local.cladding && api.local.specialMaterial == true && api.local.product.rawMaterialCost > 0.0 && api.local.product.processingCost > 0.0) {

                out.UserinputPrices?.stfprices?.each { it ->

                    stf_prices << [segment: it.key.split('_')[1],
                                   type   : it.key.split('_')[0],
                                   price  : it.value]
                }

            } else if (api.local.userInputPrices) {

                out.UserinputPrices.stfprices?.each { it ->
                    stf_prices << [segment: it.key.split('_')[1],
                                   type   : it.key.split('_')[0],
                                   price  : it.value]
                }
            } else if (api.local.panelsPricing) {
                out.PanelsPrices.stfprices?.each { it ->
                    stf_prices << [segment: it.key.split('_')[1],
                                   type   : it.key.split('_')[0],
                                   price  : it.value]
                }


            } else {
                out.getProductPrices?.stfprices?.each { it ->

                    stf_prices << [segment: it.key.split('_')[1],
                                   type   : it.key.split('_')[0],
                                   price  : it.value]
                }
            }
        }


        return stf_prices

    } catch (e) {

        def error = "Error calculating STF prices for product ${api.local.product.productCode} - ${e}."
        api.local.warnings.add(error)

        return null

    }

}