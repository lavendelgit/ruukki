if (!api.local.priceLogic) {
    return
}


if (api.local.isUserinputCalc || (api.local.product.rawMaterialCost > 0.0 && api.local.product.processingCost > 0.0) || (api.local.cladding && api.local.specialMaterial == true && (api.local.product.rawMaterialCost > 0.0 && api.local.product.processingCost > 0.0)) || (api.local.isWeightedRule && api.local.product.productType == "DESIGN_PROFILE" && out.SpecialMaterial == true && api.local.product.rawMaterialCost > 0.0 && api.local.product.processingCost > 0.0)) {

    api.local.userInputPrices = true
    def prices = {}

    api.local.product.rawMaterialCost = (api.local.product.rawMaterialCost) +(api.local.category2_Optionsprice ?: 0)
    api.local.product.processingCost = (api.local.product.processingCost) +(api.local.category1_Optionsprice ?: 0)

    api.local.product.cogsPrice = api.local.product.rawMaterialCost + api.local.product.processingCost
    api.local.transferPrice = (api.local.product.processingCost && api.local.product.rawMaterialCost) ? (api.local.product.processingCost * (api.local.factoryMarkup ?: 0.0)) + api.local.product.rawMaterialCost : 0.0
    prices.basePrice = (api.local.product.cogsPrice) * (1.0 + (api.local.basemarkupdata?.attribute1 ?: 0.0))

    BigDecimal targetprice
    if (api.local.product.market != "FI") {

        targetprice = (prices?.basePrice as BigDecimal) * ((1.0 + (api.local.basemarkupdata?.attribute5 ?: 0.0)) as BigDecimal)

        BigDecimal startprice = ((targetprice as BigDecimal) * ((1.0 + (api.local.basemarkupdata?.attribute2 ?: 0.0)) as BigDecimal))

        BigDecimal floorprice = (targetprice as BigDecimal) * (1.0 - (api.local.basemarkupdata?.attribute4 ?: 0.0) as BigDecimal)

        prices.stfprices = [start   : startprice
                            , target: targetprice
                            , floor : floorprice]

        return prices
    } else if (api.local.product.market == "FI") {


        def targetA8 = (prices?.basePrice as BigDecimal) * ((1.0 + (api.local.basemarkupdata?.attribute5 ?: 0.0)) as BigDecimal)
        def targetA1 = (prices?.basePrice as BigDecimal) * (1.0 + api.local.pmQuantityMarkupdata?.attribute3 ?: 0.0) as BigDecimal

        prices.stfprices = [target_A1  : targetA1
                            , target_A8: targetA8
                            , floor_A1 : targetA1 * (1.0 - (api.local.basemarkupdata?.attribute4 ?: 0.0) as BigDecimal)
                            , floor_A8 : targetA8 * (1.0 - (api.local.basemarkupdata?.attribute4 ?: 0.0) as BigDecimal)
                            , start_A1 : targetA1 * (1.0 + (api.local.basemarkupdata?.attribute2 ?: 0.0) as BigDecimal)
                            , start_A8 : targetA8 * (1.0 + (api.local.basemarkupdata?.attribute2 ?: 0.0) as BigDecimal)]

        return prices
    }


}



