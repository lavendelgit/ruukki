if (!api.local.priceLogic) {
    return
}

if ((api.local.isCladding && api.local.optionsPrice?.rawMaterialPrice == 0.0) || (api.local.isCladding && api.local.basemarkupdata == null) || (api.local.pmQuantityMarkupdata == null && api.local.isCladding && api.local.product.market == "FI")) {
    return
}

//if ((api.local.product.rawMaterialCost > 0.0 && api.local.product.processingCost > 0.0)) {
//    return
//}

def priceDateInput = api.local.product.pricingDate

def prices = {}
def target_A1
def target_A8
def start_A1
def start_A8
def floor_A1
def floor_A8

if (api.local.isCladding && out.SpecialMaterial == false) {

    def inputArea
    BigDecimal cogsPrice = 0.0
    BigDecimal basePrice = 0.0
    if (api.local.isCladding && api.local.optionsPrice?.rawMaterialPrice != 0.0) {

        def options = api.local.product.options.findAll { it -> (it.optionGroupKey == "dimensions" && it.selected == true) && it.qty >= 0.0
        }

        inputArea = api.local.product.options?.findAll { it.optionGroupKey == "dimensions" }?.collect { it.totalArea }.sum()


        Map priceNotFound = options.find { option -> option.price == 0.0
        }
        if (priceNotFound) {
            prices.basePrice = 0.0
            api.local.weightedCOGS = 0.0
        }
// RUUK-85 : add transferPrice here
        options.collect { option ->
            option << [price        : option?.qty > 0.0 ? (option.price * option.qty) / options.sum { it.qty } : 0.0,
                       newPrice     : option?.qty > 0.0 && (option.newPrice != null) ? (option?.newPrice * option?.qty) / options.sum { it.qty } : 0.0,
                       transferPrice: option?.qty > 0.0 ? (((((option.processingCost ?: 0.0)  * (api.local.factoryMarkup?: 0.0) ) + ((option.rawMaterialCost)?: 0.0 )) * (option.qty)) / options.sum { it.qty }) : 0.0]
        }

        api.local.weightedCOGS = options.sum { it.price } ?: 0.0
        cogsPrice = options.sum { it.newPrice } ?: 0.0
        api.local.transferPrice = options.sum { it.transferPrice } ?: 0.0

    }

    prices.basePrice = api.local.weightedCOGS * (1.0 + (api.local.basemarkupdata?.attribute1))
    api.local.claddingBasePrice = cogsPrice * (1.0 + (api.local.basemarkupdata?.attribute1))
    BigDecimal targetprice

    if (api.local.product.market != "FI") {
        targetprice = (api.local.claddingBasePrice as BigDecimal) * ((1.0 + (api.local.basemarkupdata?.attribute5 ?: 0.0)) as BigDecimal)

        BigDecimal startprice = inputArea > 100 ? ((targetprice as BigDecimal) * ((1 + (api.local.basemarkupdata?.attribute2)) as BigDecimal)) : ((targetprice as BigDecimal) * (1 + (api.local.basemarkupdata?.attribute2)) + 5.0)
        BigDecimal floorprice = (targetprice as BigDecimal) * (1.0 - (api.local.basemarkupdata?.attribute4 ?: 0.0) as BigDecimal)
        prices.stfprices = [start   : startprice
                            , target: targetprice
                            , floor : floorprice]

        return prices
    } else if (api.local.product.market == "FI") {


        def targetA8 = (api.local.claddingBasePrice as BigDecimal) * ((1.0 + (api.local.basemarkupdata?.attribute5)) as BigDecimal)
        def targetA1 = (api.local.claddingBasePrice as BigDecimal) * (1.0 + api.local.pmQuantityMarkupdata?.attribute3) as BigDecimal

        prices.stfprices = [target_A1  : targetA1
                            , target_A8: targetA8
                            , floor_A1 : targetA1 * (1.0 - (api.local.basemarkupdata?.attribute4) as BigDecimal)
                            , floor_A8 : targetA8 * (1.0 - (api.local.basemarkupdata?.attribute4) as BigDecimal)
                            , start_A1 : inputArea > 100 ? targetA1 * (1.0 + (api.local.basemarkupdata?.attribute2) as BigDecimal) : (targetA1 * (1.0 + (api.local.basemarkupdata?.attribute2) as BigDecimal) + 5.0)
                            , start_A8 : inputArea > 100 ? targetA8 * (1.0 + (api.local.basemarkupdata?.attribute2) as BigDecimal) : (targetA8 * (1.0 + (api.local.basemarkupdata?.attribute2) as BigDecimal) + 5.0)]

        return prices
    }


}

if (api.local.isCladding && out.SpecialMaterial == true && api.local.product.rawMaterialCost == 0.0 && api.local.product.processingCost == 0.0) {

    api.local.weightedCOGS = 0
    prices.basePrice = 0.0
    if (api.local.product.market != "FI") {
        prices.stfprices = [start   : 0
                            , target: 0
                            , floor : 0]

    } else if (api.local.product.market == "FI") {
        prices.stfprices = [target_A1  : 0.0
                            , target_A8: 0.0
                            , floor_A1 : 0.0
                            , floor_A8 : 0.0
                            , start_A1 : 0.0
                            , start_A8 : 0.0]

    }

    return prices

}