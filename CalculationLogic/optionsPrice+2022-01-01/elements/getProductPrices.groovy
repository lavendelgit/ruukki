if (!api.local.priceLogic) {
    return
}
//if (api.local.isWeightedRule && api.local.product.rawMaterialCost > 0.0 && api.local.product.processingCost > 0.0 && api.local.basemarkupdata) {
//    return
//}

if (api.local.isUserinputCalc || api.local.userInputPrices) {
    return
}

/*
General remarks
----------------
- We use api.local.product variable for storing Product-related information
- This Pricing Logic isn't called within a specific Product context in the API so we always check out the Product first
- In the latest Groovy API release, we do not need to use api.getElement() to refer to the output of a logic element but we keep this because
  the code is clearer this way (see https://qa.pricefx.eu/pricefx-api/groovy/3.6/net/pricefx/formulaengine/scripting/PublicGroovyAPI.html#getElement-java.lang.String-)
*/

//RUUK-109: If producttype falls under fetchFromPricesGroups, then we fetch prices from Component Product Prices table, using the old approach.

def prices = getProductPrices()


if (api.local.product.options && ["FLASHING", "SHEETS", "SUPPORTSTUD"].contains(api.local.product.productType) && prices) {

    for (it in api.local.product.options) {
        def optionKeyValue = it.optionKey
        def groupKey_Value = it.groupKey
        def filter = [Filter.equal("key3", optionKeyValue),
                      Filter.equal("key1", api.local.product.productType),
                      Filter.equal("key2", groupKey_Value)]
        def percentage_Table = "FlashingPercentage"
        def basePrice_Data = prices?.basePrice?.setScale(3, BigDecimal.ROUND_HALF_UP) ?: 0.0
        def flashing_Percentage = api.findLookupTableValues(percentage_Table, *filter)?.find()?.attribute1 ?: 0.0

        if (flashing_Percentage) {
            prices.basePrice = basePrice_Data + (basePrice_Data * flashing_Percentage)

            break
        }
    }
    return prices
}


// RUUK-112 : If prices are not generated in LPG, for failproof option, we are fetching prices from Component Product Prices PX.


if (api.local.isWeightedRule && out.SpecialMaterial == true && api.local.product.rawMaterialCost == 0.0 && api.local.product.processingCost == 0.0) {

    prices = {}
    prices.basePrice = 0.0
    api.local.transferPrice = 0.0
    if (api.local.product.market != "FI") {
        prices.stfprices = [start   : 0
                            , target: 0
                            , floor : 0]

    } else if (api.local.product.market == "FI") {
        prices.stfprices = [target_A1  : 0.0
                            , target_A8: 0.0
                            , floor_A1 : 0.0
                            , floor_A8 : 0.0
                            , start_A1 : 0.0
                            , start_A8 : 0.0]

    }
    return prices
}
return prices


def getProductPrices() {


    if (api.local.isWeightedRule && out.SpecialMaterial == false) {
        api.local.weightBasedProduct = true

        try {
            def matrix_prices = [:]
            def externalOptions = api.local.product.options?.findAll { it.optionGroupKey == "external" }
            def materialKey = externalOptions?.find { it.groupKey.contains("material") }?.optionKey
            def thicknessKey = externalOptions?.find { it.groupKey.contains("thickness") }?.optionKey ?: "*"
            def coatingKey = externalOptions?.find { it.groupKey.contains("coating") }?.optionKey ?: "*"
            def colorKey = externalOptions?.find { it.groupKey.contains("color") }?.optionKey ?: "*"
            def weightMarkupsData = api.find("PX20", Filter.equal("name", "ProductWeightMarkups"), Filter.equal("sku", api.local.product.productCode))?.find()
            def width = weightMarkupsData?.attribute6
            def plant = weightMarkupsData?.attribute7


            def prices = {}
            String lpgName = "TransferPrice"
            def secondaryKey = api.local.product.market + "|" + materialKey + "|" + thicknessKey + "|" + coatingKey + "|" + colorKey + "|" + width + "|" + plant

            lpgi = libs.options.getAvailable.getPriceFromLpg(lpgName, api.local.product.productCode, secondaryKey)

            if (!lpgi) {
                colorKey = "*"
                secondaryKey = api.local.product.market + "|" + materialKey + "|" + thicknessKey + "|" + coatingKey + "|" + colorKey + "|" + width + "|" + plant
                lpgi = libs.options.getAvailable.getPriceFromLpg(lpgName, api.local.product.productCode, secondaryKey)
                if (!lpgi) {
                    coatingKey = "*"
                    secondaryKey = api.local.product.market + "|" + materialKey + "|" + thicknessKey + "|" + coatingKey + "|" + colorKey + "|" + width + "|" + plant
                    lpgi = libs.options.getAvailable.getPriceFromLpg(lpgName, api.local.product.productCode, secondaryKey)
                }
                if (!lpgi) {
                    width = "*"
                    secondaryKey = api.local.product.market + "|" + materialKey + "|" + thicknessKey + "|" + coatingKey + "|" + colorKey + "|" + width + "|" + plant
                    lpgi = libs.options.getAvailable.getPriceFromLpg(lpgName, api.local.product.productCode, secondaryKey)
                }
                if (!lpgi) {
                    plant = "*"
                    secondaryKey = api.local.product.market + "|" + materialKey + "|" + thicknessKey + "|" + coatingKey + "|" + colorKey + "|" + width + "|" + plant
                    lpgi = libs.options.getAvailable.getPriceFromLpg(lpgName, api.local.product.productCode, secondaryKey)
                }

                if (!lpgi) {
                    return returnZeroPrices()
                }
            }
            if (lpgi) {
                def today = new Date().format('yyyy-MM-dd')
                def matrix = lpgi?.calculationResults?.find { it.resultName == "PriceMatrix" }?.result?.entries
                Collection newMatrix = lpgi?.calculationResults?.find { it.resultName == "PriceMatrix" }?.result?.entries.findAll{it.keySet().contains("newCogsPrice")}
                if (api.local.product.pricingDate) {
                    matrix = matrix?.findAll { it.rawMaterialValidFrom <= api.local.product.pricingDate && it.claddingCostValidFrom <= api.local.product.pricingDate && it.baseMarkupsValidFrom <= api.local.product.pricingDate }
                    newMatrix = newMatrix?.findAll { it.rawMaterialValidFrom <= api.local.product.pricingDate && it.claddingCostValidFrom <= api.local.product.pricingDate && it.baseMarkupsValidFrom <= api.local.product.pricingDate }
                } else {
                    matrix = matrix?.findAll { it.rawMaterialValidFrom <= today && it.claddingCostValidFrom <= today && it.baseMarkupsValidFrom <= today }
                    newMatrix = newMatrix?.findAll { it.rawMaterialValidFrom <= today && it.claddingCostValidFrom <= today && it.baseMarkupsValidFrom <= today }
                }
                matrix_prices = matrix?.sort { a, b -> b.rawMaterialValidFrom <=> a.rawMaterialValidFrom ?: b.claddingCostValidFrom <=> a.claddingCostValidFrom ?: b.baseMarkupsValidFrom <=> a.baseMarkupsValidFrom }
                        ?.getAt(0)
                new_matrix_prices = newMatrix?.sort { a, b -> b.rawMaterialValidFrom <=> a.rawMaterialValidFrom ?: b.claddingCostValidFrom <=> a.claddingCostValidFrom ?: b.baseMarkupsValidFrom <=> a.baseMarkupsValidFrom }
                        ?.getAt(0)

                if (!matrix_prices) {
                    return returnZeroPrices()
                }
                if (!new_matrix_prices) {
                    return returnZeroPrices()
                }
//                def flashingWithCoat = false


                //getting rawMaterialCost and processingCost from LPG
                def rawMaterialCost = matrix_prices?.rawMaterialCost
                def processingCost = matrix_prices?.processingCost

                def newRawMaterialCost = new_matrix_prices?.rawMaterialCost
                def newProcessingCost = new_matrix_prices?.processingCost

                rawMaterialCost = (rawMaterialCost ?: 0) + (api.local.category2_Optionsprice ?: 0)
                processingCost = (processingCost ?: 0) + (api.local.category1_Optionsprice ?: 0)

                newRawMaterialCost = (newRawMaterialCost ?: 0) + (api.local.category2_Optionsprice ?: 0)
                newProcessingCost = (newProcessingCost ?: 0) + (api.local.category1_Optionsprice ?: 0)

                def cogsPrice = rawMaterialCost + processingCost
                def newCogsPrice = newRawMaterialCost + newProcessingCost

                prices.basePrice = (newCogsPrice ?: 0) * (1.0 + (api.local.basemarkupdata?.attribute1 ?: 0))
                api.local.weightBasedBasePrice = (cogsPrice ?: 0) * (1.0 + (api.local.basemarkupdata?.attribute1 ?: 0))
                api.local.warnings.add("Original base price ${prices.basePrice}")
                api.local.product.cogsPrice = newCogsPrice
                api.local.transferPrice = (newProcessingCost * (api.local.factoryMarkup ?: 0.0)) + newRawMaterialCost

                if (api.local.product.market == "FI") {
                    prices.stfprices = [target_A1: matrix_prices?.A1_target,
                                        target_A8: matrix_prices?.A8_target,
                                        floor_A1 : matrix_prices?.A1_floor,
                                        floor_A8 : matrix_prices?.A8_floor,
                                        start_A1 : matrix_prices?.A1_start,
                                        start_A8 : matrix_prices?.A8_start]
                    api.local.product.specialPricing = true
                } else {
                    prices.stfprices = [start : matrix_prices?.startPrice,
                                        target: matrix_prices?.targetPrice,
                                        floor : matrix_prices?.floorPrice]
                }
                if (prices.stfprices.size() != 3) {
                    def error = "Expecting extactly 3 prices for product ${api.local.product.productCode} and market ${api.local.product.market} but received ${prices.stfprices.size()}."
                    api.local.warnings.add(error)
                }

                return prices
            }
            api.logInfo("+++++++++exception: price rows is null")
            def error = "No price records found for product ${api.local.product.productCode} and market ${api.local.product.market} combination."
            api.local.warnings.add(error)
            return null
        } catch (e) {

            def error = "Error retrieving prices for product ${api.local.product.productCode} and market ${api.local.product.market} combination - ${e}"
            api.local.warnings.add(error)
            return null
        }
    } else if ((api.local.isAccessoryPriceRule && api.local.product.cogsPrice == 0.0) || (api.local.basemarkupdata == null) || ((api.local.product.rawMaterialCost > 0.0 && api.local.product.processingCost > 0.0) && api.local.basemarkupdata == null) || ((api.local.isPanelPricing) && (api.local.panelPricesData == null || api.local.panelNormData == null || api.local.pmQuantityMarkupdata == null || api.local.basemarkupdata == null)) || (api.local.isCladding && api.local.optionsPrice?.rawMaterialPrice == 0.0) || (api.local.isCladding && api.local.basemarkupdata == null) || (api.local.pmQuantityMarkupdata == null && api.local.isCladding && api.local.product.market == "FI")) {

        return returnZeroPrices()
    } else if (api.local.isfromCompProductPrices) {

        return getComponentProductPrices()
    }
}

Closure returnZeroPrices() {
    prices = {}
    prices.basePrice = 0.0
    api.local.transferPrice = 0.0
    if (api.local.product.market != "FI") {
        prices.stfprices = [start   : 0
                            , target: 0
                            , floor : 0]

    } else if (api.local.product.market == "FI") {
        prices.stfprices = [target_A1  : 0.0
                            , target_A8: 0.0
                            , floor_A1 : 0.0
                            , floor_A8 : 0.0
                            , start_A1 : 0.0
                            , start_A8 : 0.0]

    }
    return prices
}

def getComponentProductPrices() {
    try {

        api.local.panelsPricing = false
        api.local.componentProductPrices = true
        def today = new Date()

        def price_rows = api.find("PX20", 0, 1, "-attribute11", *out.Filters)
        if (price_rows.size() > 0) {
            def valid_price_row = price_rows.getAt(0)
            def prices = {}
            prices.basePrice = valid_price_row?.attribute4.setScale(3, BigDecimal.ROUND_HALF_UP)

            api.local.warnings.add("Original base price ${prices.basePrice}")
            api.local.product.cogsPrice = valid_price_row?.attribute10 ?: 0.0


            if ((valid_price_row?.attribute8 != null) || (valid_price_row?.attribute9 != null)) {
                if ((valid_price_row?.attribute8 != null) && (valid_price_row?.attribute9 != null)) {
                    prices.stfprices = [target_A1: valid_price_row?.attribute8,
                                        target_A8: valid_price_row?.attribute9,
                                        floor_A1 : valid_price_row?.attribute7,
                                        floor_A8 : valid_price_row?.attribute7,
                                        start_A1 : valid_price_row?.attribute9,
                                        start_A8 : valid_price_row?.attribute9]
                    api.local.product.specialPricing = true
                } else {
                    def error = "Trying to use special pricing for product ${api.local.product.productCode} and market ${api.local.product.market} combination but missing the second target price."
                    api.local.warnings.add(error)
                    return null
                }
            } else {
                prices.stfprices = [start : valid_price_row?.attribute5,
                                    target: valid_price_row?.attribute6,
                                    floor : valid_price_row?.attribute7]

            }
            if (prices.stfprices.size() != 3) {
                def error = "Expecting extactly 3 prices for product ${api.local.product.productCode} and market ${api.local.product.market} but received ${prices.stfprices.size()}."
                api.local.warnings.add(error)
            }

            return prices
        }
        api.logInfo("+++++++++exception: price rows is null")
        def error = "No price records found for product ${api.local.product.productCode} and market ${api.local.product.market} combination."
        api.local.warnings.add(error)
        return null
    } catch (e) {
        def error = "Error retrieving prices for product ${api.local.product.productCode} and market ${api.local.product.market} combination - ${e}"
        api.local.warnings.add(error)
        return null
    }
}