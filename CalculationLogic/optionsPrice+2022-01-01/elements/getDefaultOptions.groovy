if (!api.local.priceLogic) {
    return
}
// If there are no options passed from hybris, get the default options


// Accessories don't have options


if (api.local.product.isAccessory == false) {

    options = libs.options.getDefaults.main(api.local.product)

    // Return an empty list if there is any error or empty options so the logic downstream can handle it
    if (options instanceof String || !options) {

        api.local.warnings.add(options)

        return []

    }

    // Only return default options which have a selectedByDefault flag
//     we can remove it.default == true here : as we are making default = true in library
//     return options_default = options

    def options_default = options.findAll { it.selectedByDefault == true }

    return options_default

}


