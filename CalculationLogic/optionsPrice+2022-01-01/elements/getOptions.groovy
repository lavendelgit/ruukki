
if (!api.local.optionLogic) {
    return
}
def product_rows = api.find("P", Filter.equal("sku", api.local.product.productCode))?.getAt(0)
api.local.product.productType = product_rows?.attribute4

def cladd_data = api.vLookup("PricingRule", "materialBasedRule")
def cladd_Products = cladd_data.tokenize(",").collect { it.trim() }

api.local.product.lamellaCladding = (cladd_Products.contains(api.local.product.productType))
try {

    if (api.local.product.optionGroup && api.local.product.optionGroup.size() > 0) {
        api.local.product.optionGroup = libs.options.getAvailable.main(api.local.product)
    } else {
        api.local.product.optionGroup = libs.options.getDefaults.main(api.local.product)
        api.local.product.optionGroup = getavailablerow(api.local.product)
    }
    return true
} catch (e) {
    api.local.error = "No options found for product ${api.local.product.productCode} in market ${api.local.product.market}"
}

def getavailablerow(Map product) {
    def optionGroupdefault = product.optionGroup
    optionGroupdefault.removeAll {
        r -> r.selected == false
    }
    def optionGroupSelected = optionGroupdefault
    api.local.product.optionGroup = optionGroupSelected
    def availableOptions
    availableOptions = libs.options.getAvailable.main(api.local.product)
    return availableOptions
}