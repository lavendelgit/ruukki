Object sendEmail(Long startTime) {
    Date date = new Date()
    Long endTime = (long) (((((date.getHours() * 60) + date.getMinutes()) * 60) + date.getSeconds()) * 1000)

    if ((endTime - startTime) > 8000) {

        String headerMessage = "<p><br/>Dear team,<br/><br/>Please check, The requesting and response time exceeds the threshold value in RUUKKI Production Environment. </p>"
        String footerMessage = "<br/><br/><i>Please do not reply to this email as it is an automated email from Pricefx</i><br/><br/>Best Regards,<br/>Pricefx team!"

        List userGroups = api.find("U", Filter.equal("groups.uniqueName", "ResponseDelayAlert"))?.email

        String productRequest = "<br/>"
        String logicName = api.local.product.logicName

        if (logicName == "productOptions") {
            String pricingDate = api.local.product.pricingDate ?: null
            String productCode = api.local.product.productCode ?: null
            String language = api.local.product.language ?: null
            String market = api.local.product.market ?: null
            List selectedOptions = api.local.optionGroup ?: null
            String currency = api.local.product.currency ?: null
            String selectedOptionsProductOptions = ""
            if (selectedOptions) {
                for (option in selectedOptions) {
                    String joinedSelectedOptions = """ {
                    "special": ${option?.special ?: null},
                    "standard": ${option?.standard ?: null},
                    "optionLabel": "${option?.optionLabel ?: null}",
                    "optionGroupKey": "${option?.optionGroupKey ?: null}",
                    "optionKey": "${option?.optionKey ?: null}",
                    "price": ${option?.price ?: (0.0 as String)},
                    "qty": ${option?.qty ?: (0.0 as String)},
                    "customSort": ${option?.customSort ?: (0.0 as String)},
                    "groupLabel": "${option?.groupLabel ?: null}",
                    "optionGroupLabel": "${option?.optionGroupLabel ?: null}",
                    "groupKey": "${option?.groupKey ?: null}",
                    "selected": ${option?.selected ?: null}
                }"""
                    selectedOptionsProductOptions = selectedOptionsProductOptions + joinedSelectedOptions + ","
                }
                selectedOptionsProductOptions = selectedOptionsProductOptions[0..-2]
            }

            productRequest = """<br/>Below is the request that is taking longer than expected: <br/><br/><font color="red">[
                 {
                    "data": {
                    "pricingDate": "${pricingDate}",
                    "language": "${language}",
                    "logicName": "${logicName}",
                    "market": "${market}",
                    "productCode": "${productCode}",
                    "selectedOptions":[${selectedOptionsProductOptions}],
                    "currency": "${currency}"
                }
            }
            ]</font><br/>"""

        }


        if (logicName == "productPrice") {
            String customerClass = api.local.customerClass ?: null
            String customerSegment = api.local.customerSegment ?: null
            String lineItemNumber = api.local.lineItemNumber ?: null
            String currency = api.local.product.currency ?: null
            String creationDate = api.local.product.quoteDate ?: null
            String productCode = api.local.product.productCode ?: null
            String unitOfMeasure = api.local.product.unitOfMeasure ?: null
            String rawMaterialCost = api.local.product.rawMaterialCost ?: (0.0 as String)
            String processingCost = api.local.product.processingCost ?: (0.0 as String)
            String language = api.local.product.language ?: null
            String market = api.local.product.market ?: null
            String freightPrice = api.local.product.freightPrice ?: (0.0 as String)
            String quantity = api.local.product.quantity ?: (0.0 as String)
            List selectedOptions = api.local.product.options ?: null
            String selectedOptionsProductPrice = ""
            if (selectedOptions) {
                for (option in selectedOptions) {
                    String joinedSelectedOptions = """ {
                    "special": ${option?.special ?: null},
                    "standard": ${option?.standard ?: null},
                    "optionLabel": "${option?.optionLabel ?: null}",
                    "optionGroupKey": "${option?.optionGroupKey ?: null}",
                    "optionKey": "${option?.optionKey ?: null}",
                    "price": ${option?.price ?: (0.0 as String)},
                    "qty": ${option?.qty ?: (0.0 as String)},
                    "customSort": ${option?.customSort ?: (0.0 as String)},
                    "groupLabel": "${option?.groupLabel ?: null}",
                    "optionGroupLabel": "${option?.optionGroupLabel ?: null}",
                    "groupKey": "${option?.groupKey ?: null}",
                    "selected": ${option?.selected ?: null}
                }"""
                    selectedOptionsProductPrice = selectedOptionsProductPrice + joinedSelectedOptions + ","
                }
                selectedOptionsProductPrice = selectedOptionsProductPrice[0..-2]
            }
            String pricingDate = api.local.product.pricingDate ?: null

            productRequest = """<br/>Below is the request that is taking longer than expected: <br/><br/><font color="red">[
                {
                    "data": {
                    "quantity": ${quantity},
                    "pricingDate": "${pricingDate}",
                    "unitOfMeasure": "${unitOfMeasure}",
                    "processingCost": ${processingCost},
                    "language": "${language}",
                    "creationDate": "${creationDate}",
                    "rawMaterialCost": ${rawMaterialCost},
                    "logicName": "${logicName}",
                    "market": "${market}",
                    "productCode": "${productCode}",
                    "customerClass": "${customerClass}",
                    "customerSegment": "${customerSegment}",
                    "selectedOptions":[${selectedOptionsProductPrice}],
                    "currency": "${currency}",
                    "freightPrice": ${freightPrice},
                    "lineItemNumber": "${lineItemNumber}"
                }
            }
            ]</font><br/>"""

        }
        String finalMessage = headerMessage + productRequest + footerMessage
        userGroups?.each {
            api.sendEmail(it, "URL Response Delay Alert", finalMessage)
        }
    }
}

