if (!api.local.priceLogic) {
    return
}

//  MaterialBasedRule for cladding products
def cladd_data = api.vLookup("PricingRule", "materialBasedRule")
def cladd_Products = cladd_data.tokenize(",").collect { it.trim() }
api.local.isCladding = (cladd_Products.contains(api.local.product.productType))


// UserInputCalculations rule for User input rawmaterial and processingcosts
def userinputCalculationsgroups = api.vLookup("PricingRule", "UserInputPriceCalculations")?.tokenize(",")?.collect { it.trim() }
api.local.isUserinputCalc = (userinputCalculationsgroups.contains(api.local.product.productType))


// ComponentProductPricesFetch to fetch prices from "component Product prices table"
List fetchFromPricesGroups = api.vLookup("PricingRule", "componentProductPricesFetch")?.tokenize(",")?.collect { it.trim() }
api.local.isfromCompProductPrices = fetchFromPricesGroups.contains(api.local.product.productType)


//AccessoriesPriceRule
List accessoryPriceRuleGroups = api.vLookup("PricingRule", "AccessoriesPriceRule")?.tokenize(",")?.collect { it.trim() }
api.local.isAccessoryPriceRule = accessoryPriceRuleGroups.contains(api.local.product.productType)


// WeightBased Rule
List WeightedRuleGroups = api.vLookup("PricingRule", "WeightBasedRule")?.tokenize(",")?.collect { it.trim() }
api.local.isWeightedRule = WeightedRuleGroups.contains(api.local.product.productType)

//PanelsPricing
List panelsPricing = api.vLookup("PricingRule", "PanelsPricing")?.tokenize(",")?.collect { it.trim() }
api.local.isPanelPricing = panelsPricing.contains(api.local.product.productType)
