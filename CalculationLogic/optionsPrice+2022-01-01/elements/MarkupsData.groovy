def marketFilter = com.googlecode.genericdao.search.Filter.equal("key1", api.local.product.market)
def productCodeFilter = com.googlecode.genericdao.search.Filter.equal("key2", api.local.product.productCode)
def baseproductFilter = com.googlecode.genericdao.search.Filter.equal("key2", api.local.product.baseProduct)
def productCategoryFilter = com.googlecode.genericdao.search.Filter.equal("key2", api.local.product.productCategory)

def priceDateInput = api.local.product.pricingDate


def pmMarketFilter = Filter.equal("key1", api.local.product.market)
def pmproductCodeFilter = Filter.equal("key2", api.local.product.productCode)
def pmbaseproductFilter = Filter.equal("key2", api.local.product.baseProduct)
def pmproductCategoryFilter = Filter.equal("key2", api.local.product.productCategory)
def segmentFilter = Filter.equal("key3", "A1")


def basemarkupdataList = libs.claddingCOGS.BaseMarkup.getBaseMarkupList(marketFilter, productCodeFilter, baseproductFilter, productCategoryFilter, api.local.product.pricingDate)
api.local.basemarkupdata = basemarkupdataList?.sort { a, b -> b.key3 <=> a.key3 }?.getAt(0)


api.local.pmQuantityMarkupdata = api.findLookupTableValues("PM_QuantitySegments", pmproductCodeFilter, pmMarketFilter, segmentFilter)?.find()
if (!api.local.pmQuantityMarkupdata) {
    api.local.pmQuantityMarkupdata = api.findLookupTableValues("PM_QuantitySegments", pmbaseproductFilter, pmMarketFilter, segmentFilter)?.find()
}
if (!api.local.pmQuantityMarkupdata) {
    api.local.pmQuantityMarkupdata = api.findLookupTableValues("PM_QuantitySegments", pmproductCategoryFilter, pmMarketFilter, segmentFilter)?.find()
}


api.local.factoryMarkup = api.local.basemarkupdata?.attribute3


if (!api.local.basemarkupdata) {
    def error = "No Markup data found from Base_Sales_Markups table for ${api.local.product.productCode}, ${api.local.product.productCategory} "
    api.local.warnings.add(error)
}

