if (!api.local.priceLogic) {
    return
} else {
    if (api.local.product.currency == 'EUR') {
        api.local.conversionFactor = 1.0
    }

    def product_stf_prices = out.formatSTFPrices
    def product_base_price = out.formatBasePrice
    def freight_price = out.getFreightPrice
    def cogs_price = out.getCogsPrice
    def lastUpdateDate

    try {

        // Delete the quantity segment field from the prices for Accessories
        // as it is not needed in the price response
        if (api.local.product.isAccessory == true && product_stf_prices) {
            product_stf_prices[0].remove('quantitySegment')
        }
//RUUK-181 : TransferPrice should be always in EUR irresepective of input currency.
//RUUK-180  : LastUpdateDate from "ComponentProductPrices" Px in case of fallback else it is from table based on the rule.


        def product_attributes = [productCode     : api.local.product.productCode
                                  , lastUpdateDate: out.LastUpdateDate
                                  , isAccessory   : api.local.product.isAccessory
                                  , unitOfMeasure : api.local.product.unitOfMeasure
                                  , basePrice     : product_base_price
                                  , transferPrice : api.local.transferPrice ? ((api.local.transferPrice)?.setScale(3, BigDecimal.ROUND_HALF_UP) ?: null) : null
                                  , cogsPrice     : ((api.local.isCladding && api.local.optionsPrice?.rawMaterialPrice != null && api.local.optionsPrice?.rawMaterialPrice != 0.0)) ? ((api.local.weightedCOGS as BigDecimal) * api.local.conversionFactor)?.setScale(3, BigDecimal.ROUND_HALF_UP) : cogs_price
                                  , exchangeRate  : api.local.conversionFactor

        ]

        if ((api.local.product.rawMaterialCost && api.local.product.processingCost) || out.SpecialMaterial == true) {
            product_attributes.putAll([rawMaterialCost : api.local.product.rawMaterialCost //* api.local.conversionFactor
                                       , processingCost: api.local.product.processingCost //* api.local.conversionFactor

            ])
        }
        product_attributes.putAll([prices: product_stf_prices])
        // Append options and other attributes for non-accessories
        if (api.local.product.isAccessory == false) {
            product_attributes.putAll([productGroup  : api.local.product.productGroup
                                       , quantity    : api.local.product.quantity
                                       , freightPrice: freight_price

            ])
        }

        def output = [market           : api.local.product.market
                      , lang           : api.local.product.language
                      , customerClass  : api.local.customerClass
                      , lineItemNumber : api.local.lineItemNumber
                      , customerSegment: api.local.customerSegment
                      , currency       : api.local.product.currency
                      , pricingDate    : api.targetDate()?.format("yyyy-MM-dd HH:mm:ss Z")
                      , product        : product_attributes]

        // If the product is an accessory, check if a quantity segment has been set else use A8
        if (api.local.product.isAccessory == false) {
            output.putAll([quantitySegment: api.local.product.quantitySegment ?: 'A8'])
        }

        // Add warnings in test and Dev only
        if (api.local.env != 'prod') {

            if (api.local.warnings.size() > 0) {
                api.local.warnings.each { w -> api.addWarning("${w}")
                }
            }
        }


        return output

    } catch (e) {

        api.logWarn("Error building output for product ${api.local.product.productCode} ------- ${e}")

    }

}


