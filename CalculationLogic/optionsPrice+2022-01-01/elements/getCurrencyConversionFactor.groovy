if (!api.local.priceLogic) {
    return
}
if (api.local.product.currency != 'EUR') {

    try {

        def conversion_factor = api.vLookup('Rates', 'ConversionFactor', 'EUR', api.local.product.currency)

        // If we fail to get a conversion factor, use 1 and add a warning
        if (!conversion_factor) {

            api.local.warnings.add("Error getting conversion factor for currency ${api.local.product.currency}. The prices returned are in EUR.")
            api.local.product.currency = 'EUR'
            api.local.conversionFactor = 1

        }

        api.local.conversionFactor = conversion_factor

        return true


    } catch (e) {

        // If we fail to get a conversion factor, use 1 and add a warning
        api.local.warnings.add("Error getting conversion factor for currency ${api.local.product.currency}. The prices returned are in EUR.")
        api.local.product.currency = 'EUR'
        api.local.conversionFactor = 1

    }

}


        