//RUUK-194 options categorisation on groupKey level for cladding

def name_filter_default = com.googlecode.genericdao.search.Filter.equal("name", "DefaultOptions")
def hidden_filter_default = com.googlecode.genericdao.search.Filter.or(com.googlecode.genericdao.search.Filter.notEqual("attribute13", "1"),
        com.googlecode.genericdao.search.Filter.notEqual("attribute13", 1),
        com.googlecode.genericdao.search.Filter.isNull("attribute13"))
def category1_filter_default = com.googlecode.genericdao.search.Filter.or(com.googlecode.genericdao.search.Filter.equal("attribute15", "1"),
        com.googlecode.genericdao.search.Filter.equal("attribute15", 1))

if (api.local.defaultoptions == null) {
    def defaultDataStream = api.stream("PX20", null, ["sku", "attribute1", "attribute2", "attribute3",
                                                      "attribute5", "attribute7"],
            name_filter_default, hidden_filter_default, category1_filter_default)
    api.local.defaultoptions = defaultDataStream.collect().groupBy { it.attribute2 }
    defaultDataStream.close()
}

List defaultOptions = api.local.defaultoptions?.subMap(api.local.product.market, "ALL", "All", "all")?.values()?.flatten()?.findAll { it -> (([it.attribute1, it.sku].contains(api.local.product.productCode)) && (it.attribute3 != "dimensions"))
}

List category1groupKeys = []

defaultOptions?.each { defaultRow ->
    if (!(defaultRow?.attribute5 in category1groupKeys)) {
        category1groupKeys << (defaultRow.attribute5)
    }
}

api.local.category1_Optionsprice = 0
api.local.category2_Optionsprice = 0

def receivedoptions1 = api.local.product.options?.findAll {
    (it.optionGroupKey == "external" || it.optionGroupKey == "internal") && (it.groupKey in category1groupKeys)
}
List received_options1 = receivedoptions1 instanceof List ? receivedoptions1 : [receivedoptions1]

def receivedoptions2 = api.local.product.options?.findAll {
    (it.optionGroupKey == "external" || it.optionGroupKey == "internal") && !(it.groupKey in category1groupKeys)
}
List received_options2 = receivedoptions2 instanceof List ? receivedoptions2 : [receivedoptions2]


for (received_option in received_options1) {
    if (received_option?.price) {
        if (api.local.product.currency != 'EUR') {
            received_option?.price = received_option.price / api.local.conversionFactor
        }
        api.local.category1_Optionsprice = (api.local.category1_Optionsprice + received_option.price) as BigDecimal
        //?.setScale(1, BigDecimal.ROUND_HALF_UP)
    }
}

for (received_option in received_options2) {
    if (received_option?.price) {
        if (api.local.product.currency != 'EUR') {
            received_option?.price = received_option.price / api.local.conversionFactor
        }
        api.local.category2_Optionsprice = (api.local.category2_Optionsprice + received_option.price) as BigDecimal
        //?.setScale(1, BigDecimal.ROUND_HALF_UP)
    }
}

return

