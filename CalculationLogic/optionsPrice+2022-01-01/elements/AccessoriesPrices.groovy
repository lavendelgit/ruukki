if (!api.local.priceLogic) {
    return
}

if (api.local.isCladding || api.local.isUserinputCalc || api.local.isfromCompProductPrices || api.local.isWeightedRule) {
    return
}

if ((api.local.product.rawMaterialCost > 0.0 && api.local.product.processingCost > 0.0)) {
    return
}


if (api.local.isAccessoryPriceRule) {

    def today = new Date()

    def price_row = api.find("PX20", 0, 1, "-attribute11", *out.Filters)?.find()
    api.local.product.cogsPrice = price_row?.attribute10 ?: 0.0


    if (api.local.product.cogsPrice == 0.0 || api.local.basemarkupdata == null) {
        return
    }

    api.local.accessoryCogs = true
    api.local.transferPrice = api.local.product.cogsPrice + api.local.product.cogsPrice / 10

    def prices = {}
    def target_A1
    def target_A8
    def start_A1
    def start_A8
    def floor_A1
    def floor_A8

    prices?.basePrice = (api.local.product.cogsPrice ?: 0.0) * (1.0 + (api.local.basemarkupdata?.attribute1))


    if (api.local.product.market != "FI") {

        targetprice = (prices?.basePrice as BigDecimal) * ((1.0 + (api.local.basemarkupdata?.attribute5)) as BigDecimal)
        BigDecimal startprice = ((targetprice as BigDecimal) * ((1 + (api.local.basemarkupdata?.attribute2)) as BigDecimal))
        BigDecimal floorprice = (targetprice as BigDecimal) * (1.0 - (api.local.basemarkupdata?.attribute4) as BigDecimal)

        prices.stfprices = [
                start   : startprice
                , target: targetprice
                , floor : floorprice
        ]

        return prices
    } else if (api.local.product.market == "FI") {

        def targetA8 = (prices?.basePrice as BigDecimal) * ((1.0 + (api.local.basemarkupdata?.attribute5)) as BigDecimal)
        def targetA1 = (prices?.basePrice as BigDecimal) * (1.0 + api.local.pmQuantityMarkupdata?.attribute3) as BigDecimal

        prices.stfprices = [
                target_A1  : targetA1
                , target_A8: targetA8
                , floor_A1 : targetA1 * (1.0 - (api.local.basemarkupdata?.attribute4) as BigDecimal)
                , floor_A8 : targetA8 * (1.0 - (api.local.basemarkupdata?.attribute4) as BigDecimal)
                , start_A1 : targetA1 * (1.0 + (api.local.basemarkupdata?.attribute2) as BigDecimal)
                , start_A8 : targetA8 * (1.0 + (api.local.basemarkupdata?.attribute2) as BigDecimal)
        ]

        return prices
    }

}





