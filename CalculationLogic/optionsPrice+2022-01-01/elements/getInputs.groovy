api.local.product = [:]
api.local.product.optionGroup = []
api.local.optionLogic = false
api.local.priceLogic = false
//api.logInfo("**************product********",api.local.product)
api.local.product.logicName = api.input("logicName")
api.logInfo("api.local.product.logicName", api.local.product.logicName)
if (api.local.product.logicName == "productOptions") {

    api.local.optionLogic = true
//    api.local.product.creationDate = api.input("creationDate")
    api.local.product.pricingDate = api.input("pricingDate")
    api.local.product.productCode = api.input("productCode")
    api.local.product.language = api.input("language")
    api.local.product.market = api.input("market")
    api.local.product.optionGroup = api.input("selectedOptions")
    api.local.optionGroup = api.local.product.optionGroup
    api.local.product.currency = api.input("currency")
    api.local.error = null


} else if (api.local.product.logicName == "productPrice") {
    api.local.priceLogic = true
    String Currpart = api.currentPartitionName()
    if (Currpart == "brcp5de644e3") {
        api.local.env = 'prod'
    } else if (Currpart == "bynfo_ruukki_qa") {
        api.local.env = 'dev'
    }

    api.local.customerClass = api.input("customerClass")
    api.local.customerSegment = api.input("customerSegment")
    api.local.lineItemNumber = api.input("lineItemNumber")

    api.local.product.currency = api.input("currency")
    api.local.product.quoteDate = api.input("creationDate")
    api.local.product.productCode = api.input("productCode")
    api.local.product.unitOfMeasure = api.input("unitOfMeasure")
    api.local.product.rawMaterialCost = api.input("rawMaterialCost")
    api.local.product.processingCost = api.input("processingCost")
    api.local.product.language = api.input("language")
    api.local.product.market = api.input("market")
    api.local.product.freightPrice = api.input("freightPrice")
    api.local.product.quantity = api.input("quantity")
    api.local.product.options = api.input("selectedOptions") ?: [:]
    api.local.product.pricingDate = api.input("pricingDate") ?: null

    api.local.warnings = []
//    api.logInfo("**************product********",api.local.product)
// Not for options!
    api.local.product.each {
        if ((!it.value || it.value == null) && it.key != 'options') {
            api.local.warnings.add("Missing input value for ${it.key}.")
        }
    }
} else {
    api.local.warnings?.add("No Logic selected. Please select the Logic to be executed")
}
//if (api.isDebugMode()) {
//    api.local.product.options = [
//            [
//                    "special": false,
//                    "standard": true,
//                    "optionLabel": "Color coated steel",
//                    "optionGroupKey": "external",
//                    "optionKey": "materialCC",
//                    "price": 0.0,
//                    "qty": 0.0,
//                    "customSort": 0,
//                    "groupLabel": "Material",
//                    "optionGroupLabel": "External",
//                    "groupKey": "material",
//                    "selected": true
//            ],
//
//            [
//                    "special": false,
//                    "standard": true,
//                    "optionLabel": "1.2 mm",
//                    "optionGroupKey": "external",
//                    "optionKey": "thickness12",
//                    "price": 0.0,
//                    "qty": 0.0,
//                    "customSort": 0,
//                    "groupLabel": "Thickness",
//                    "optionGroupLabel": "External",
//                    "groupKey": "thickness",
//                    "selected": true
//            ],
//            [
//
//
//                    "special": false,
//                    "standard": true,
//                    "optionLabel": "Hiarc 27my",
//                    "optionGroupKey": "external",
//                    "optionKey": "coatingHiarc",
//                    "price": 0.0,
//                    "qty": 0.0,
//                    "customSort": 0,
//                    "groupLabel": "Coating",
//                    "optionGroupLabel": "External",
//                    "groupKey": "coating",
//                    "selected": true
//
//            ],
//            [
//                    "special": false,
//                    "standard": true,
//                    "optionLabel": "RR21",
//                    "optionGroupKey": "external",
//                    "optionKey": "colRR21",
//                    "price": 0.0,
//                    "qty": 0.0,
//                    "customSort": 0,
//                    "groupLabel": "Color",
//                    "optionGroupLabel": "External",
//                    "groupKey": "color",
//                    "selected": true
//            ],
//            [
//                    "special": false,
//                    "standard": true,
//                    "optionLabel": "No perforation",
//                    "optionGroupKey": "external",
//                    "optionKey": "perforNone",
//                    "price": 0.0,
//                    "qty": 0.0,
//                    "customSort": 0,
//                    "groupLabel": "Perforation",
//                    "optionGroupLabel": "External",
//                    "groupKey": "perfor",
//                    "selected": true
//            ],
//            [
//                    "special": false,
//                    "standard": false,
//                    "optionLabel": "750-1500/550-600",
//                    "optionGroupKey": "dimensions",
//                    "optionKey": "750/550",
//                    "price": 35.8,
//                    "qty": 12.0,
//                    "customSort": 0,
//                    "groupLabel": "A/B",
//                    "optionGroupLabel": "Dimension",
//                    "groupKey": "A/B",
//                    "selected": true
//
//            ]
//            [
//
//                    "special": false,
//                    "standard": true,
//                    "optionLabel": "0.22",
//                    "optionGroupKey": "external",
//                    "optionKey": "uvalue022",
//                    "price": 0.0,
//                    "qty": 0.0,
//                    "customSort": 0,
//                    "groupLabel": "U-Value (W/m2K)",
//                    "optionGroupLabel": "External",
//                    "groupKey": "uvalue",
//                    "selected": true
//            ],
//            [
//                    "special": false,
//                    "standard": true,
//                    "optionLabel": "A2-s1, d0",
//                    "optionGroupKey": "external",
//                    "optionKey": "rtfa2",
//                    "price": 0.0,
//                    "qty": 0.0,
//                    "customSort": 0,
//                    "groupLabel": "Reaction to Fire",
//                    "optionGroupLabel": "External",
//                    "groupKey": "rtf",
//                    "selected": true
//            ],
//            [
//                    "special": false,
//                    "standard": true,
//                    "optionLabel": "EIM120_6,0/-",
//                    "optionGroupKey": "external",
//                    "optionKey": "spaneim120",
//                    "price": 0.0,
//                    "qty": 0.0,
//                    "customSort": 0,
//                    "groupLabel": "Wall fire resistance values & max span horizontal / vertical orientation (m):",
//                    "optionGroupLabel": "External",
//                    "groupKey": "span",
//                    "selected": true
//            ],
//            [
//                    "special": false,
//                    "standard": true,
//                    "optionLabel": "EIM90_7,5/-",
//                    "optionGroupKey": "external",
//                    "optionKey": "spaneim90",
//                    "price": 0.0,
//                    "qty": 0.0,
//                    "customSort": 0,
//                    "groupLabel": "Wall fire resistance values & max span horizontal / vertical orientation (m):",
//                    "optionGroupLabel": "External",
//                    "groupKey": "span",
//                    "selected": true
//            ],[
//                    "special": false,
//                    "standard": true,
//                    "optionLabel": "EIM60_7,5/-",
//                    "optionGroupKey": "external",
//                    "optionKey": "spaneim60",
//                    "price": 0.0,
//                    "qty": 0.0,
//                    "customSort": 0,
//                    "groupLabel": "Wall fire resistance values & max span horizontal / vertical orientation (m):",
//                    "optionGroupLabel": "External",
//                    "groupKey": "span",
//                    "selected": true
//            ],
//            [
//                    "special": false,
//                    "standard": true,
//                    "optionLabel": "EIM30_7,5/-",
//                    "optionGroupKey": "external",
//                    "optionKey": "spaneim30",
//                    "price": 0.0,
//                    "qty": 0.0,
//                    "customSort": 0,
//                    "groupLabel": "Wall fire resistance values & max span horizontal / vertical orientation (m):",
//                    "optionGroupLabel": "External",
//                    "groupKey": "span",
//                    "selected": true
//            ],[
//                    "special": false,
//                    "standard": true,
//                    "optionLabel": "31",
//                    "optionGroupKey": "external",
//                    "optionKey": "sound31",
//                    "price": 0.0,
//                    "qty": 0.0,
//                    "customSort": 0,
//                    "groupLabel": "Sound insulation Rw (dB)",
//                    "optionGroupLabel": "External",
//                    "groupKey": "sound",
//                    "selected": true
//            ],
//            [
//                    "special": false,
//                    "standard": true,
//                    "optionLabel": "Color coated",
//                    "optionGroupKey": "internal",
//                    "optionKey": "CC",
//                    "price": 0.0,
//                    "qty": 0.0,
//                    "customSort": 0,
//                    "groupLabel": "Material type",
//                    "optionGroupLabel": "Internal",
//                    "groupKey": "materialpanels_int",
//                    "selected": true
//            ],[
//                    "special": false,
//                    "standard": true,
//                    "optionLabel": "Steel 0,7mm",
//                    "optionGroupKey": "internal",
//                    "optionKey": "materialSteel07",
//                    "price": 0.0,
//                    "qty": 0.0,
//                    "customSort": 0,
//                    "groupLabel": "Material thickness",
//                    "optionGroupLabel": "Internal",
//                    "groupKey": "thickness_int",
//                    "selected": true
//            ],[
//                    "special": false,
//                    "standard": true,
//                    "optionLabel": "Polyester 25my",
//                    "optionGroupKey": "internal",
//                    "optionKey": "coatingPOL07",
//                    "price": 0.0,
//                    "qty": 0.0,
//                    "customSort": 0,
//                    "groupLabel": "Coating",
//                    "optionGroupLabel": "Internal",
//                    "groupKey": "coating_int",
//                    "selected": true
//            ],[
//                    "special": false,
//                    "standard": true,
//                    "optionLabel": "RR20",
//                    "optionGroupKey": "internal",
//                    "optionKey": "colRR20",
//                    "price": 0.0,
//                    "qty": 0.0,
//                    "customSort": 0,
//                    "groupLabel": "Color",
//                    "optionGroupLabel": "Internal",
//                    "groupKey": "color_int",
//                    "selected": true
//            ],[
//                    "special": false,
//                    "standard": true,
//                    "optionLabel": "Micro M15",
//                    "optionGroupKey": "internal",
//                    "optionKey": "profilingM15",
//                    "price": 0.0,
//                    "qty": 0.0,
//                    "customSort": 0,
//                    "groupLabel": "Profiling",
//                    "optionGroupLabel": "Internal",
//                    "groupKey": "profiling_int",
//                    "selected": true
//            ],[
//                    "special": false,
//                    "standard": true,
//                    "optionLabel": "Yes",
//                    "optionGroupKey": "internal",
//                    "optionKey": "sealing4Yes",
//                    "price": 0.0,
//                    "qty": 0.0,
//                    "customSort": 0,
//                    "groupLabel": "Sealing",
//                    "optionGroupLabel": "Internal",
//                    "groupKey": "sealing_int",
//                    "selected": true
//            ]
//////
////
//    ]
////
////
//    ]
//}