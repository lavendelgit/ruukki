if (!api.local.priceLogic) {
    return
}
try {
    def base_price
    def freight_price = out.getFreightPrice

    if (api.local.pmQuantityMarkupdata == null && api.local.isCladding && api.local.product.market == "FI") {
        base_price = out.getProductPrices
    } else if ((api.local.isCladding && api.local.optionsPrice?.rawMaterialPrice != null && api.local.optionsPrice?.rawMaterialPrice != 0.0)) {
        base_price = out.CladdingPrices
    } else if (api.local.isCladding && out.SpecialMaterial == true && api.local.product.rawMaterialCost == 0.0 && api.local.product.processingCost == 0.0) {
        base_price = out.CladdingPrices
    } else if ((api.local.isCladding && api.local.specialMaterial == true && (api.local.product.rawMaterialCost > 0.0 && api.local.product.processingCost > 0.0))) {
        base_price = out.UserinputPrices
    } else if (api.local.userInputPrices) {
        base_price = out.UserinputPrices
    } else if (api.local.accessoryCogs) {
        base_price = out.AccessoriesPrices
    } else if (api.local.panelsPricing) {
        base_price = out.PanelsPrices
    } else {
        base_price = out.getProductPrices
    }

//def options_price = out.getOptionsPrice

    if (api.local.componentProductPrices) {
        base_price.basePrice = (base_price.basePrice ?: 0) + (out.getOptionsPrice ?: 0)
    }
//    api.local.warnings.add("Options price ${options_price}")
    api.local.warnings.add("Base price ${(base_price.basePrice)?.setScale(3, BigDecimal.ROUND_HALF_UP)}")
    api.local.warnings.add("Freight price ${freight_price}")

    if (base_price != null && freight_price != null) {

        def final_base_price = base_price.basePrice
        final_base_price = final_base_price.add(freight_price).setScale(3, BigDecimal.ROUND_HALF_UP)
        api.local.warnings.add("Final base price ${final_base_price}")

        return final_base_price

    } else {
        api.local.warnings.add("Error getting final price for product ${api.local.product.productCode}.")
        return null
    }

} catch (e) {

    api.local.warnings.add("Error getting final price for product ${api.local.product.productCode} with currency ${api.local.product.currency} --------- ${e}.")

    return null

}
