if (!api.local.priceLogic) {
    return
}
def virtual_stf = out.getVirtualSTF
def customer_pm = out.getCustomerPM
def quantity_pm = out.getQuantitySegmentPM


// We always get a value or 0 on failure in each of these steps
if (virtual_stf == null || customer_pm == null || quantity_pm == null) {
    return null
}
//api.trace("quantity_pm.unique()",quantity_pm.unique())
// If the quantity price modifiers are ok
// ensure we dont have duplicated values
if ((!quantity_pm) instanceof BigDecimal) {
    quantity_pm = quantity_pm.unique()
}
def total_stf = null

if (api.local.componentProductPrices) {

    if (api.local.product.specialPricing == true || api.local.product.market == "FI") {

        try {
            // For special pricing, the target and segment are in the keys name: target_A1, target_A8
            total_stf = virtual_stf.collect {
                [
                        segment: it.type.split('_')[1],
                        type   : it.type.split('_')[0],
                        markup : customer_pm + it.markup
                ]
            }

        }

        catch (e) {
            api.local.warnings.add("Error running getTotalSTF with special pricing -  ${e}")
            return null
        }

    } else {
        // Handling normal stf pricing
        try {

            total_stf = []
            for (q in quantity_pm) {
                for (v in virtual_stf) {

                    total_stf.add(
                            [
                                    segment: q.name,
                                    type   : v.type,
                                    markup : customer_pm + q.markup + v.markup
                            ]
                    )
                }
            }

        } catch (e) {
            api.local.warnings.add("Error in getTotalSTF with normal pricing - ${e}")
            return null
        }
    }

    if (total_stf.size() < 2) {
        api.local.warnings.add("Error in getTotalSTF accessory pricing - got ${total_stf.size()} values when there should have been at least 2")
        return null
    }

    return total_stf
} else {

// Handling special stf pricing
    if (api.local.product.specialPricing == true || api.local.product.market == "FI") {

        try {
            // For special pricing, the target and segment are in the keys name: target_A1, target_A8
            total_stf = virtual_stf.collect {
                [
                        segment: it.type.split('_')[1],
                        type   : it.type.split('_')[0],
                        markup : customer_pm + it.markup
                ]
            }

        }

        catch (e) {

            api.local.warnings.add("Error running getTotalSTF with special pricing -  ${e}")
            return null
        }

    } else {
        // Handling normal stf pricing

        try {

            total_stf = []
            quantity_pm.each { q ->

                virtual_stf.each { v ->

                    if (v.type == "target") {
                        total_stf << [
                                segment: q.name,
                                type   : "target",
                                markup : q.markup
                        ]

                    }
                    if (v.type == "start") {
                        total_stf << [
                                segment: q.name,
                                type   : "start",
                                markup : api.local.basemarkupdata?.attribute2

                        ]

                    }
                    if (v.type == "floor") {
                        total_stf << [
                                segment: q.name,
                                type   : "floor",
                                markup : api.local.basemarkupdata?.attribute4

                        ]
                    }
                }
            }
        }
        catch (e) {

            api.local.warnings.add("Error in getTotalSTF with normal pricing - ${e}")
            return null
        }
    }
    if (total_stf.size() < 2) {

        api.local.warnings.add("Error in getTotalSTF accessory pricing - got ${total_stf.size()} values when there should have been at least 2")
        return null
    }
    return total_stf
}