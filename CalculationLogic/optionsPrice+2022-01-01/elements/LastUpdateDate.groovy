// RUUK-180 changes

def lastUpdateDate
// Fetching Rawmaterial PX data
def materialKey = api.local.product.options?.find { it.groupKey.contains("material") }?.optionKey
def thicknessKey = api.local.product.options?.find { it.groupKey.contains("thickness") }?.optionKey
def coatingKey = api.local.product.options?.find { it.groupKey.contains("coating") }?.optionKey
def colorKey = api.local.product.options?.find { it.groupKey.contains("color") }?.optionKey

List filters = [Filter.equal("name", "RawMaterialPrice"),
                Filter.in("sku", [materialKey, "*"]),
                Filter.in("attribute1", [thicknessKey, "*"]),
                Filter.in("attribute3", [coatingKey, "*"])]

def rawMaterialDateFilter = Filter.lessOrEqual("attribute6", api.local.product.pricingDate)

if (colorKey) {
    filters << Filter.in("attribute4", [colorKey, "*"])
}

if (rawMaterialDateFilter) {
    filters << rawMaterialDateFilter
}

Map rawMaterialData = api.find("PX", 0, "-attribute6", *filters)?.find()

// Fetching ComponentProductPrices data
Map price_row = api.find("PX20", 0, 1, "-attribute11", *out.Filters)?.find()


if (api.local.weightBasedProduct && !api.local.componentProductPrices) {
    lastUpdateDate = rawMaterialData?.lastUpdateDate
} else if (api.local.weightBasedProduct && api.local.componentProductPrices) {
    lastUpdateDate = price_row?.lastUpdateDate
} else if(out.CladdingPrices) {
    lastUpdateDate = rawMaterialData?.lastUpdateDate
} else if (out.PanelsPrices) {
    lastUpdateDate = api.local.LastUpdateDatePanels
} else if (api.local.componentProductPrices || out.AccessoriesPrices) {
    lastUpdateDate = price_row?.lastUpdateDate
}

return lastUpdateDate