if (!api.local.priceLogic) {
    return
}

if (api.local.product.cogsPrice == null) {
    return api.local.product.cogsPrice
}

//if (api.local.isCladding  && api.local.optionsPrice?.rawMaterialPrice != 0.0 ) {
//    return new BigDecimal('0')
//}
//if (!api.local.product.cogsPrice | api.local.product.cogsPrice == 0) {
//    return new BigDecimal('0')
//}

BigDecimal cogs_price

if (api.local.componentProductPrices) {
    cogs_price = new BigDecimal(api.local.product.cogsPrice)
    cogs_price = (cogs_price ?: 0) + (out.getOptionsPrice ?: 0)
}

// Cast the received value

if (!api.local.componentProductPrices) {
    cogs_price = new BigDecimal(api.local.product.cogsPrice)
}


// Convert back to EUR if the user has another currency context
if (api.local.product.currency != 'EUR') {

    try {
//Jira ticket RUUK-10 changes - Replace division with multiplication for currency conversion  : Begin
        def converted_cogs_price = cogs_price * api.local.conversionFactor
//Jira ticket RUUK-10 changes - Replace division with multiplication for currency conversion  : Begin

        return converted_cogs_price.setScale(3, BigDecimal.ROUND_HALF_UP) ?: new BigDecimal('0')

    } catch (e) {

        api.local.warnings.add("Error getting converted cogs price for product ${api.local.product.productCode} from currency ${api.local.product.currency} ${e}.")
        return null

    }

}

// If we didn't find any cogs price
return cogs_price.setScale(3, BigDecimal.ROUND_HALF_UP) ?: new BigDecimal('0')