import java.math.RoundingMode

if (!api.local.priceLogic) {
    return
}

if (api.local.isUserinputCalc || api.local.isAccessoryPriceRule || api.local.isWeightedRule) {
    return
}

if (api.local.isCladding && api.local.basemarkupdata == null) {
    return
}

if ((api.local.product.rawMaterialCost > 0.0 && api.local.product.processingCost > 0.0)) {
    return
}

if (api.local.isCladding && out.SpecialMaterial == true) {
    return
}


def coilWidth = api.find("PX", Filter.equal("name", "CladdingProcessingCost"), Filter.equal("sku", api.local.product.productCode))?.getAt(0)?.attribute8
if (api.local.isCladding && out.SpecialMaterial == false) {

    def materialKey = api.local.product.options?.find { it.groupKey.contains("material") }?.optionKey
    def thicknessKey = api.local.product.options?.find { it.groupKey.contains("thickness") }?.optionKey
    def colorKey = api.local.product.options?.find { it.groupKey.contains("color") }?.optionKey
    def coatingKey = api.local.product.options?.find { it.groupKey.contains("coating") }?.optionKey
    def options = api.local.product.options.findAll { it.optionGroupKey == "dimensions" && it.selected == true }


    def length
    def width
    def quantity
    options.each { it ->

        if (it.standard == true) {
            length = it?.optionKey.split("/")[0].split("-")[0] as BigDecimal
            length = length + 5.0
            width = it?.optionKey.split("/")[1].split("-")[0] as BigDecimal
            width = width + 5.0
            quantity = (it?.qty / (length * width / 1000000))?.setScale(0, RoundingMode.HALF_UP)

        } else {
            length = it?.optionKey.split("/")[0] as BigDecimal
            width = it?.optionKey.split("/")[1] as BigDecimal
            quantity = it?.qty
        }

        String rawMaterialTableName = "RawMaterialCOGS"
        api.local.optionsPrice = libs.claddingCOGS.COGSPrice.getOptionsPrice(api.local.product.productCode, length, width, materialKey, thicknessKey, coatingKey, colorKey, api.local.product.pricingDate, quantity, rawMaterialTableName)
        def processingCost = ((api.local.optionsPrice?.processingCost) ?: 0) + (api.local.category1_Optionsprice ?: 0)
        def rawMaterialCost = ((api.local.optionsPrice?.rawMaterialPrice) ?: 0) + (api.local.category2_Optionsprice ?: 0)

        if (length <= 3900 && width <= coilWidth) {

            it?.price = (processingCost + rawMaterialCost)
            it?.rawMaterialCost = rawMaterialCost
            it?.processingCost = processingCost
            it?.totalArea = it.standard == true ? (api.local.optionsPrice?.inputArea ?: 0.0) * (it?.qty / (length * width / 1000000))?.setScale(0, RoundingMode.HALF_UP) : (api.local.optionsPrice?.inputArea ?: 0.0) * it.qty
        } else if (width <= 3900 && length <= coilWidth) {

            it?.price = (processingCost + rawMaterialCost)
            it?.rawMaterialCost = rawMaterialCost
            it?.processingCost = processingCost
            it?.totalArea = it.standard == true ? (api.local.optionsPrice?.inputArea ?: 0.0) * (it?.qty / (length * width / 1000000))?.setScale(0, RoundingMode.HALF_UP) : (api.local.optionsPrice?.inputArea ?: 0.0) * it.qty
        } else {
            def error = "Dimensions Length should not be greater than 3900/ height should not be greater than ${coilWidth}"
            api.local.warnings.add(error)
            it?.price = 0.0
            it?.rawMaterialCost = 0.0
            it?.processingCost = 0.0
            it?.totalArea = 0.0
            return
        }
    }

    options.each { it ->

        if (it.standard == true) {
            length = it?.optionKey.split("/")[0].split("-")[0] as BigDecimal
            length = length + 5.0
            width = it?.optionKey.split("/")[1].split("-")[0] as BigDecimal
            width = width + 5.0
            quantity = (it?.qty / (length * width / 1000000))?.setScale(0, RoundingMode.HALF_UP)

        } else {
            length = it?.optionKey.split("/")[0] as BigDecimal
            width = it?.optionKey.split("/")[1] as BigDecimal
            quantity = it?.qty
        }

        String rawMaterialTableName = "RawMaterialPrice"
        api.local.optionsPrice = libs.claddingCOGS.COGSPrice.getOptionsPrice(api.local.product.productCode, length, width, materialKey, thicknessKey, coatingKey, colorKey, api.local.product.pricingDate, quantity, rawMaterialTableName)
        def processingCost = ((api.local.optionsPrice?.processingCost) ?: 0) + (api.local.category1_Optionsprice ?: 0)
        def rawMaterialCost = ((api.local.optionsPrice?.rawMaterialPrice) ?: 0) + (api.local.category2_Optionsprice ?: 0)

        if (length <= 3900 && width <= coilWidth) {

            it?.newPrice = (processingCost + rawMaterialCost)
            it?.newRawMaterialCost = rawMaterialCost
            it?.newProcessingCost = processingCost
            it?.newTotalArea = it.standard == true ? (api.local.optionsPrice?.inputArea ?: 0.0) * (it?.qty / (length * width / 1000000))?.setScale(0, RoundingMode.HALF_UP) : (api.local.optionsPrice?.inputArea ?: 0.0) * it.qty
        } else if (width <= 3900 && length <= coilWidth) {

            it?.newPrice = (processingCost + rawMaterialCost)
            it?.newRawMaterialCost = rawMaterialCost
            it?.newProcessingCost = processingCost
            it?.newTotalArea = it.standard == true ? (api.local.optionsPrice?.inputArea ?: 0.0) * (it?.qty / (length * width / 1000000))?.setScale(0, RoundingMode.HALF_UP) : (api.local.optionsPrice?.inputArea ?: 0.0) * it.qty
        } else {
            def error = "Dimensions Length should not be greater than 3900/ height should not be greater than ${coilWidth}"
            api.local.warnings.add(error)
            it?.newPrice = 0.0
            it?.newRawMaterialCost = 0.0
            it?.newProcessingCost = 0.0
            it?.newTotalArea = 0.0
            return
        }
    }

}