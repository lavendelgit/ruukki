String materialKey = api.local.product.options?.find { it.groupKey.contains("material") }?.optionKey
List specialMaterials = api.findLookupTableValues("SpecialMaterials", Filter.equal("key1", api.local.product.productType))?.key2
def specialMaterial = (materialKey in specialMaterials) ? true : false
return specialMaterial

