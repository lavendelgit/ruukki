if (api.isDebugMode()) {
    def customerClass = api.stringUserEntry("customerClass")
    def customerSegment = api.stringUserEntry("customerSegment")
    def lineItemNumber = api.integerUserEntry("lineItemNumber")

    def currency = api.stringUserEntry("currency")
    def quoteDate = api.dateUserEntry("creationDate")
    def productCode = api.stringUserEntry("productCode")
    def unitOfMeasure = api.stringUserEntry("unitOfMeasure")
    def language = api.stringUserEntry("language")
    def market = api.stringUserEntry("market")
    def freightPrice = api.userEntry("freightPrice")
    def quantity = api.userEntry("quantity")
    def pricingDate = api.dateUserEntry("pricingDate")
    def logicName = api.stringUserEntry("logicName")
    def rawMaterialCost = api.integerUserEntry("rawMaterialCost")
    def processingCost = api.integerUserEntry("processingCost")

}

if (api.isSyntaxCheck()) {
    api.abortCalculation()
}