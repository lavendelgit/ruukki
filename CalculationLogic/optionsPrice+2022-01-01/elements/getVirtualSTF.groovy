if (!api.local.priceLogic) {
    return
}
// Build virtual start target and floor modifiers from prices


def raw_prices

if (api.local.pmQuantityMarkupdata == null && api.local.isCladding && api.local.product.market == "FI") {
    raw_prices = out.getProductPrices
} else if ((api.local.isCladding && api.local.optionsPrice?.rawMaterialPrice != null && api.local.optionsPrice?.rawMaterialPrice != 0.0)) {
    raw_prices = out.CladdingPrices
} else if (api.local.isCladding && out.SpecialMaterial == true && (api.local.product.rawMaterialCost == 0.0 && api.local.product.processingCost == 0.0)) {
    raw_prices = out.CladdingPrices
} else if ((api.local.isCladding && api.local.specialMaterial == true && (api.local.product.rawMaterialCost > 0.0 && api.local.product.processingCost > 0.0))) {
    raw_prices = out.UserinputPrices
} else if (api.local.accessoryCogs) {
    raw_prices = out.AccessoriesPrices
} else if (api.local.userInputPrices) {
    raw_prices = out.UserinputPrices
} else if (api.local.panelsPricing) {
    raw_prices = out.PanelsPrices
} else if (api.local.isWeightedRule && out.SpecialMaterial == false) {
    raw_prices = out.getProductPrices
} else {
    raw_prices = out.getProductPrices
}

if (!raw_prices) {
    return null
}


try {

    // Calculate the virtual stf, this works for both regular and special pricing logic
    def virtual_stf = raw_prices?.stfprices.collect { k, v ->

        [type    : k
         , markup: api.local.componentProductPrices ? ((v as BigDecimal) / raw_prices?.basePrice) : 0.0]
    }


    if (virtual_stf) {
        // For normal pricing, we have always three modifiers (STF) for regular pricing
        // we only have 2 (A1 and A8) and for accessories we also have 2
        if (virtual_stf.size() < 2) {
            api.local.warnings.add("Expected at least 2 price modifiers but only ${virtual_stf.size()} could be obtained for product ${api.local.product.productCode}.")
            return null
        }
        api.local.warnings.add("Virtual price modifiers ${virtual_stf}")

        return virtual_stf
    }

    api.local.warnings.add("No virtual price modifiers could be calculated for product ${api.local.product.productCode}.")

    return null

} catch (e) {
    api.local.warnings.add("Error trying to calculate virtual price modifier for product ${api.local.product.productCode} - ${e}")
    return null

}
