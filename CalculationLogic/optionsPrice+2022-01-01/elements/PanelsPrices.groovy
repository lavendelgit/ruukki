if (!api.local.priceLogic) {
    return
}
if ((api.local.isPanelPricing && api.local.basemarkupdata == null) || (api.local.isPanelPricing && api.local.pmQuantityMarkupdata == null)) {
    return
}

BigDecimal cogsPrice
BigDecimal transferPrice

if (api.local.isPanelPricing && !(api.local.product.rawMaterialCost > 0.0 && api.local.product.processingCost > 0.0)) {

    api.local.panelsPricing = true

// EPIR panels:materialcost = sheetsactual+foam actual+packingmaterialsactual+protectivefoilactual+gasketandAlfoilactual+primeractual
//   mineralWoolPanels: sheetaactual+glueactual+packingmaterialsactual+protectivefoilactual+mineralwoolactual


    BigDecimal costActual
    String panelType = api.product("Categories", api.local.product.productCode)

    api.local.panelNormData = api.find("PX", Filter.equal("name", "PanelsNorms"),
            Filter.equal("sku", api.local.product.productCode))?.find()

    api.local.panelPricesData = api.find("PX", Filter.equal("name", "PanelsPrices"),
            Filter.equal("sku", api.local.product.productCode))

    api.local.panelNormDataCOGS = api.find("PX", Filter.equal("name", "PanelsNormsCOGS"),
            Filter.equal("sku", api.local.product.productCode))?.find()
    api.local.panelPricesDataCOGS = api.find("PX", Filter.equal("name", "PanelsPricesCOGS"),
            Filter.equal("sku", api.local.product.productCode))
    List pricesData = []
    api.local.panelPricesData.each { it ->
        BigDecimal externalSheetPrice = it?.attribute8 ?: BigDecimal.ZERO
        BigDecimal internalSheetPrice = it?.attribute10 ?: BigDecimal.ZERO

        BigDecimal internalsheetNorm = api.local.panelNormData?.attribute1 ?: BigDecimal.ZERO
        BigDecimal externalsheetNorm = api.local.panelNormData?.attribute2 ?: BigDecimal.ZERO

        BigDecimal sheetsActual = (internalSheetPrice * internalsheetNorm) + (externalSheetPrice * externalsheetNorm)

        BigDecimal foamNorm = api.local.panelNormData?.attribute3 ?: BigDecimal.ZERO
        BigDecimal foamPrice = it?.attribute1 ?: BigDecimal.ZERO

        BigDecimal foamActual = (foamNorm * foamPrice) ?: BigDecimal.ZERO

        BigDecimal packingMaterialsActuals = (api.local.panelNormData?.attribute11) ?: BigDecimal.ZERO

        BigDecimal foilnorm = api.local.panelNormData?.attribute5 ?: BigDecimal.ZERO
        BigDecimal foilPrice = it?.attribute3 ?: BigDecimal.ZERO

        BigDecimal protectiveFoilActual = (foilnorm * foilPrice) ?: BigDecimal.ZERO

        BigDecimal puGasketnorm = api.local.panelNormData?.attribute6 ?: BigDecimal.ZERO
        BigDecimal alfoilNorm = api.local.panelNormData?.attribute7 ?: BigDecimal.ZERO

        BigDecimal puGasketPrice = it?.attribute4 ?: BigDecimal.ZERO
        BigDecimal alfoilPrice = it?.attribute5 ?: BigDecimal.ZERO

        BigDecimal gasketandAlfoilactual = ((puGasketnorm * puGasketPrice) + (alfoilNorm * alfoilPrice)) ?: 0

        BigDecimal primerNorm = api.local.panelNormData?.attribute4 ?: BigDecimal.ZERO
        BigDecimal primerPrice = it?.attribute2 ?: BigDecimal.ZERO

        BigDecimal primerActual = (primerNorm * primerPrice) ?: BigDecimal.ZERO

        BigDecimal glueNorm = api.local.panelNormData?.attribute8 ?: BigDecimal.ZERO
        BigDecimal gluePrice = it?.attribute6 ?: BigDecimal.ZERO

        BigDecimal glueActual = glueNorm * gluePrice

        BigDecimal mineralWoolNorm = api.local.panelNormData?.attribute9 ?: BigDecimal.ZERO
        BigDecimal mineralWoolPrice = it?.attribute7 ?: BigDecimal.ZERO

        BigDecimal mineralWoolActual = mineralWoolNorm * mineralWoolPrice

        BigDecimal paperGasketHumbPrice = it?.attribute9 ?: BigDecimal.ZERO
        BigDecimal paperGasketHumbNorm = api.local.panelNormData?.attribute13 ?: BigDecimal.ZERO

        BigDecimal paperGasketHumbActual = paperGasketHumbPrice * paperGasketHumbNorm

        costActual = sheetsActual + foamActual + packingMaterialsActuals + protectiveFoilActual + gasketandAlfoilactual + primerActual + glueActual + mineralWoolActual + paperGasketHumbActual

        BigDecimal processingCost = (api.local.panelNormData?.attribute10) ?: BigDecimal.ZERO

        costActual = costActual + (api.local.category2_Optionsprice ?: BigDecimal.ZERO)

        processingCost = processingCost + (api.local.category1_Optionsprice ?: BigDecimal.ZERO)

        cogsPrice = costActual + processingCost
        BigDecimal transferMarkup = (api.local.panelNormData?.attribute12) ?: BigDecimal.ZERO
        transferPrice = costActual + processingCost * (1 + transferMarkup)
        Map map = [:]
        map['cogsPrice'] = cogsPrice
        map['transferPrice'] = transferPrice
        map['validFrom'] = it?.attribute11
        map['lastUpdateDate'] = it?.lastUpdateDate
        pricesData << map
    }

    List newPricesData = []
    api.local.panelPricesDataCOGS.each { it ->
        BigDecimal externalSheetPriceCOGS = it?.attribute8 ?: BigDecimal.ZERO
        BigDecimal internalSheetPriceCOGS = it?.attribute10 ?: BigDecimal.ZERO

        BigDecimal internalsheetNormCOGS = api.local.panelNormDataCOGS?.attribute1 ?: BigDecimal.ZERO
        BigDecimal externalsheetNormCOGS = api.local.panelNormDataCOGS?.attribute2 ?: BigDecimal.ZERO

        BigDecimal sheetsActualCOGS = (internalSheetPriceCOGS * internalsheetNormCOGS) + (externalSheetPriceCOGS * externalsheetNormCOGS)

        BigDecimal foamNormCOGS = api.local.panelNormDataCOGS?.attribute3 ?: BigDecimal.ZERO
        BigDecimal foamPriceCOGS = it?.attribute1 ?: BigDecimal.ZERO

        BigDecimal foamActualCOGS = (foamNormCOGS * foamPriceCOGS) ?: BigDecimal.ZERO

        BigDecimal packingMaterialsActualsCOGS = (api.local.panelNormDataCOGS?.attribute11) ?: BigDecimal.ZERO

        BigDecimal foilnormCOGS = api.local.panelNormDataCOGS?.attribute5 ?: BigDecimal.ZERO
        BigDecimal foilPriceCOGS = it?.attribute3 ?: BigDecimal.ZERO

        BigDecimal protectiveFoilActualCOGS = (foilnormCOGS * foilPriceCOGS) ?: BigDecimal.ZERO

        BigDecimal puGasketnormCOGS = api.local.panelNormDataCOGS?.attribute6 ?: BigDecimal.ZERO
        BigDecimal alfoilNormCOGS = api.local.panelNormDataCOGS?.attribute7 ?: BigDecimal.ZERO

        BigDecimal puGasketPriceCOGS = it?.attribute4 ?: BigDecimal.ZERO
        BigDecimal alfoilPriceCOGS = it?.attribute5 ?: BigDecimal.ZERO

        BigDecimal gasketandAlfoilactualCOGS = ((puGasketnormCOGS * puGasketPriceCOGS) + (alfoilNormCOGS * alfoilPriceCOGS)) ?: 0

        BigDecimal primerNormCOGS = api.local.panelNormDataCOGS?.attribute4 ?: BigDecimal.ZERO
        BigDecimal primerPriceCOGS = it?.attribute2 ?: BigDecimal.ZERO

        BigDecimal primerActualCOGS = (primerNormCOGS * primerPriceCOGS) ?: BigDecimal.ZERO

        BigDecimal glueNormCOGS = api.local.panelNormDataCOGS?.attribute8 ?: BigDecimal.ZERO
        BigDecimal gluePriceCOGS = it?.attribute6 ?: BigDecimal.ZERO

        BigDecimal glueActualCOGS = glueNormCOGS * gluePriceCOGS

        BigDecimal mineralWoolNormCOGS = api.local.panelNormDataCOGS?.attribute9 ?: BigDecimal.ZERO
        BigDecimal mineralWoolPriceCOGS = it?.attribute7 ?: BigDecimal.ZERO

        BigDecimal mineralWoolActualCOGS = mineralWoolNormCOGS * mineralWoolPriceCOGS

        BigDecimal paperGasketHumbPriceCOGS = it?.attribute9 ?: BigDecimal.ZERO
        BigDecimal paperGasketHumbNormCOGS = api.local.panelNormDataCOGS?.attribute13 ?: BigDecimal.ZERO

        BigDecimal paperGasketHumbActualCOGS = paperGasketHumbPriceCOGS * paperGasketHumbNormCOGS

        costActualCOGS = sheetsActualCOGS + foamActualCOGS + packingMaterialsActualsCOGS + protectiveFoilActualCOGS + gasketandAlfoilactualCOGS + primerActualCOGS + glueActualCOGS + mineralWoolActualCOGS + paperGasketHumbActualCOGS

        BigDecimal processingCostCOGS = (api.local.panelNormDataCOGS?.attribute10) ?: BigDecimal.ZERO

        costActualCOGS = costActualCOGS + (api.local.category2_Optionsprice ?: BigDecimal.ZERO)

        processingCostCOGS = processingCostCOGS + (api.local.category1_Optionsprice ?: BigDecimal.ZERO)

        newCogsPrice = costActualCOGS + processingCostCOGS
        BigDecimal newTransferMarkup = (api.local.panelNormDataCOGS?.attribute12) ?: BigDecimal.ZERO
        newTransferPrice = costActualCOGS + processingCostCOGS * (1 + newTransferMarkup)
        Map map = [:]
        map['newCogsPrice'] = newCogsPrice
        map['newTransferPrice'] = newTransferPrice
        map['newValidFrom'] = it?.attribute11
        map['newLastUpdateDate'] = it?.lastUpdateDate
        newPricesData << map
    }

    Closure prices = {}
    BigDecimal target_A1
    BigDecimal target_A8
    BigDecimal start_A1
    BigDecimal start_A8
    BigDecimal floor_A1
    BigDecimal floor_A8
    BigDecimal targetprice
    String today = new Date().format('yyyy-MM-dd')
    if (api.local.product.pricingDate) {
        validRecords = pricesData?.findAll { it.validFrom <= api.local.product.pricingDate }
        newValidRecords = newPricesData?.findAll { it.newValidFrom <= api.local.product.pricingDate }
    } else {
        validRecords = pricesData?.findAll { it.validFrom <= today }
        newValidRecords = newPricesData?.findAll { it.newValidFrom <= today }
    }
    Map validRecord = validRecords?.sort { a, b -> b.validFrom <=> a.validFrom }?.getAt(0)
    Map newValidRecord = newValidRecords?.sort { a, b -> b.newValidFrom <=> a.newValidFrom }?.getAt(0)
    api.local.LastUpdateDatePanels = validRecord?.lastUpdateDate
    api.local.product.cogsPrice = newValidRecord?.newCogsPrice
    api.local.transferPrice = newValidRecord?.newTransferPrice
    prices?.basePrice = (newValidRecord?.newCogsPrice ?: 0.0) * (1.0 + (api.local.basemarkupdata?.attribute1))
    api.local.panelBasePrice = (validRecord?.cogsPrice ?: 0.0) * (1.0 + (api.local.basemarkupdata?.attribute1))

    if (api.local.product.market != "FI") {
        targetprice = (api.local.panelBasePrice as BigDecimal) * ((1.0 + (api.local.basemarkupdata?.attribute5)) as BigDecimal)
        BigDecimal startprice = ((targetprice as BigDecimal) * ((1 + (api.local.basemarkupdata?.attribute2)) as BigDecimal))
        BigDecimal floorprice = (targetprice as BigDecimal) * (1.0 - (api.local.basemarkupdata?.attribute4) as BigDecimal)

        prices.stfprices = [start   : startprice
                            , target: targetprice
                            , floor : floorprice]
        return prices
    } else if (api.local.product.market == "FI") {

        BigDecimal targetA8 = (api.local.panelBasePrice as BigDecimal) * ((1.0 + (api.local.basemarkupdata?.attribute5)) as BigDecimal)
        BigDecimal targetA1 = (api.local.panelBasePrice as BigDecimal) * (1.0 + api.local.pmQuantityMarkupdata?.attribute3) as BigDecimal
        prices.stfprices = [target_A1  : targetA1
                            , target_A8: targetA8
                            , floor_A1 : targetA1 * (1.0 - (api.local.basemarkupdata?.attribute4) as BigDecimal)
                            , floor_A8 : targetA8 * (1.0 - (api.local.basemarkupdata?.attribute4) as BigDecimal)
                            , start_A1 : targetA1 * (1.0 + (api.local.basemarkupdata?.attribute2) as BigDecimal)
                            , start_A8 : targetA8 * (1.0 + (api.local.basemarkupdata?.attribute2) as BigDecimal)]
        return prices
    }
}