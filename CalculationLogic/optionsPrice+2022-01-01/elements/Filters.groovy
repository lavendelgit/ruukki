def filters = [
        Filter.equal("name", "ComponentsProductPrices"),
        Filter.or(
                Filter.equal("attribute1", api.local.product.productCode),
                Filter.equal("sku", api.local.product.productCode)
        ),
        Filter.equal("attribute3", api.local.product.market)
]
def priceDateInput = api.local.product.pricingDate
def priceDate
def priceDateFormatted
if (priceDateInput) {
    priceDate = Date.parse("yyyy-MM-dd", priceDateInput)
    priceDateFormatted = priceDate.format("yyyy-MM-dd'T'HH:mm:ss.SSS")
    filters << Filter.or(Filter.lessOrEqual("attribute11", priceDateFormatted), Filter.isNull("attribute11"))
}

return filters