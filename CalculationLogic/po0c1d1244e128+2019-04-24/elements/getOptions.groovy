try {
    
  if (api.local.product.optionGroup && api.local.product.optionGroup.size() > 0) {
    
	api.local.product.optionGroup = libs.options.getAvailable.main(api.local.product)
    
  
  } else {
    
      api.local.product.optionGroup = libs.options.getDefaults.main(api.local.product)
    
  }
  
} catch (e) {
  
  api.logWarn("Options module error: ", e)
  
  api.local.error = "No options found for product ${api.local.product.productCode} in market ${api.local.product.market}"
  
}
