api.local.product = [:]
api.local.product.optionGroup	= []

api.local.product.creationDate  = api.input("creationDate")
api.local.product.productCode   = api.input("productCode")
api.local.product.language      = api.input("language")
api.local.product.market        = api.input("market")
api.local.product.optionGroup   = api.input("selectedOptions")
api.local.product.currency      = api.input("currency")

api.local.error = null
