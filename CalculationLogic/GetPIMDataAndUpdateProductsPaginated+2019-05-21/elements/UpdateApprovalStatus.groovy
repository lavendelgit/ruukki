if (api.local.updatedProducts) {
    Iterator streamedData = api.stream("P", null, ["attribute7", "sku"],
            Filter.notIn("sku", api.local.updatedProducts))

    streamedData?.withCloseable {
        it.each { item ->
            api.update("P", ["sku"       : item.sku ?: "",
                             "attribute7": ""])
        }
    }
}