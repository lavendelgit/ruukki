if (api.syntaxCheck) {
  return
}   

/*
  Object httpCall(String url,
  String body,
  String methodType,
  String contentType,
  Map... properties)
  Returns:
  Map containing two elements 'responseBody' and 'statusCode'
*/

def settings = api.getElement("credentials")

api.trace(settings)
 
try {

  def host          = settings.ip
  def auth_endpoint = settings.authendpoint
  def auth_query    = settings.authquery
  def auth_props    = [:]
  auth_props.sslTrustAllCertificates = true
  auth_props.additionalHeaders = [ 'Authorization' : settings.header ]

  // Call the Oauth endpoint to get a token
  def auth_result = api.httpCall(
      "${host}${auth_endpoint}${auth_query}"
    , ''
    , 'POST'
    , 'application/x-www-form-urlencoded'
    , auth_props
  )

  if (auth_result.statusCode == '200') {

    def parsed = api.jsonDecode(auth_result.responseBody.toString())
    def token = parsed['access_token']
    
    api.trace("token")

    return token

  } else {

  	api.throwException("PIM API ACCESS ERROR --- ${auth_result.toString()}")

  }

} catch(e) {

  api.throwException("PIM API ACCESS ERROR --- ${e.toString()}")

}
