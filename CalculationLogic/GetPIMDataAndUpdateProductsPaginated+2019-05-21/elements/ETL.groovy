/*
 * Get the data in small amount and update directly instead of accumulating
 */

if (api.syntaxCheck) {
    return
}

api.local.updatedProducts = []
api.logInfo("JOB Start-----------")
def token = api.getElement("getToken")
def settings = api.getElement("credentials")
def page_size = settings.pagesize
def lang = 'en'
def host = settings.ip
def data_endpoint = settings.dataendpoint
def data_props = [:]
data_props.sslTrustAllCertificates = true
data_props.additionalHeaders = [
        'Authorization': "Bearer ${token}",
        'Accept'       : 'application/json'
]

// Initial looping conditions
def current_page = 0
def total_pages = 1
def total_products_updated = 0

try {

    while (current_page <= total_pages) {

        api.logInfo("page ${current_page} of ${total_pages}")

        def data_query = "?fields=COMPONENTS_EXPORT&lang=${lang}&currentPage=${current_page}&pageSize=${page_size}"
        def data_url = "${host}${data_endpoint}${data_query}"
        def data_result = api.httpCall(
                data_url
                , ''
                , 'GET'
                , 'application/json'
                , data_props
        )

        if (data_result.statusCode == '200' && data_result.responseBody.size() > 0) {

            def raw_data = api.jsonDecode(data_result.responseBody)

            try {

                def transformed_data = raw_data.products.collect {

                    [
                            productCode            : it.code
                            , commercialProductCode: it.commercialProductCode
                            , baseProduct          : it.baseProduct
                            , name                 : it.name
                            , type                 : it.productType
                            , categories           : it.categories ? it.categories.first().code : ""
                            , markets              : it.classifications.findAll { classification ->
                        classification.code == 'RuukkiComponentProduct'
                    }.collect { p ->
                        p.features.findAll { feature ->
                            feature.code == 'ruukkicomponentsFiClassification/1.0/RuukkiComponentProduct.markets'
                        }.collect { item ->
                            item.featureValues.collect { a -> a.code }
                        }
                    }.flatten()
                            , approvalStatus       : it.approvalStatus
                    ]
                }

                //def filtered_data = transformed_data.findAll { it.categories != null }
                //def deleted_data = transformed_data.findAll { it.categories == null }
                if (transformed_data) {
                    for (product in transformed_data) {

                        // Update Product Master
                        api.addOrUpdate("P", [
                                "sku"         : product.productCode
                                , "label"     : product.productCode
                                , "currency"  : product.currency ?: ""
                                , "attribute1": product.baseProduct ?: ""
                                , "attribute2": product.productCode ?: ""
                                , "attribute3": product.name ?: ""
                                , "attribute4": product.type ?: ""
                                , "attribute5": product.markets?.toString() ?: ""
                                , "attribute6": product.categories ?: ""
                                , "attribute7": product.approvalStatus ?: ""
                        ])
                        api.local.updatedProducts << product.productCode
                    }
                }
//                for (product in transformed_data) {
//                    // Update Product Master
//                    api.addOrUpdate("P", [
//                            "sku"         : product.productCode
//                            , "label"     : product.productCode
//                            , "currency"  : product.currency ?: ""
//                            , "attribute1": product.baseProduct ?: ""
//                            , "attribute2": product.productCode ?: ""
//                            , "attribute3": product.name ?: ""
//                            , "attribute4": product.type ?: ""
//                            , "attribute5": product.markets ?: ""
//                            , "attribute6": product.categories ?: ""
//                            , "attribute7": product.approvalStatus ?: ""
//                    ])
//
//                }

                /*if ( deleted_data && deleted_data.size() > 0 ) {

                  for ( product in deleted_data ) {

                    // TODO we need to lookup the product to see if it actually exists

                     api.delete("P", ["sku": product.productCode])

                  }

                }*/

            } catch (e) {
                api.throwException("Exception in ETL: "+ e.getMessage())
                api.logInfo("Failed to parse objects at page ${current_page} of ${total_pages}")
                api.logInfo("raw_data",raw_data)
                api.dump()
                  
                continue

            }

            total_pages = raw_data.totalPageCount
            total_products_updated = raw_data.totalProductCount
            current_page = raw_data.currentPage + 1

        }

    }


    libs.slack.sendMessage.webhookCall("Fetched and updated ${total_products_updated} products.")

    return true

} catch (e) {

    //libs.slack.sendMessage.webhookCall("Failed to update products.")

    api.throwException("PIM API ACCESS ERROR --- ${e.toString()}")

}
    
 