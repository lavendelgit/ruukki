
try {

  // Options prices is always 0 for accessories
  def final_base_price = out.getFinalBasePrice
  def stf_multipliers = out.getTotalSTF
  
  if (!stf_multipliers) {
    
      def error = "Cannot compute STF prices with missing multipliers for product ${api.local.product.productCode}."
  	  api.local.warnings.add(error)
      return null
  }
  
  if (!final_base_price) {
    
    def error = "Missing base price for product ${api.local.product.productCode}."
  	  api.local.warnings.add(error)
      return null
    
  }

  // Muliply the total price for each stf
  def stf_prices = stf_multipliers.collect {
    [
        segment : it.segment
      , type    : it.type
      , price   : it.markup * final_base_price
    ]
  }

  return stf_prices

} catch (e) {

  def error = "Error calculating STF prices for product ${api.local.product.productCode} - ${e}."
  api.local.warnings.add(error)

  return null

}
  
