
try {

  def final_stf_prices = api.local.product.currency != 'EUR' ? out.getConvertedSTFPrices: out.getFinalSTFPrices
  
  if (!final_stf_prices) {

    def error = "No STF prices to format for product ${api.local.product.productCode}."
    api.local.warnings.add(error)
    return null

  }

  def output = final_stf_prices.groupBy { it.segment }.collect { it ->
    [
            'quantitySegment' : api.local.product.isAccessory ? 'any' : it.key,
            'value': it.value.collect { t ->
                [
                  'markupType' : t.type,
                  'price'      : t.price.setScale(3, BigDecimal.ROUND_HALF_UP)
                ]
            }
    ]
  }

  return output


} catch (e) {
  
  api.local.warnings.add("Error formatting final STF prices for product ${api.local.product.productCode}")
  
  return null
  
}