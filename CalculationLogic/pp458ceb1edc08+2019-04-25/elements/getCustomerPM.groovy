// Get customer-dependant price markup


// THIS LOGIC IS DISABLED UNTIL FURTHER NOTICE (4/12/2019 -- adrien, after call with Henri Polvi)

/*
def customer_pm = api.vLookup( 
    'PM_Customers'
  , 'Markup'
  , api.local.product.market
  , api.local.customerClass
  , api.local.customerSegment
)

// If we don't have any STF, add a warning and return 0 so we don't do any actual price modification
if (!customer_pm) {

  def error = "No Customer markup data for product ${api.local.product.productCode} and combination market ${api.local.product.market}, customer class ${api.local.customerClass}, customer segment ${api.local.customerSegment}}."
  api.local.warnings.add(error)

  return new BigDecimal('0')

}

return customer_pm

*/

return new BigDecimal('0')