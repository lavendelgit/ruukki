

// Accessories only have STF, not Superior Extras, so we get the prices accordingly
try {
  
 	def multiplicated_prices = api.local.product.isAccessory == false ? out.getSuperiorExtrasPrice : out.getSTFPrice
  
	return multiplicated_prices
  
} catch (e) {
  
    api.local.warnings.add("Error getting final STF prices for product ${api.local.product.productCode} ${e}.")

	return null
}
 