api.local.product_without_group = ['LIBERTA', 'DESIGN_PROFILE', 'CLADDING_LAMELLA', 'PROFILE', 'PURLIN', 'LOAD_BEARING_PROFILE']

/*
   NOTE THESE AREN'T VALID ANYMORE FOR CHG004 BECAUSE WE ARE USING CATEGORY

  B - LOAD_BEARING_PROFILE - HAS BASE PRODUCT AND USED AS "LOAD_BEARING_PROFILE" IN STF TABLE
  C - CLADDING_LAMELLA     - NO BASE PRODUCT AT ALL
  D - DESIGN_PROFILE       - NO BASE PRODUCT AT ALL
  L - LIBERTA              - NO BASE PRODUCT AT ALL
  S - PROFILE              - NO BASE PRODUCT AT ALL
  U - PURLIN               - HAS BASE PRODUCT AND USED AS "PURLIN" IN STF TABLE
*/

// Retrieve a reference to the product so we can have the Product Group
def sku_filter = Filter.equal("sku", api.local.product.productCode)
def code_filter = Filter.equal("attribute2", api.local.product.productCode)
api.trace("sku_filter", sku_filter)
try {

    def product_rows = api.find("P", sku_filter, code_filter)

    if (product_rows.size() > 1) {

        api.local.product.isAccessory = false
        api.local.product.baseProduct = ''
        api.local.product.productType = ''
        api.local.product.productCategory = ''
        api.local.product.productGroup = null

        def error = "Duplicate entries in product table for product ${api.local.product.productCode}"
        api.local.warnings.add(error)
        return

    }

    def product = product_rows.getAt(0)

    // Set a flag if it's an accessories so we can route the options pricing logic as needed
    // downstream because accessories only require
    api.local.product.isAccessory = product.attribute6.contains('accessories_') ? true : false
    api.local.product.baseProduct = product.attribute1 ? product.attribute1 : ''
    api.local.product.productType = product.attribute4 ?: ''
    api.local.product.productCategory = product.attribute6 ?: ''
    api.local.product.specialPricing = false
    api.local.product.isProductWithoutGroup = api.local.product_without_group.any { it.contains(product.attribute4) }

    // For normal product without a group or for accessories, the 'productGroup' attribute we are using is the type of product
    // For normal products with proper base product, it's the base product code
    api.local.product.productGroup = api.local.product.isProductWithoutGroup || api.local.product.isAccessory == true ? product.attribute4 : product.attribute1 ? product.attribute1 : null

    if (!api.local.product.productGroup) {
        def error = "No product group for product ${api.local.product.productCode}"
        api.local.warnings.add(error)
    }

} catch (e) {

    // Set null if no base product in the table
    api.local.product.productGroup = null
    def error = "No product group for product ${api.local.product.productCode}"
    api.local.warnings.add(error)

}
