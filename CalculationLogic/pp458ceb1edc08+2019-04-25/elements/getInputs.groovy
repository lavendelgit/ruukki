// Get the inputs via the endpoint
// when testing use the stringUserEntry block


//RUUK-41 change start
//Getting the current partition and setting the global variable accordingly
def String Currpart = api.currentPartitionName()
if (Currpart == "brcp5de644e3") {
    api.global.env = 'prod'
} else if (Currpart == "bynfo_ruukki_qa") {
    api.global.env = 'dev'
}
//RUUK-41 changes end

api.local.product = [:]

api.local.customerClass = api.input("customerClass")
api.local.customerSegment = api.input("customerSegment")
api.local.lineItemNumber = api.input("lineItemNumber")

api.local.product.currency = api.input("currency")
api.local.product.quoteDate = api.input("creationDate")
api.local.product.productCode = api.input("productCode")
api.local.product.unitOfMeasure = api.input("unitOfMeasure")
api.local.product.language = api.input("language")
api.local.product.market = api.input("market")
api.local.product.freightPrice = api.input("freightPrice")
api.local.product.quantity = api.input("quantity")
api.local.product.options = api.input("selectedOptions") ?: [:]
// Jira ticket RUUK-7 Changes : Begin
api.local.product.pricingDate = api.input("pricingDate") ?: null
// Jira ticket RUUK-7 Changes : End
// Define a warning placeholder
api.local.warnings = []

// Not for options!
api.local.product.each {
    if ((!it.value || it.value == null) && it.key != 'options') {
        api.local.warnings.add("Missing input value for ${it.key}.")
    }
}