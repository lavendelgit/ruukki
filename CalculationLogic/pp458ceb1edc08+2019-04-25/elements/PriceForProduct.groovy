

def product_stf_prices = out.formatSTFPrices
def product_base_price = out.formatBasePrice
def freight_price = out.getFreightPrice
def cogs_price = out.getCogsPrice

try {
  
   // Delete the quantity segment field from the prices for Accessories
  // as it is not needed in the price response
  if ( api.local.product.isAccessory == true && product_stf_prices) {
    product_stf_prices[0].remove('quantitySegment')
  }

  def product_attributes = [
      productCode   : api.local.product.productCode
    , lastUpdateDate   : api.local.product.lastUpdateDate
    , isAccessory   : api.local.product.isAccessory
    , unitOfMeasure : api.local.product.unitOfMeasure
    , basePrice     : product_base_price
    , cogsPrice     : cogs_price
    , prices        : product_stf_prices
  ]
  
  // Append options and other attributes for non-accessories
  if ( api.local.product.isAccessory == false ) {
    product_attributes.putAll(
      [
          productGroup : api.local.product.productGroup  
        , quantity     : api.local.product.quantity
        , freightPrice : freight_price 

      ]
    )
  }

  def output = [
        market          : api.local.product.market
      , lang            : api.local.product.language
      , customerClass   : api.local.customerClass
      , lineItemNumber  : api.local.lineItemNumber
      , customerSegment : api.local.customerSegment
      , currency        : api.local.product.currency
      , pricingDate     : api.targetDate()?.format("yyyy-MM-dd HH:mm:ss Z")
      , product         : product_attributes
  ]

  // If the product is an accessory, check if a quantity segment has been set else use A8
  if ( api.local.product.isAccessory == false ) {
    output.putAll(
            [
                    quantitySegment : api.local.product.quantitySegment ?: 'A8'
            ]
    )
  }
  
  // Add warnings in test and Dev only
  if (api.global.env != 'prod') {
      
    if ( api.local.warnings.size() > 0 ) {
      api.local.warnings.each { w ->
        api.addWarning("${w}")
      }
    }
  }


  
  return output
  
} catch (e) {
  
  api.logWarn("Error building output for product ${api.local.product.productCode} ------- ${e}")
  
}


// If any warnings, add to server logs

