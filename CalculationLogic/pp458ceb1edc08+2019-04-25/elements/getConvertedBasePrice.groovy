
if ( api.local.product.currency != 'EUR' ) {
  
  try {
    
   
     def final_base_price = api.getElement('getFinalBasePrice')
    
     if (!final_base_price) {
        api.local.warnings.add("No final base price to convert for product ${api.local.product.productCode} and currency ${api.local.product.currency}.")
		return null
	 }
    
     def converted_base_price = final_base_price * api.local.conversionFactor

     return converted_base_price
      
  } catch (e) {
  
    api.local.warnings.add("Error getting converted base price for product ${api.local.product.productCode} with currency ${api.local.product.currency} ${e}.")
    return null
  
  }

}


  
