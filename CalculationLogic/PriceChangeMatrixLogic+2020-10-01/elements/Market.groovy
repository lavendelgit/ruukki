String sku = api.product("sku")
if (api.isDebugMode()) {
    sku = "O0036"
}
return (getMarkets(sku) + getMarketsFromPriceEntries(sku)).unique()

List getMarkets(String sku) {
    return libs.SharedLib.CacheUtils.getOrSet("PRODUCT_MARKETS", {
        return libs.SharedLib.StreamUtils.stream("P", "sku", ["sku", "attribute5"], null).collectEntries { Object productEntry ->
            return [(productEntry.sku): libs.RukkiCommonLib.CommonUtils.marketStringToList(productEntry.attribute5)]
        }
    })?.getAt(sku) ?: []
}

List getMarketsFromPriceEntries(String sku) {
    return libs.SharedLib.CacheUtils.getOrSet("PRODUCT_MARKET_ENTRIES", {
        return libs.SharedLib.StreamUtils.stream("PX", "sku", ["sku", "attribute3"], [Filter.equal("name", "ComponentsProductPrices")]).groupBy { Object productMarketEntry ->
            productMarketEntry.sku
        }?.collectEntries { String skuKey, List productMarketEntries ->
            return [(skuKey): productMarketEntries?.attribute3?.unique()]
        }
    })?.getAt(sku) ?: []
}
