
def IS_NULL (args){
    if(args == null || args?.size() < 1) api.throwException("Invalid arguments ${args}")
    def arg = args?.getAt(0)
    return arg == null
}

def AS_STRING (args){
    if(args == null || args?.size() < 1) api.throwException("Invalid arguments ${args}")
    def arg
    return args?.inject([]){res,it->
        arg = it as String
        arg = arg?.trim()
        arg = arg?.substring(1, arg.size() - 1)
        res.add(arg)
        return res
    }?.join(",")

}


def SIZE (args){
    if(args == null || args?.size() < 1) api.throwException("Invalid arguments ${args}")
    def arg = args?.getAt(0)
    return arg?.size()
}

def IF (args){
    if(args == null || args?.size() < 3) api.throwException("Invalid arguments ${args}")
    def condition = args?.getAt(0)
    def thenTrue = args?.getAt(1)
    def thenFalse = args?.getAt(2)
    if (condition == true) {
        return thenTrue
    } else {
        return thenFalse
    }
}


def getSupportedOperator() {
    return getSupportedOperationConfiguration()?.getAt("config")?.collect {
        it.name
    }?.sort {
        a, b -> b.size() <=> a.size()
    }
}

def getWeightedSupportedOperator(sortBy) {
    return getSupportedOperationConfiguration()?.getAt("config")?.sort {
        a, b ->
            switch (sortBy) {
                case "name":
                    b?.getAt(sortBy).size() <=> a?.getAt(sortBy).size()
                    break
                case "weight":
                    b?.getAt(sortBy) <=> a?.getAt(sortBy)
                    break
                default:
                    0
                    break
            }

    }
}

def getOperatorConfig(operator){
    return getWeightedSupportedOperator()?.find {
        it?.getAt("name") == operator
    }
}

def getOperatorWeight(operator) {
    return operator == null ? -1000 : getOperatorConfig(operator)?.getAt("weight")
}

def getLeftLogical() {
    return getSupportedOperationConfiguration()?.getAt("config")?.findAll {it.type == "left-logical"}?.collect {
        it.name
    }?.sort {
        a, b -> b.size() <=> a.size()
    }
}


def getFunctionalOperator() {
    return getSupportedOperationConfiguration()?.getAt("config")?.findAll {it.type == "functional"}?.collect {
        it.name
    }?.sort {
        a, b -> b.size() <=> a.size()
    }
}

def getRightHandOperator() {
    return getSupportedOperationConfiguration()?.getAt("config")?.findAll {!["left-logical","functional"].contains(it.type)}?.collect {
        it.name
    }?.sort {
        a, b -> b.size() <=> a.size()
    }
}

def getSupportedOperationConfiguration() {
    return [type:["calculation","logical","left-logical","comparision","functional"],
            config:[
                    [
                            name: "+",
                            weight: 1,
                            type:"calculation",
                            preEvaluate:true,
                            executor:{x,y -> x+y}
                    ],
                    [
                            name: "-",
                            weight: 1,
                            type:"calculation",
                            preEvaluate:true,
                            executor:{x,y -> x-y}
                    ],
                    [
                            name: "*",
                            weight: 2,
                            type:"calculation",
                            preEvaluate:true,
                            executor:{x,y -> x*y}
                    ],
                    [
                            name: "/",
                            weight: 2,
                            type:"calculation",
                            preEvaluate:true,
                            executor:{x,y -> x/y}
                    ],
                    [
                            name: "||",
                            weight: -1,
                            type:"logical",
                            preEvaluate:true,
                            executor:{x,y -> x||y}
                    ],
                    [
                            name: "&&",
                            weight: -1,
                            type:"logical",
                            preEvaluate:true,
                            executor:{x,y -> x&&y}
                    ],
                    [
                            name: "==",
                            weight: 0,
                            type:"comparision",
                            preEvaluate:true,
                            executor:{x,y -> x==y}
                    ],
                    [
                            name: "!=",
                            weight: 0,
                            type:"comparision",
                            preEvaluate:true,
                            executor:{x,y -> x!=y}
                    ],
                    [
                            name: ">",
                            weight: 0,
                            type:"comparision",
                            preEvaluate:true,
                            executor:{x,y -> x>y}
                    ],
                    [
                            name: "<",
                            weight: 0,
                            type:"comparision",
                            preEvaluate:true,
                            executor:{x,y -> x<y}
                    ],
                    [
                            name: ">=",
                            weight: 0,
                            type:"comparision",
                            preEvaluate:true,
                            executor:{x,y -> x>=y}
                    ],
                    [
                            name: "<=",
                            weight: 0,
                            type:"comparision",
                            preEvaluate:true,
                            executor:{x,y -> x<=y}
                    ],
                    [
                            name: "!",
                            weight: 1000,
                            type:"left-logical",
                            preEvaluate:true,
                            executor:{x -> !x}],
                    [
                            name: "IF",
                            weight: 1000,
                            type:"functional",
                            preEvaluate:true,
                            numberOfParameters:3,
                            executor:{args ->
                                IF(args)
                            }
                    ],
                    [
                            name: "IS_NULL",
                            weight: 1000,
                            type:"functional",
                            preEvaluate:true,
                            numberOfParameters:1,
                            executor:{args ->
                                IS_NULL(args)
                            }
                    ],
                    [
                            name: "SIZE",
                            weight: 1000,
                            type:"functional",
                            preEvaluate:true,
                            numberOfParameters:1,
                            executor:{args ->
                                SIZE(args)
                            }
                    ],
                    [
                            name: "AS_STRING",
                            weight: 1000,
                            type:"functional",
                            preEvaluate:false,
                            numberOfParameters:1,
                            executor:{args ->
                                AS_STRING(args)
                            }
                    ],
                    [
                            name: "AND",
                            weight: -1,
                            type:"logical",
                            executor:{x,y ->
                                return x && y
                            }
                    ],
                    [
                            name: "OR",
                            weight: -1,
                            type:"logical",
                            executor:{x,y ->
                                return x || y
                            }
                    ]
            ]
    ]
}