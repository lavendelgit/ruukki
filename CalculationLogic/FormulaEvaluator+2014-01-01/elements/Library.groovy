def isExpressionResult(result) {
    return (result != null && result instanceof Map && result?.type == "expressionResult")
}

def toExpressionResult(expr, value) {
    return [type: "expressionResult", value: value, expression: expr]
}


def isString(expr) {
    if (expr == null || !(expr instanceof String)) return false
    def trim = expr?.trim()
    return (trim?.startsWith('"') && trim?.endsWith('"')) ||
            (trim?.startsWith("'") && trim?.endsWith("'"))
}

def isExpression(list) {
    if (list == null || list.isEmpty()) return false
    for (it in list) {
        if ((it instanceof Map) && (it?.type == "parentheses" || it?.leftOperator != null || it?.rightOperator != null)) {
            return true
        }
    }
    return false
}