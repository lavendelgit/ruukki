
def eval(expr, binding) {
    if (expr == null || binding == null) return null
    def list = libs.FormulaEvaluator.FormulaParser.parseExpression(expr)
    return evalListExpression(list, binding)?.value
}

def evalListExpression(List list, binding) {
    if (list?.size() == 1) {
        def lastStand = list?.first()
        if (libs.FormulaEvaluator.Library.isExpressionResult(lastStand?.expr)) return lastStand?.expr
        if (lastStand.leftOperator != null) {
            lastStand = [leftOperator : null,
                         expr         : evalOperatorExpression(libs.FormulaEvaluator.Library.toExpressionResult(null,null), lastStand.leftOperator, lastStand.expr, binding),
                         rightOperator: lastStand?.rightOperator]
        }
        return evalBindingExpression(lastStand?.expr, binding)
    }
    def result = []
    def previous
    def newExpr = [:]
    def leftOperatorWeight, rightOperatorWeight
    def idx = 0
    def processing
    list?.each {
        idx ++
        if (it.leftOperator != null) {
            processing = [leftOperator : null,
                  expr         : evalOperatorExpression(libs.FormulaEvaluator.Library.toExpressionResult(null,null), it.leftOperator, it.expr, binding),
                  rightOperator: it.rightOperator]
        }else{
            processing = it
        }
        if (previous != null) {
            leftOperatorWeight = libs.FormulaEvaluator.FormulaSupportedOperator.getOperatorWeight(previous?.rightOperator)
            rightOperatorWeight = libs.FormulaEvaluator.FormulaSupportedOperator.getOperatorWeight(processing?.rightOperator)
            if (rightOperatorWeight <= leftOperatorWeight) {
                newExpr = [leftOperator : previous.leftOperator,
                           expr         : evalOperatorExpression(previous.expr, previous.rightOperator, processing.expr, binding),
                           rightOperator: processing.rightOperator]

                previous = newExpr
                if (idx == list?.size()) {
                    result.add(newExpr)
                }
            } else {
                result.add(previous)
                previous = processing
            }
        } else {
            previous = processing
        }
    }

    return evalListExpression(result, binding)

}


def evalBindingExpression(variable, binding) {
    if(variable == null) return libs.FormulaEvaluator.Library.toExpressionResult(variable,variable)
    if (libs.FormulaEvaluator.Library.isExpressionResult(variable)) return variable
    def list = libs.FormulaEvaluator.FormulaParser.parseExpression(variable)
    if (libs.FormulaEvaluator.Library.isExpression(list)) return evalListExpression(list, binding)

    if (binding == null) {
        api.throwException("Invalid Binding ${binding}")
    }
    def result, var
    def isStringExpr = libs.FormulaEvaluator.Library.isString(variable)
    if (isStringExpr) {
        variable = variable?.trim()
        var = variable?.substring(1, variable.size() - 1)
    }else {
        var = variable
    }

    if (binding?.keySet()?.contains(var)) {
        result = binding?.getAt(var)
    } else if (isStringExpr) {
        result = var
    }  else {
        try {
            if(var == "null"){
                result = null
            }else if(var == "true"){
                result = true
            }else if(var == "false"){
                result = false
            }else if (var?.contains(".")) {
                result = (var) as BigDecimal
            } else {
                result = Integer.parseInt(var)?.intValue()
            }

        } catch (e) {
            api.throwException("Binding for variable ${var} not found ${e}")
        }
    }

    return libs.FormulaEvaluator.Library.toExpressionResult(variable, result)

}



def evalOperatorExpression(x, operator, y, binding) {
    def operatorConfig = libs.FormulaEvaluator.FormulaSupportedOperator.getOperatorConfig(operator)

    if(operatorConfig == null ) api.throwException("UnSupported Operator ${operator}")

    if(operatorConfig.preEvaluate != false){
        if(operatorConfig.type != "functional" && !libs.FormulaEvaluator.Library.isExpressionResult(y)){
            y = evalBindingExpression(y, binding)
        }

        if (!libs.FormulaEvaluator.Library.isExpressionResult(x)) {
            x = evalBindingExpression(x, binding)
        }
    }

    def result
    def y_result
    switch (operatorConfig.type){
        case "calculation":
            result = operatorConfig.executor(x?.value,y?.value)
            break
        case "logical":
            result = operatorConfig.executor(x?.value,y?.value)
            break
        case "left-logical":
            result = operatorConfig.executor(y?.value)
            break
        case "comparision":
            result = operatorConfig.executor(x?.value,y?.value)
            break
        case "functional":
            y_result = getFunctionalExpressionArgument(y,binding,operatorConfig.preEvaluate,operatorConfig.numberOfParameters)
            result = operatorConfig.executor(y_result?.value)
            break
        default:
            api.throwException("UnSupported Operator Type: ${operatorConfig.type}")
    }

    return libs.FormulaEvaluator.Library.toExpressionResult("${x?.expression?'(' + x?.expression + ')':""} ${operator} (${operatorConfig.preEvaluate?y_result?.getAt('expression'):'('+y+')'})", result)
}

def getFunctionalExpressionArgument(expr,binding,preEvaluate = true, numberOfParameters = null) {
    if(!api.global.FORMULA_EVALUATOR_CACHED_FUNC_EXPR){
        api.global.FORMULA_EVALUATOR_CACHED_FUNC_EXPR = [:]
    }
    def globalCached = api.global.FORMULA_EVALUATOR_CACHED_FUNC_EXPR
    def cached = globalCached?.getAt(expr)

    def result = []
    if(cached){
        api.logInfo("Functional--Using Cached")
        return preEvaluateFunctionalExpressionAttempt(expr,cached,binding,preEvaluate,numberOfParameters)
    }
    def separateOperators = [","]
    def expression
    def separate
    def originExpr = expr
    def item = """ """
    while (expr != null) {

        separate = libs.FormulaEvaluator.FormulaParser.getOperatorExpression(expr, separateOperators)
        if (separate?.result != null) {
            result.add(item)
            item = """ """
        }
        separate = libs.FormulaEvaluator.FormulaParser.getLeftHandOperatorExpression(separate?.remain)
        item += separate?.result ?: ""
        expression = libs.FormulaEvaluator.FormulaParser.getGroupExpression(separate?.remain, true)
        item += expression?.result ?: ""
        separate = libs.FormulaEvaluator.FormulaParser.getRightHandOperatorExpression(expression?.remain)
        item += separate?.result ?: ""
        expr = separate?.remain
    }

    result.add(item)
    globalCached.putAt(originExpr,result)
    return preEvaluateFunctionalExpressionAttempt(originExpr,result,binding,preEvaluate,numberOfParameters)
}

def preEvaluateFunctionalExpressionAttempt(expr,parsedResult,binding,preEvaluate = true, numberOfParameters = null){
    if(!preEvaluate) return parsedResult
    def result = []
    parsedResult?.each{
        result.add(evalBindingExpression(it,binding).value)
    }
    functionalExpressionParameterRequired(result,numberOfParameters)
    return libs.FormulaEvaluator.Library.toExpressionResult(expr,result)
}

def functionalExpressionParameterRequired(params,numberOfParameterRequired){
    if(numberOfParameterRequired == null || numberOfParameterRequired < 0 ||
            (params != null && params.size() >= numberOfParameterRequired)){
        return
    }
    api.throwException("Parameter reqired ${numberOfParameterRequired}, but got ${!params?:params.size()}")
}