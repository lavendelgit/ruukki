

def parseExpression(expr, parentheseWrap = false) {

    if(!api.global.FORMULA_EVALUATOR_CACHED_EXPR){
        api.global.FORMULA_EVALUATOR_CACHED_EXPR = [:]
    }
    def globalCached = api.global.FORMULA_EVALUATOR_CACHED_EXPR
    def cached = globalCached?.getAt(expr)
    if(cached){
        return cached
    }

    def expression
    def originalExpr = expr
    def item
    def list = []
    def operator
    while (expr != null) {
        operator = getLeftHandOperatorExpression(expr)
        item = [leftOperator: operator?.result]
        expression = getGroupExpression(operator?.remain, parentheseWrap)
        item.expr = expression.result
        item.type = expression.type
        operator = getRightHandOperatorExpression(expression.remain)
        item.rightOperator = operator.result
        expr = operator.remain
        if ((expr != null && item.rightOperator == null) ||
                item.expr == null && item.rightOperator != null) {
            api.throwException("Invalid ${originalExpr} Expression")
        }
        list.add(item)
    }
    if (list?.isEmpty() || list?.first()?.expr == null || list?.last()?.rightOperator != null) {
        api.throwException("Invalid ${originalExpr} Expression")
    }
    globalCached.putAt(originalExpr,list)
    return list
}


def getGroupExpression(String expr, parenthesesWrap = false) {
    if (expr == null || expr?.trim()?.isEmpty()) return [result: null, remain: null]
    def trim = expr?.trim()
    def expression = getParenthesesExpression(trim)

    if (expression?.result == null) {
        expression = getString(expression?.remain)
    }
    if (expression?.result == null) {
        expression = getVariableExpression(expression?.remain)
    }
    if (parenthesesWrap == true) {
        expression = [result: "(" + expression.result + ")", remain: expression.remain]
    }
    return expression
}

def getVariableExpression(String expr) {
    if (expr == null || expr?.trim()?.isEmpty()) return [result: null, remain: null]
    def variableSeparator = ["(", '"', "'", " ",","]
    def operator
    def trim = expr.trim()
    def str = """ """
    def endIdx = 0
    def currentIdx = -1
    for (it in trim) {
        currentIdx += 1
        operator = getAllOperatorExpression(trim?.substring(currentIdx))
        if ((operator.result == null) && (!variableSeparator?.contains(it))) {
            str += it
            endIdx += 1

        } else {
            break
        }
    }
    return [result: str?.trim()?.isEmpty() ? null : str?.trim(),
            remain: trim?.size() > endIdx ? trim?.substring(endIdx) : null,
            type  : str?.trim()?.isEmpty() ? null : "variable"
    ]

}

def getString(String expr) {
    if (expr == null || expr?.trim()?.isEmpty()) return [result: null, remain: null]
    def trim = expr.trim()
    if (trim?.startsWith('"') || trim?.startsWith("'")) {
        def str = """ """
        def start = trim?.getAt(0)
        def endIdx = 0
        def count = 0
        for (it in trim) {
            if (it == start) count += 1
            if (count <= 2) {
                str += it
                endIdx += 1
            }
            if (count == 2) break
        }

        if (count < 2) api.throwException("Invalid String Expression ${expr}")

        return [result: libs.FormulaEvaluator.Library.isString(str) ? str?.trim() : null,
                remain: trim?.size() > endIdx ? trim?.substring(endIdx) : null,
                type  : "string"
        ]
    }
    return [result: null, remain: expr]

}

def getParenthesesExpression(String expr) {
    if (expr == null || expr?.trim()?.isEmpty()) return [result: null, remain: null]
    def trim = expr.trim()
    if (trim?.startsWith("(")) {
        def str = """ """
        def endIdx = 0
        def count = 0
        for (it in trim) {
            if (it == "(") count += 1
            if (it == ")") count -= 1
            if (count >= 0) {
                str += it
                endIdx += 1
            }
            if (count == 0) break
        }
        if (count > 0) {
            api.throwException("Invalid Expression ${expr}")
        }
        str = str?.trim()
        str = str?.substring(1, str.size() - 1)
        return [result: str,
                remain: trim?.size() > endIdx ? trim?.substring(endIdx) : null,
                type  : "parentheses"
        ]
    }
    return [result: null, remain: expr]
}

def getLeftHandOperatorExpression(String expr) {
    def leftHandOperators = libs.FormulaEvaluator.FormulaSupportedOperator.getLeftLogical()
    def operator = getOperatorExpression(expr, leftHandOperators)
    if (operator?.result != null) return operator
    def functionalOperator = getFunctionalOperatorExpression(expr)
    return functionalOperator
}

def getFunctionalOperatorExpression(String expr) {
    def functionalOperators = libs.FormulaEvaluator.FormulaSupportedOperator.getFunctionalOperator()
    def operator = getOperatorExpression(expr, functionalOperators)
    if (operator?.result != null) {
        def remain = operator?.remain
        if (remain == null || remain?.trim()?.isEmpty()) {
            api.throwException("Invalid Funtional Operator:  ${expr}")
        }
        if(remain?.trim()?.getAt(0) != "("){
            return [result:null,remain:expr]
        }
    }
    return operator
}

def getRightHandOperatorExpression(String expr) {
    def operators = libs.FormulaEvaluator.FormulaSupportedOperator.getRightHandOperator()
    return getOperatorExpression(expr, operators)
}

def getAllOperatorExpression(String expr) {
    def result = getRightHandOperatorExpression(expr)
    if(result.result == null){
        result = getLeftHandOperatorExpression(expr)
    }
    return result
}

def getOperatorExpression(String expr, List operators) {
    if (expr == null || expr?.trim()?.isEmpty()) return [result: null, remain: null]
    def trim = expr.trim()
    def operator
    def result = [result: null, remain: trim]
    def remain
    for (it in operators) {
        operator = trim?.size() >= it?.size() ? trim?.substring(0, it.size()) : null
        if (operator == it) {
            remain = trim?.size() > it.size() ? trim?.substring(it.size()) : null
            result = [result: operator,
                      remain: remain,
                      type  : "operator"]
            break
        }
    }
    return result

}