def setFormula(formula){
    def _formula = formula
    def _binding = [:]
    def _builder
    _builder = [setBinding:{binding->
        _binding = binding
        return _builder
    },eval:{
        return libs.FormulaEvaluator.Evaluator.eval(_formula,_binding)
    },getParsedVariables:{
        def first = libs.FormulaEvaluator.FormulaParser.parseExpression(_formula)
        def ret = []
        def currentParsed
        def getAll
        getAll = {list ->
            def r = []
            for(i in list){
                /*Functional Operator Parsing*/
                if(i instanceof  Map && i.leftOperator!= null &&
                        libs.FormulaEvaluator
                            .FormulaSupportedOperator
                                .getFunctionalOperator()?.contains(i.leftOperator)){
                    def func = libs.FormulaEvaluator
                                    .Evaluator
                                    .getFunctionalExpressionArgument(i.expr,null,false,true)
                    r.addAll(getAll(func))
                    continue

                }

                if(i instanceof String){
                    def parsed = libs.FormulaEvaluator.FormulaParser.parseExpression(i)
                    r.addAll(getAll(parsed))
                    continue
                }

                /*End Functional Operator Parsing*/

                currentParsed = libs.FormulaEvaluator.FormulaParser.parseExpression(i?.expr)
                if(libs.FormulaEvaluator.Library.isExpression(currentParsed)){
                    r.addAll(getAll(currentParsed))
                   }
                else{
                     r.add(i)
                }
            }
            return  r
        }
        ret.addAll(getAll(first))
        return ret
    }]
}