def getBaseMarkup(marketFilter, productCodeFilter, baseproductFilter, productCategoryFilter, priceDateInput) {
    def result = getBaseMarkupList(marketFilter, productCodeFilter, baseproductFilter, productCategoryFilter, priceDateInput)
    return result?.sort{a,b -> b.key3 <=> a.key3}?.getAt(0).attribute1

}

def getBaseMarkupList(marketFilter, productCodeFilter, baseproductFilter, productCategoryFilter, priceDateInput) {

    def dateFilter
    if (priceDateInput) {
        dateFilter = Filter.lessOrEqual("key3", priceDateInput)
    }

    def basemarkupsdata = api.findLookupTableValues("Base_Sales_Markups", "-key3", productCodeFilter, dateFilter, marketFilter)
    if (!basemarkupsdata) {
        basemarkupsdata = api.findLookupTableValues("Base_Sales_Markups", "-key3", baseproductFilter, dateFilter, marketFilter)
    }
    if (!basemarkupsdata) {
        basemarkupsdata = api.findLookupTableValues("Base_Sales_Markups", "-key3", productCategoryFilter, dateFilter, marketFilter)
    }

    return basemarkupsdata
}