List getCOGS(String sku, String market, Date priceDateInput) {
    def result = getCOGSlist(sku,market, priceDateInput)
     return result ?.sort { a, b -> b.attribute11 <=> a.aatribute11}?.getAt(0)?.attribute10
}


List getCOGSList(String sku, String market, Date priceDateInput) {
    def filters = [
            Filter.equal("name", "ComponentsProductPrices"),
            Filter.or(
                    Filter.equal("attribute1", sku),
                    Filter.equal("sku", sku)),
            Filter.equal("attribute3", market)
    ]
    if (priceDateInput) {
        filters << Filter.lessorEqual("attribute11", priceDateInput)
    }
    def price_row = api.find("PX", 0, "-attribute11", *filters)
    return price_row
}