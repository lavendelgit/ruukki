import java.math.RoundingMode

Map getOptionsPrice(String productCode, BigDecimal length, BigDecimal height, String materialKey, String thicknessKey, String coatingKey, String colorKey, String priceDateInput, BigDecimal quantity, String rawMaterialTableName) {
    List result = getOptionsPriceList(productCode, length, height, materialKey, thicknessKey, coatingKey, colorKey, priceDateInput, quantity, rawMaterialTableName)
    if (!result) {
        api.local.warnings?.add("Rawmaterial price not found for ${materialKey},${thicknessKey},${coatingKey} and ${colorKey}")
        return ["cogsPrice"       : 0,
                "rawMaterialPrice": 0,
                "processingCost"  : 0,
                "inputArea"       : ((length * height) / 1000000)]
    }

    return result?.sort { a, b -> b.rawMaterialValidFrom <=> a.rawMaterialValidFrom ?: b.claddingCostValidFrom <=> a.claddingCostValidFrom }.getAt(0)
}

def getOptionsPriceList(String productCode, BigDecimal length, BigDecimal height, String materialKey, String thicknessKey,
                        String coatingKey, String colorKey, priceDateInput, BigDecimal quantity, String rawMaterialTableName) {


    def claddingdateFilter, rawMaterialDateFilter
    if (priceDateInput) {
        claddingdateFilter = Filter.lessOrEqual("attribute7", priceDateInput)
        rawMaterialDateFilter = Filter.lessOrEqual("attribute6", priceDateInput)
    }
    rawMaterialTableName = rawMaterialTableName?:"RawMaterialPrice"

    if (length == 0 || height == 0) {
        api.local.warnings?.add("Length/height should not be zero")
        return ["cogsPrice"       : 0,
                "rawMaterialPrice": 0,
                "processingCost"  : 0,
                "inputArea"       : 0]
    }
    def filters = [Filter.equal("name", rawMaterialTableName),
                   Filter.in("sku", [materialKey, "*"]),
                   Filter.in("attribute1", [thicknessKey, "*"]),
                   Filter.in("attribute3", [coatingKey, "*"])]
    if (rawMaterialDateFilter) {
        filters << rawMaterialDateFilter
    }
    if (colorKey) {
        filters << Filter.in("attribute4", [colorKey, "*"])
    }
    def rawMaterialData = api.find("PX", 0, "-attribute6", *filters)
    if (!rawMaterialData) {
        return
    }

    List stdDimensions = api.findLookupTableValues("StandardDimensions")
    List stdDimensionsLength = stdDimensions?.collect { it.key1 }


    def inputArea = ((length * height) / 1000000)


    List claddingFilters = [Filter.equal("name", "CladdingProcessingCost"), Filter.equal("sku", productCode)]

    if (claddingdateFilter) {
        claddingFilters << claddingdateFilter
    }

    def claddingData = api.find("PX", 0, "-attribute7", *claddingFilters)
    List result = []
//     Adding additional dimensions based on product
    claddingData.each { claddingCost ->
        def Aextra = claddingCost?.attribute2 as BigDecimal
        def Bextra = claddingCost?.attribute3 as BigDecimal

        def Adim = (length + Aextra as BigDecimal) ?: 0
        def Bdim = (height + Bextra as BigDecimal) ?: 0
        def coilWidth = claddingCost?.attribute8 as BigDecimal

//        Calculating length remainings

        def lengthCalculations = stdDimensionsLength?.collect { it ->
            def onSheet = [(coilWidth / Adim)?.setScale(0, BigDecimal.ROUND_DOWN) * (it / Bdim).setScale(0, BigDecimal.ROUND_DOWN),
                           (it / Adim)?.setScale(0, BigDecimal.ROUND_DOWN) * (coilWidth / Bdim).setScale(0, BigDecimal.ROUND_DOWN)].max()

            def rawSheets = onSheet ? (quantity / onSheet).setScale(0, BigDecimal.ROUND_UP) : 0.0


            def area = rawSheets * it * coilWidth / 1000000
            api.trace("area", area)
            ["StdDimension": it,
             "OnSheet"     : onSheet,
             "RawSheets"   : rawSheets ?: 0.0,
             "Area"        : area]
        }
        
        def minAreaData = lengthCalculations?.findAll { it.Area != 0.0 }
        def minArea = minAreaData?.min { it.Area }?.Area ?: 0.0
        def selectedLength = minAreaData?.min { it.Area }?.StdDimension ?: 0.0

        def standardLeftovers = api.find("PX", Filter.equal("name", "StandardLeftovers"))?.collect { it.sku }
                .collect { it.split("/")[0] as BigDecimal }.unique()

        standardLeftovers.add(0, 0)

        def leftoverCalculations = standardLeftovers?.collect { it ->
            def adjusted = coilWidth - it
            def onSheet = [(adjusted / Bdim)?.setScale(0, BigDecimal.ROUND_DOWN) * (selectedLength / Adim)?.setScale(0, BigDecimal.ROUND_DOWN),
                           (adjusted / Adim)?.setScale(0, BigDecimal.ROUND_DOWN) * (selectedLength / Bdim)?.setScale(0, BigDecimal.ROUND_DOWN)].max()

            def rawSheets = onSheet ? (quantity / onSheet).setScale(0, BigDecimal.ROUND_UP) : 0.0

            def area = rawSheets * adjusted * selectedLength / 1000000

            ["stdLeftover": it,
             "Adjusted"   : adjusted,
             "OnSheet"    : onSheet,
             "RawSheets"  : rawSheets,
             "Area"       : area]
        }

        def minLeftOverData = leftoverCalculations?.findAll { it.Area != 0.0 }

        def minLeftOverArea = minLeftOverData?.min { it.Area }?.Area ?: 0.0
        def selectedWidth = minLeftOverData?.min { it.Area }?.Adjusted ?: 0.0
        def usableLeftOver = minLeftOverData?.min { it.Area }?.stdLeftover ?: 0.0

        def halfLeftoverwidth = selectedWidth + usableLeftOver / 2
        def neededRawMaterialqty = leftoverCalculations?.find { it.Adjusted == selectedWidth }?.RawSheets ?: 0.0
        def neededRawMaterialArea = halfLeftoverwidth * selectedLength * neededRawMaterialqty / 1000000

        def processingCostVar = claddingCost?.attribute4
        def processingCostFix = claddingCost?.attribute5
        def averageSize = claddingCost?.attribute6
        def processingCost = averageSize / inputArea * processingCostVar + processingCostFix

        rawMaterialData.each { rawMaterial ->
            def rawMaterialPrice = rawMaterial?.attribute5 ?: 0
            def rawMaterialFullcost = rawMaterialPrice * neededRawMaterialArea
            def productArea = length * height * quantity / 1000000
            def rawMaterialTotal = productArea ? rawMaterialFullcost / productArea : 0.0
            def cogsPrice = rawMaterialTotal + processingCost

            result << ["cogsPrice"            : cogsPrice,
                       "rawMaterialPrice"     : rawMaterialTotal,
                       "rawMaterialValidFrom" : rawMaterial?.attribute6,
                       "claddingCostValidFrom": claddingCost?.attribute7,
                       "processingCost"       : processingCost,
                       "inputArea"            : inputArea]
        }

    }
    return result
}



