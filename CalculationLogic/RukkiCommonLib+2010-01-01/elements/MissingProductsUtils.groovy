def removeBaseProductsFromProductMarketMaster(streamedData, variantProductGroups) {

    LinkedHashMap prodMarketMaster = [:]
    LinkedHashMap baseProductMaster = [:]

    if (streamedData) {
        streamedData?.withCloseable {
            it.each { product ->
                ArrayList markets = product?.attribute5?.replaceAll(']', "")?.substring(1)?.tokenize(", ")
                if (product.attribute1 && variantProductGroups?.contains(product.attribute4)) {
                    if (!baseProductMaster[(product.attribute4)]) {
                        baseProductMaster[(product.attribute4)] = [product.attribute1]
                    } else {
                        if (!(baseProductMaster[(product.attribute4)].contains(product.attribute1))) {
                            baseProductMaster[(product.attribute4)] << product.attribute1
                        }
                    }
                }
                prodMarketMaster[(product.attribute4)] = (prodMarketMaster[product.attribute4] ?: [:]) + [(product.sku): markets]
            }
        }
    }
    ArrayList productGroup = prodMarketMaster?.keySet()
    productGroup?.each { proGroup ->
        baseIds = baseProductMaster.get(proGroup)
        baseIds?.each { baseProduct -> prodMarketMaster[proGroup].remove(baseProduct)
        }
    }
    return prodMarketMaster
}
