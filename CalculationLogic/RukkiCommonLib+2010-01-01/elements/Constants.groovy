@Field Map SYMBOLS = [
        DECREASE: "↓",
        INCREASE: "↑",
        FLAT    : ""
]

@Field Date EOT = new Date().parse("yyyy-MM-dd", "2099-12-31")

@Field Map SEVERITY = [
        CRITICAL: [importance: 3, color: 'red', alert: { String msg ->
            return api.criticalAlert(msg)
        }],
        ERROR   : [importance: 2, color: 'orange', alert: { String msg ->
            return api.redAlert(msg)
        }],
        WARN    : [importance: 1, color: 'yellow', alert: { String msg ->
            return api.yellowAlert(msg)
        }]
]

@Field Map VALIDATION_COLUMNS = [
        SEVERITY      : 'attribute29',
        VALIDATION_MSG: 'attribute30'
]

@Field Map PRICERECORDS_MATRIX_COLUMNS = [
        SKU              : 'Product ID',
        MARKET           : 'Market',
        BASE_PRODUCT_CODE: 'Base Product Code',
        BASE_PRICE       : 'Normal Base Price',
        FLOOR_PRICE      : 'Normal Floor Price',
        TARGET_PRICE     : 'Normal Target Price',
        START_PRICE      : 'Normal Start Price',
        VALID_FROM       : 'Valid From',
        VALID_UNTIL      : 'Valid Until'
]