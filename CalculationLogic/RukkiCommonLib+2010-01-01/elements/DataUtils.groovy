List findAll(String type, String sortBy, List fields, List filters) {
    int start = 0
    int max = api.getMaxFindResultsLimit()
    List result = []
    List page
    while (page = find(type, start, max, sortBy, fields, filters)) {
        result.addAll(page)
        start = +page.size()
    }
    return result
}

private List find(String type, int start, int max, String sortBy, List fields, List filters) {
    if (fields) {
        return api.find(type, start, max, sortBy, fields, *filters)
    }
    return api.find(type, start, max, sortBy, *filters)
}

