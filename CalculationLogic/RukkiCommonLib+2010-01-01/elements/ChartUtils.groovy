Map getTimeSeriesChartDef(String title, String subTitle, List seriesData, String currency) {
    return [
            chart      : [
                    type    : 'spline',
                    zoomType: 'xy'
            ],
            title      : [
                    text: title
            ],
            subtitle   : [
                    text: subTitle
            ],
            credits    : [
                    enabled: false
            ],
            xAxis      : [
                    type                : 'datetime',
                    dateTimeLabelFormats: [
                            millisecond: '%Y-%b-%e %H:%M:%S.%L',
                            second     : '%Y-%b-%e %H:%M:%S',
                            minute     : '%Y-%b-%e %H:%M',
                            hour       : '%Y-%b-%e %H:%M',
                            day        : '%Y %b %e',
                            week       : '%b %e',
                            month      : '%b %Y',
                            year       : '%Y'
                    ],
                    title               : [
                            text: 'Date'
                    ],
                    labels              : [
                            rotation: 320
                    ]
            ],
            yAxis      : [
                    title: [
                            text: "Price ( in ${currency} )"
                    ],
                    min  : 0
            ],
            tooltip    : [
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat : "{point.x:%e %b, %Y}: ${currency}{point.y:.2f}"
            ],

            plotOptions: [
                    series: [
                            marker: [
                                    enabled: true
                            ]
                    ]
            ],
            colors     : ['#2F7ED8', '#C7B805', '#8BBC21', '#BA0900'],
            series     : seriesData
    ]
}