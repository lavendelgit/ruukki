import net.pricefx.server.dto.calculation.ResultMatrix

Map newPriceGridRow() {
    Map methods = [:]
    Map definition = [deltaAsPercent: false, forCell: false, currency: '€']


    methods += [
            forProduct                : { Object product ->
                definition << [
                        sku            : product.sku,
                        allMarkets     : product.attribute5,
                        productType    : product.attribute4,
                        productCategory: product.attribute6,
                        baseProduct    : product.attribute1
                ]
                return methods
            },
            withMarket                : { String market ->
                definition.market = market
                return methods
            },
            withCurrency              : { String currency ->
                definition.currency = currency
                return methods
            },
            withNewBasePrice          : { BigDecimal newBasePrice ->
                definition.newBasePrice = newBasePrice
                return methods
            },
            withNewFloorPrice         : { BigDecimal newFloorPrice ->
                definition.newFloorPrice = newFloorPrice
                return methods
            },
            withNewTargetPrice        : { BigDecimal newTargetPrice ->
                definition.newTargetPrice = newTargetPrice
                return methods
            },
            withNewStartPrice         : { BigDecimal newStartPrice ->
                definition.newStartPrice = newStartPrice
                return methods
            },
            withNewValidFrom          : { Date validFrom ->
                definition.newValidFrom = validFrom
                return methods
            },
            withNewValidUntil         : { Date validUntil ->
                definition.newValidUntil = validUntil
                return methods
            },
            getSKU                    : {
                return definition.sku
            },
            getMarket                 : {
                return definition.market
            },
            getProductType            : {
                return definition.productType
            },
            getProductCategory        : {
                return definition.productCategory
            },
            getBaseProduct            : {
                return definition.baseProduct
            },
            getActiveBasePrice        : {
                return roundOffPrice(definition.activeBasePrice)
            },
            getNewBasePrice           : {
                return roundOffPrice(definition.newBasePrice)
            },
            getBasePriceDelta         : {
                return methods.getPriceDelta(definition.newBasePrice, definition.activeBasePrice)
            },
            getActiveFloorPrice       : {
                return roundOffPrice(definition.activeFloorPrice)
            },
            getNewFloorPrice          : {
                return roundOffPrice(definition.newFloorPrice)
            },
            getFloorPriceDelta        : {
                return methods.getPriceDelta(definition.newFloorPrice, definition.activeFloorPrice)
            },
            getActiveTargetPrice      : {
                return roundOffPrice(definition.activeTargetPrice)
            },
            getNewTargetPrice         : {
                return roundOffPrice(definition.newTargetPrice)
            },
            getTargetPriceDelta       : {
                return methods.getPriceDelta(definition.newTargetPrice, definition.activeTargetPrice)
            },
            getActiveStartPrice       : {
                return roundOffPrice(definition.activeStartPrice)
            },
            getNewStartPrice          : {
                return roundOffPrice(definition.newStartPrice)
            },
            getStartPriceDelta        : {
                return methods.getPriceDelta(definition.newStartPrice, definition.activeStartPrice)
            },
            getNewValidFrom           : {
                return definition.newValidFrom
            },
            getNewValidUntil          : {
                return definition.newValidUntil
            },
            getCurrentPrice           : {
                def commonUtils = libs.RukkiCommonLib.CommonUtils
                return commonUtils.nvl(definition.newBasePrice) +
                        commonUtils.nvl(definition.newFloorPrice) +
                        commonUtils.nvl(definition.newTargetPrice) +
                        commonUtils.nvl(definition.newStartPrice)
            },
            forCell                   : {
                definition.forCell = true
                return methods
            },
            asPercent                 : {
                definition.deltaAsPercent = true
                return methods
            },
            decorateIfMissing         : {
                definition.decorateMissing = true
                return methods
            },
            getPriceDelta             : { BigDecimal newValue, BigDecimal activeValue ->
                if (newValue == null || activeValue == null) {
                    return 0.0
                }
                BigDecimal delta = newValue - activeValue
                String prefix = null
                if (definition.deltaAsPercent) {
                    definition.deltaAsPercent = false
                    delta = activeValue ? delta / activeValue : 0.0
                }
                if (definition.forCell) {
                    definition.forCell = false
                    return libs.RukkiCommonLib.CommonUtils.getDeltaCell(delta, prefix)
                }

                return delta
            },
            loadPrices                : {
                definition << getPrices(definition.sku, definition.market, api.global.TODAY)
                return methods
            },
            init                      : {
                if (!api.global.TODAY) {
                    api.global.TODAY = new Date()
                }
                methods.loadPrices()

                api.trace("definition-1", definition)

                def utilLibs = libs.RukkiCommonLib.CommonUtils
                definition.newBasePrice = utilLibs.nvl(definition.newBasePrice, definition.activeBasePrice)
                definition.newFloorPrice = utilLibs.nvl(definition.newFloorPrice, definition.activeFloorPrice)
                definition.newTargetPrice = utilLibs.nvl(definition.newTargetPrice, definition.activeTargetPrice)
                definition.newStartPrice = utilLibs.nvl(definition.newStartPrice, definition.activeStartPrice)

                api.trace("definition-2", definition)

                return methods
            },
            getPriceChangeHistoryChart: {
                return getPriceChangeHistory(definition.sku, definition.market, definition.currency)
            },
            getValidFrom              : {
                return api.global.TODAY
            },
            getValidUntil             : {
                return libs.RukkiCommonLib.Constants.EOT
            },
            getActiveValidFrom        : {
                return definition.activeValidFrom
            },
            getActiveValidUntil       : {
                return definition.activeValidUntil
            },
            setSeverity               : { Map severity ->
                definition.severity = severity
                return methods
            },
            getSeverity               : { Map severity ->
                return definition.severity
            },
            getValidationMatrix       : {
                return buildValidationMatrix(methods)
            },
            getPriceRecordsMatrix     : {
                return buildPriceRecordsMatrix(methods)
            },
            getAttribute              : { String attributeName ->
                return definition[attributeName]
            },
            getA1TargetPrice          : {
                return definition.a1TargetPrice
            },
            getA8TargetPrice          : {
                return definition.a8TargetPrice
            },
            getCogsPrice              : {
                return definition.cogsPrice
            },
            getProductCode            : {
                return definition.productCode
            }
    ]
    return methods
}

Map getPrices(String sku, String market, Date pricingDate) {
    if (!pricingDate) {
        api.throwException("Invalid Pricing Date.")
    }
    return libs.SharedLib.CacheUtils.getOrSet("COMPONENT_PRODUCT_PRICES", {
        def utilLibs = libs.RukkiCommonLib.CommonUtils
        String pricingDateStr = pricingDate.format("yyyy-MM-dd")
        List filters = [
                Filter.equal("name", "ComponentsProductPrices"),
                Filter.lessOrEqual("attribute11", pricingDateStr),
                Filter.or(Filter.isNull("attribute13"), Filter.greaterOrEqual("attribute13", pricingDateStr))
        ]

        return libs.SharedLib.StreamUtils.stream("PX", "lastUpdateDate", null, filters).collectEntries { Map priceRecord ->
            return [(priceRecord.sku + "." + (priceRecord.attribute3 ?: "-")): [
                    activeBasePrice   : priceRecord?.attribute4,
                    activeFloorPrice  : priceRecord?.attribute7,
                    activeTargetPrice : priceRecord?.attribute6,
                    activeStartPrice  : priceRecord?.attribute5,
                    activeValidFrom   : api.parseDate("yyyy-MM-dd", priceRecord.attribute11),
                    activeValidUntil  : priceRecord.attribute13 ? api.parseDate("yyyy-MM-dd", priceRecord.attribute13) : libs.RukkiCommonLib.Constants.EOT,
                    activePriceTypedId: priceRecord.typedId,
                    a1TargetPrice     : priceRecord.attribute8,
                    a8TargetPrice     : priceRecord.attribute9,
                    cogsPrice         : priceRecord.attribute10,
                    productCode       : priceRecord.attribute1
            ]]
        }
    })?.getAt(sku + "." + (market ?: "-")) ?: [:]
}

List getAllPrices(String sku, String market) {
    return libs.SharedLib.CacheUtils.getOrSet("COMPONENT_PRODUCT_All_PRICES", {
        def utilLibs = libs.RukkiCommonLib.CommonUtils
        List filters = [
                Filter.equal("name", "ComponentsProductPrices")
        ]
        return libs.SharedLib.StreamUtils.stream("PX", "attribute11", null, filters).groupBy { java.util.Map priceRecord ->
            (priceRecord.sku + "." + (priceRecord.attribute3 ?: "-"))
        }?.collectEntries { String key, List priceRecords ->
            return [(key): priceRecords.collect { Map priceRecord ->
                [
                        "Base Price"  : utilLibs.nvl(priceRecord?.attribute4),
                        "Floor Price" : utilLibs.nvl(priceRecord?.attribute7),
                        "Target Price": utilLibs.nvl(priceRecord?.attribute6),
                        "Start Price" : utilLibs.nvl(priceRecord?.attribute5),
                        "PriceTypedId": priceRecord.typedId,
                        "Valid From"  : api.parseDate("yyyy-MM-dd", priceRecord.attribute11),
                        "Valid Until" : priceRecord.attribute13 ? api.parseDate("yyyy-MM-dd", priceRecord.attribute13) : libs.RukkiCommonLib.Constants.EOT
                ]
            }]
        }
    })?.getAt(sku + "." + (market ?: "-")) ?: []
}


def getPriceChangeHistory(String sku, String market, String currency) {
    List allPrices = getAllPrices(sku, market)
    if (allPrices) {
        def roundingUtils = libs.SharedLib.RoundingUtils
        List seriesData = ["Base Price", "Floor Price", "Target Price", "Start Price"].collect { String pricePoint ->
            return [
                    name: pricePoint,
                    data: allPrices.collect { Map priceRow ->
                        [priceRow."Valid From"?.getTime(), roundingUtils.round(priceRow[pricePoint], 2)]
                    }
            ]
        }

        return api.buildHighchart(libs.RukkiCommonLib.ChartUtils.getTimeSeriesChartDef("Price Change Over Time", sku, seriesData, currency))
                  .showDataTab()
    }
}

List getValidationRules() {
    return libs.SharedLib.CacheUtils.getOrSet("PRICEPOINT_FORMULA", {
        return api.findLookupTableValues("PricePointValidationRules")?.collect()
    })
}

Map getPricePointValidationFormulaRow(String pricePoint) {
    return libs.SharedLib.CacheUtils.getOrSet("PRICEPOINT_FORMULA", {
        return api.findLookupTableValues("PricePointValidationRules")?.collectEntries { Object ruleRow ->
            [(ruleRow.name): ruleRow]
        }
    })?.getAt(pricePoint)
}

String buildFormula(Map formulaRow) {
    Map COLUMNS = libs.RukkiCommonLib.Constants.VALIDATION_COLUMNS
    return formulaRow.findAll { String fieldName, Object value ->
        value != null && fieldName.startsWith("attribute") && !fieldName.equals(COLUMNS.SEVERITY) && !fieldName.equals(COLUMNS.VALIDATION_MSG)
    }?.values()?.join(" ")
}

Boolean isPricePointPriceValid(String formula, java.util.Map inputs) {
    return libs.FormulaEvaluator.Builder
               .setFormula(formula)
               .setBinding(inputs).eval()
}

ResultMatrix buildValidationMatrix(Map instance) {
    ResultMatrix validationMatrix = api.newMatrix("Validation Messages")
    Map COLUMNS = libs.RukkiCommonLib.Constants.VALIDATION_COLUMNS
    Map SEVERITY = libs.RukkiCommonLib.Constants.SEVERITY
    Map severity = null
    boolean hasValidFromChanged = instance.getValidFrom() != instance.getNewValidFrom()

    if (hasValidFromChanged && instance.getNewValidFrom() < api.global.TODAY) {
        String msg = "Valid From cannot be earlier then current date."
        validationMatrix.addRow(["Validation Messages": validationMatrix.styledCell(msg).withColor('red')])
        severity = SEVERITY.CRITICAL
        severity.msg = msg
    }

    if (instance.getNewValidFrom() > instance.getNewValidUntil()) {
        String msg = "Valid Until cannot be earlier then Valid From."
        validationMatrix.addRow(["Validation Messages": validationMatrix.styledCell(msg).withColor('red')])
        severity = SEVERITY.CRITICAL
        severity.msg = msg
    }

    Map inputs = [
            NewStartPrice     : instance.getNewStartPrice(),
            NewTargetPrice    : instance.getNewTargetPrice(),
            NewBasePrice      : instance.getNewBasePrice(),
            NewFloorPrice     : instance.getNewFloorPrice(),
            CurrentStartPrice : instance.getActiveStartPrice(),
            CurrentTargetPrice: instance.getActiveTargetPrice(),
            CurrentBasePrice  : instance.getActiveBasePrice(),
            CurrentFloorPrice : instance.getActiveFloorPrice()
    ]
    api.trace("inputs", inputs)
    List validationRules = getValidationRules()
    validationRules.each { Object formulaRow ->
        String formula = buildFormula(formulaRow)
        api.trace("formula", formula)
        if (!isPricePointPriceValid(formula, inputs)) {
            String validationMsg = formulaRow.getAt(COLUMNS.VALIDATION_MSG)
            Map newSeverity = SEVERITY.getAt(formulaRow.getAt(COLUMNS.SEVERITY))
            validationMatrix.addRow(["Validation Messages": validationMatrix.styledCell(validationMsg).withColor(newSeverity.color)])
            if (!severity || newSeverity.importance > severity.importance) {
                severity = newSeverity
                severity.msg = validationMsg
            }
        }
    }

    if (severity) {
        instance.setSeverity(severity)
        severity.alert(severity.msg)
    }

    return validationMatrix
}


BigDecimal roundOffPrice(BigDecimal valueToRound) {
    return valueToRound ? libs.SharedLib.RoundingUtils.round(valueToRound, 2) : valueToRound
}

ResultMatrix buildPriceRecordsMatrix(Map instance) {
    Map COLUMNS = libs.RukkiCommonLib.Constants.PRICERECORDS_MATRIX_COLUMNS
    ResultMatrix priceRecordsMatrix = api.newMatrix(COLUMNS.values() as List)
    String sku = instance.getSKU()
    String market = instance.getMarket()

    List prices = getAllPrices(sku, market)?.sort { Map priceRow1, Map priceRow2 ->
        priceRow2."Valid From" <=> priceRow1."Valid From"
    }

    def roundingUtils = libs.SharedLib.RoundingUtils

    prices?.each { Map priceRow ->
        boolean isCurrentPrice = instance.getAttribute("activePriceTypedId") == priceRow.PriceTypedId
        String color = isCurrentPrice ? '#FFFFFF' : null
        String bgColor = isCurrentPrice ? '#008941' : null
        priceRecordsMatrix.addRow([
                (COLUMNS.SKU)              : priceStyleCell(priceRecordsMatrix, sku, color, bgColor),
                (COLUMNS.MARKET)           : priceStyleCell(priceRecordsMatrix, market, color, bgColor),
                (COLUMNS.BASE_PRODUCT_CODE): priceStyleCell(priceRecordsMatrix, instance.getBaseProduct(), color, bgColor),
                (COLUMNS.BASE_PRICE)       : priceStyleCell(priceRecordsMatrix, roundingUtils.round(priceRow."Base Price", 2), color, bgColor),
                (COLUMNS.FLOOR_PRICE)      : priceStyleCell(priceRecordsMatrix, roundingUtils.round(priceRow."Floor Price", 2), color, bgColor),
                (COLUMNS.TARGET_PRICE)     : priceStyleCell(priceRecordsMatrix, roundingUtils.round(priceRow."Target Price", 2), color, bgColor),
                (COLUMNS.START_PRICE)      : priceStyleCell(priceRecordsMatrix, roundingUtils.round(priceRow."Start Price", 2), color, bgColor),
                (COLUMNS.VALID_FROM)       : priceStyleCell(priceRecordsMatrix, priceRow."Valid From", color, bgColor),
                (COLUMNS.VALID_UNTIL)      : priceStyleCell(priceRecordsMatrix, priceRow."Valid Until", color, bgColor)
        ])

    }

    return priceRecordsMatrix
}

def priceStyleCell(ResultMatrix matrix, Object data, String color, String bgColor) {
    return matrix.styledCell(data).withColor(color).withBackground(bgColor)
}