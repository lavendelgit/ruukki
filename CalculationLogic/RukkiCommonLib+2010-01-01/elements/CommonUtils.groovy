import net.pricefx.server.dto.calculation.AttributedResult
import net.pricefx.server.dto.calculation.ResultMatrix

BigDecimal nvl(value, BigDecimal defaultValue = 0.0) {
    try {
        return (value != null ? value : defaultValue) as BigDecimal
    } catch (ex) {
        return defaultValue
    }
}


AttributedResult getDeltaCell(BigDecimal numberValue, String suffix = null) {
    String textColor = numberValue ? (numberValue < 0 ? 'red' : 'green') : null
    def Constants = libs.RukkiCommonLib.Constants
    suffix = (suffix ? " " + suffix : "") + (numberValue ? (numberValue < 0 ? Constants.SYMBOLS.DECREASE : Constants.SYMBOLS.INCREASE) : Constants.SYMBOLS.FLAT)
    return api.attributedResult(numberValue).withTextColor(textColor).withSuffix(suffix)
}

AttributedResult decorateMissingDataCell(Object value) {
    if (value == null) {
        return api.attributedResult(value).withBackgroundColor("#FF0000").withTextColor("#FFFFFF")
    }

    return api.attributedResult(value)
}

List marketStringToList(String strToParse) {
    return strToParse?.replaceAll("[\\[\\]]", '')?.split(",")?.collect {
        it.trim()
    }
}
