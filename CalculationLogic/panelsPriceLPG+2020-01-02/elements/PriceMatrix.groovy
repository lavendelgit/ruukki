import net.pricefx.formulaengine.scripting.portlets.ResultMatrix

String sku = out.Product?.sku
String baseProduct = out.Product?.attribute1
String productCategory = out.Product?.attribute6
String market = out.Market
ResultMatrix matrix
Filter productCodeFilter = Filter.equal("key2", sku)
Filter baseproductFilter = Filter.equal("key2", baseProduct)
Filter productCategoryFilter = Filter.equal("key2", productCategory)
Filter marketFilter = Filter.equal("key1", market)

if (!api.global.basemarkups) {
    api.global.basemarkups = api.findLookupTableValues("Base_Sales_Markups", "-key3")?.groupBy {
        it.key1 + "_" + it.key2
    }
}
List basemarkups = api.global.basemarkups?.getAt(market + "_" + sku)//libs.claddingCOGS.BaseMarkup.getBaseMarkupList(marketFilter, productCodeFilter, baseproductFilter, productCategoryFilter, null)
if (!basemarkups) {
    basemarkups = api.global.basemarkups?.getAt(market + "_" + baseProduct)
}
if (!basemarkups) {
    basemarkups = api.global.basemarkups?.getAt(market + "_" + productCategory)
}
Filter segmentFilter = Filter.equal("key3", "A1")

if (!api.global.pmQtySegments) {
    api.global.pmQtySegments = api.findLookupTableValues("PM_QuantitySegments", segmentFilter)?.collectEntries {
        [(it.key1 + "_" + it.key2): it]
    }
}

Map pmQuantityMarkupdata = api.global.pmQtySegments?.getAt(market + "_" + sku)//api.findLookupTableValues("PM_QuantitySegments", productCodeFilter,marketFilter,segmentFilter)?.find()
if (!pmQuantityMarkupdata) {
    pmQuantityMarkupdata = api.global.pmQtySegments?.getAt(market + "_" + baseProduct)
}
if (!pmQuantityMarkupdata) {
    pmQuantityMarkupdata = api.global.pmQtySegments?.getAt(market + "_" + productCategory)
}

BigDecimal cogsPrice
BigDecimal transferPrice
BigDecimal costActual
BigDecimal processingCost
String panelType = api.product("Categories", sku)


api.local.panelNormData = api.find("PX", Filter.equal("name", "PanelsNorms"),
        Filter.equal("sku", sku))?.find()

api.local.panelPricesData = api.find("PX", Filter.equal("name", "PanelsPrices"),
        Filter.equal("sku", sku))

api.local.panelNormDataCOGS = api.find("PX", Filter.equal("name", "PanelsNormsCOGS"),
        Filter.equal("sku", sku))?.find()

api.local.panelPricesDataCOGS = api.find("PX", Filter.equal("name", "PanelsPricesCOGS"),
        Filter.equal("sku", sku))

List pricesData = []
api.local.panelPricesData.each { it ->

    BigDecimal externalSheetPrice = it?.attribute8 ?: BigDecimal.ZERO
    BigDecimal internalSheetPrice = it?.attribute10 ?: BigDecimal.ZERO

    BigDecimal internalsheetNorm = api.local.panelNormData?.attribute1 ?: BigDecimal.ZERO
    BigDecimal externalsheetNorm = api.local.panelNormData?.attribute2 ?: BigDecimal.ZERO

    BigDecimal sheetsActual = (internalSheetPrice * internalsheetNorm) + (externalSheetPrice * externalsheetNorm)

    BigDecimal foamNorm = api.local.panelNormData?.attribute3 ?: BigDecimal.ZERO
    BigDecimal foamPrice = it?.attribute1 ?: BigDecimal.ZERO

    BigDecimal foamActual = foamNorm * foamPrice

    BigDecimal packingMaterialsActuals = api.local.panelNormData?.attribute11

    BigDecimal foilnorm = api.local.panelNormData?.attribute5 ?: BigDecimal.ZERO
    BigDecimal foilPrice = it?.attribute3 ?: BigDecimal.ZERO

    BigDecimal protectiveFoilActual = foilnorm * foilPrice

    BigDecimal puGasketnorm = api.local.panelNormData?.attribute6 ?: BigDecimal.ZERO
    BigDecimal alfoilNorm = api.local.panelNormData?.attribute7 ?: BigDecimal.ZERO

    BigDecimal puGasketPrice = it?.attribute4 ?: BigDecimal.ZERO
    BigDecimal alfoilPrice = it?.attribute5 ?: BigDecimal.ZERO

    BigDecimal gasketandAlfoilactual = (puGasketnorm * puGasketPrice) + (alfoilNorm * alfoilPrice)
    BigDecimal primerNorm = api.local.panelNormData?.attribute4 ?: BigDecimal.ZERO
    BigDecimal primerPrice = it?.attribute2 ?: BigDecimal.ZERO

    BigDecimal primerActual = primerNorm * primerPrice

    BigDecimal glueNorm = api.local.panelNormData?.attribute8 ?: BigDecimal.ZERO
    BigDecimal gluePrice = it?.attribute6 ?: BigDecimal.ZERO

    BigDecimal glueActual = glueNorm * gluePrice

    BigDecimal mineralWoolNorm = api.local.panelNormData?.attribute9 ?: BigDecimal.ZERO
    BigDecimal mineralWoolPrice = it?.attribute7 ?: BigDecimal.ZERO

    BigDecimal mineralWoolActual = mineralWoolNorm * mineralWoolPrice

    BigDecimal paperGasketHumbPrice = it?.attribute9 ?: BigDecimal.ZERO
    BigDecimal paperGasketHumbNorm = api.local.panelNormData?.attribute13 ?: BigDecimal.ZERO

    BigDecimal paperGasketHumbActual = paperGasketHumbPrice * paperGasketHumbNorm

    costActual = sheetsActual + foamActual + packingMaterialsActuals + protectiveFoilActual + gasketandAlfoilactual + primerActual + glueActual + mineralWoolActual + paperGasketHumbActual

    processingCost = api.local.panelNormData?.attribute10

    cogsPrice = costActual + processingCost


    BigDecimal transferMarkup = api.local.panelNormData?.attribute12
    transferPrice = costActual + processingCost * (1 + transferMarkup)


    Map map = [:]
    map['cogsPrice'] = cogsPrice
    map['transferPrice'] = transferPrice
    map['validFrom'] = it?.attribute11
    map['lastUpdateDate'] = it?.lastUpdateDate
    pricesData << map
}

List newPricesData = []
api.local.panelPricesDataCOGS.each { it ->
    BigDecimal externalSheetPriceCOGS = it?.attribute8 ?: BigDecimal.ZERO
    BigDecimal internalSheetPriceCOGS = it?.attribute10 ?: BigDecimal.ZERO

    BigDecimal internalsheetNormCOGS = api.local.panelNormDataCOGS?.attribute1 ?: BigDecimal.ZERO
    BigDecimal externalsheetNormCOGS = api.local.panelNormDataCOGS?.attribute2 ?: BigDecimal.ZERO

    BigDecimal sheetsActualCOGS = (internalSheetPriceCOGS * internalsheetNormCOGS) + (externalSheetPriceCOGS * externalsheetNormCOGS)

    BigDecimal foamNormCOGS = api.local.panelNormDataCOGS?.attribute3 ?: BigDecimal.ZERO
    BigDecimal foamPriceCOGS = it?.attribute1 ?: BigDecimal.ZERO

    BigDecimal foamActualCOGS = (foamNormCOGS * foamPriceCOGS) ?: BigDecimal.ZERO

    BigDecimal packingMaterialsActualsCOGS = (api.local.panelNormDataCOGS?.attribute11) ?: BigDecimal.ZERO

    BigDecimal foilnormCOGS = api.local.panelNormDataCOGS?.attribute5 ?: BigDecimal.ZERO
    BigDecimal foilPriceCOGS = it?.attribute3 ?: BigDecimal.ZERO

    BigDecimal protectiveFoilActualCOGS = (foilnormCOGS * foilPriceCOGS) ?: BigDecimal.ZERO

    BigDecimal puGasketnormCOGS = api.local.panelNormDataCOGS?.attribute6 ?: BigDecimal.ZERO
    BigDecimal alfoilNormCOGS = api.local.panelNormDataCOGS?.attribute7 ?: BigDecimal.ZERO

    BigDecimal puGasketPriceCOGS = it?.attribute4 ?: BigDecimal.ZERO
    BigDecimal alfoilPriceCOGS = it?.attribute5 ?: BigDecimal.ZERO

    BigDecimal gasketandAlfoilactualCOGS = ((puGasketnormCOGS * puGasketPriceCOGS) + (alfoilNormCOGS * alfoilPriceCOGS)) ?: 0

    BigDecimal primerNormCOGS = api.local.panelNormDataCOGS?.attribute4 ?: BigDecimal.ZERO
    BigDecimal primerPriceCOGS = it?.attribute2 ?: BigDecimal.ZERO

    BigDecimal primerActualCOGS = (primerNormCOGS * primerPriceCOGS) ?: BigDecimal.ZERO

    BigDecimal glueNormCOGS = api.local.panelNormDataCOGS?.attribute8 ?: BigDecimal.ZERO
    BigDecimal gluePriceCOGS = it?.attribute6 ?: BigDecimal.ZERO

    BigDecimal glueActualCOGS = glueNormCOGS * gluePriceCOGS

    BigDecimal mineralWoolNormCOGS = api.local.panelNormDataCOGS?.attribute9 ?: BigDecimal.ZERO
    BigDecimal mineralWoolPriceCOGS = it?.attribute7 ?: BigDecimal.ZERO

    BigDecimal mineralWoolActualCOGS = mineralWoolNormCOGS * mineralWoolPriceCOGS

    BigDecimal paperGasketHumbPriceCOGS = it?.attribute9 ?: BigDecimal.ZERO
    BigDecimal paperGasketHumbNormCOGS = api.local.panelNormDataCOGS?.attribute13 ?: BigDecimal.ZERO

    BigDecimal paperGasketHumbActualCOGS = paperGasketHumbPriceCOGS * paperGasketHumbNormCOGS

    costActualCOGS = sheetsActualCOGS + foamActualCOGS + packingMaterialsActualsCOGS + protectiveFoilActualCOGS + gasketandAlfoilactualCOGS + primerActualCOGS + glueActualCOGS + mineralWoolActualCOGS + paperGasketHumbActualCOGS

    BigDecimal processingCostCOGS = (api.local.panelNormDataCOGS?.attribute10) ?: BigDecimal.ZERO

    costActualCOGS = costActualCOGS + (api.local.category2_Optionsprice ?: BigDecimal.ZERO)

    processingCostCOGS = processingCostCOGS + (api.local.category1_Optionsprice ?: BigDecimal.ZERO)

    newCogsPrice = costActualCOGS + processingCostCOGS
    BigDecimal newTransferMarkup = (api.local.panelNormDataCOGS?.attribute12) ?: BigDecimal.ZERO
    newTransferPrice = costActualCOGS + processingCostCOGS * (1 + newTransferMarkup)
    Map map = [:]
    map['newCogsPrice'] = newCogsPrice
    map['newTransferPrice'] = newTransferPrice
    map['newValidFrom'] = it?.attribute11
    map['newLastUpdateDate'] = it?.lastUpdateDate
    newPricesData << map
}

List prices = []
BigDecimal basePrice
BigDecimal newBasePrice
BigDecimal target_A1
BigDecimal target_A8
BigDecimal start_A1
BigDecimal start_A8
BigDecimal floor_A1
BigDecimal floor_A8
BigDecimal rawMaterialCost
BigDecimal startPrice
BigDecimal floorPrice
BigDecimal targetPrice
String rawMaterialValidFrom
String today = new Date().format('yyyy-MM-dd')

validRecords = pricesData?.findAll { it.validFrom <= today }
Map validRecord = validRecords?.sort { a, b -> b.validFrom <=> a.validFrom }?.getAt(0)
api.local.LastUpdateDatePanels = validRecord?.lastUpdateDate
api.local.cogsPrice = validRecord?.cogsPrice
api.local.transferPrice = validRecord?.transferPrice
rawMaterialValidFrom = validRecord?.validFrom
basePrice = (validRecord?.cogsPrice ?: 0.0) * (1.0 + basemarkups?.getAt(0)?.attribute1 as BigDecimal)

newValidRecords = newPricesData?.findAll { it.newValidFrom <= today }
Map newValidRecord = newValidRecords?.sort { a, b -> b.newValidFrom <=> a.newValidFrom }?.getAt(0)
api.local.newLastUpdateDatePanels = newValidRecord?.newLastUpdateDate
api.local.newCogsPrice = newValidRecord?.newCogsPrice
api.local.newTransferPrice = newValidRecord?.newTransferPrice
newBasePrice = (newValidRecord?.newCogsPrice ?: 0.0) * (1.0 + basemarkups?.getAt(0)?.attribute1 as BigDecimal)

if (market != "FI") {
    targetPrice = (basePrice as BigDecimal) * ((1.0 + (basemarkups?.getAt(0)?.attribute5)) as BigDecimal)
    startPrice = ((targetPrice as BigDecimal) * ((1 + (basemarkups?.getAt(0)?.attribute2)) as BigDecimal))
    floorPrice = (targetPrice as BigDecimal) * (1.0 - (basemarkups?.getAt(0)?.attribute4) as BigDecimal)
} else {
    target_A8 = (basePrice as BigDecimal) * ((1.0 + (basemarkups?.getAt(0)?.attribute5)) as BigDecimal)
    target_A1 = (basePrice as BigDecimal) * (1.0 + pmQuantityMarkupdata?.attribute3) as BigDecimal
    floor_A1 = target_A1 * (1.0 - (basemarkups?.getAt(0)?.attribute4) as BigDecimal)
    floor_A8 = target_A8 * (1.0 - (basemarkups?.getAt(0)?.attribute4) as BigDecimal)
    start_A1 = target_A1 * (1.0 + (basemarkups?.getAt(0)?.attribute2) as BigDecimal)
    start_A8 = target_A8 * (1.0 + (basemarkups?.getAt(0)?.attribute2) as BigDecimal)
}
prices << ["cogsPrice"       : api.local.cogsPrice,
           "basePrice"       : basePrice,
           "newCogsPrice"    : api.local.newCogsPrice,
           "newBasePrice"    : newBasePrice,
           "rawMaterialCost" : costActual,
           "processingCost"  : processingCost,
           "startPrice"      : startPrice,
           "floorPrice"      : floorPrice,
           "targetPrice"     : targetPrice,
           "A1_target"       : target_A1,
           "A8_target"       : target_A8,
           "A1_start"        : start_A1,
           "A8_start"        : start_A8,
           "A1_floor"        : floor_A1,
           "A8_floor"        : floor_A8,
           "transferPrice"   : api.local.transferPrice,
           "newTransferPrice": api.local.newTransferPrice,
           "validFrom"       : rawMaterialValidFrom]
matrix = api.newMatrix("cogsPrice", "basePrice", "newCogsPrice", "newBasePrice", "rawMaterialCost", "processingCost", "startPrice", "floorPrice", "targetPrice", "A1_target", "A8_target", "A1_start", "A8_start", "A1_floor",
        "A8_floor", "transferPrice", "newTransferPrice", "validFrom")

prices.each {
    matrix.addRow(it)
}

return matrix



