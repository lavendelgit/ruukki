def skus = []

Map product = api.product()
if (api.isDebugMode()) {
    product = api.find("P", Filter.equal("sku", "P00422"))?.find()
}
def markets = product?.attribute5.replaceAll(']', "").substring(1)
def sku = product?.sku
List marketList = markets?.tokenize(", ")

marketList.each { market ->
    def SecondaryKey = market + "|" + sku
    skus?.add(SecondaryKey)
}
return skus