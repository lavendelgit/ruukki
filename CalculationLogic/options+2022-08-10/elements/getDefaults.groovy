def main(Map Product) {

    /*
     *   Product.productId
     *   Product.productName
	 *   Product.productGroup
	 *   Product.productGroupCode
     *   Product.market
     *   Product.currency
     *   Product.language
     *   Product.creationDate
     *   Product.unitOfMeasure
	 */


// put a new filter for
    // Gather the options
//    def product_filter = Filter.or(
//            Filter.equal("attribute1", Product.productCode),
//            Filter.equal("sku", Product.productCode)
//    )
//    // Technical options have no market and use the keyword "all"
//    def market_filter = Filter.or(
//            Filter.equal("attribute2", Product.market),
//            Filter.equal("attribute2", "all")
//    )
    def hidden_filter = Filter.or(
            Filter.notEqual("attribute13", "1"),
            Filter.notEqual("attribute13", 1),
            Filter.isNull("attribute13")
    )

    def name_filter = Filter.equal("name", "DefaultOptions")
    //api.trace("**********ProductCodebeforecache********", Product.productCode)

    def startRow = 0
    if (!api.global.defaultoptions) {
        def defaultDataStream = api.stream("PX20", null, ["sku", "attribute1", "attribute2", "attribute3",
                                                          "attribute4", "attribute5", "attribute6", "attribute7", "attribute8",
                                                          "attribute9", "attribute10", "attribute11", "attribute12", "attribute14", "attribute16"],
                name_filter, hidden_filter)

        api.global.defaultoptions = defaultDataStream.collect().groupBy { it.attribute2 }

        defaultDataStream.close()

        //api.trace("********productcode******", Product.productCode)
    }
    api.logInfo("******api.global.defaultoptions", api.global.defaultoptions)
    def conversion_factor = new BigDecimal('1')


    // Change the conversion factor if needed
    if (Product.currency != 'EUR') {
        try {
            conversion_factor = libs.currencyConversion.getFactor.main('EUR', Product.currency)
        } catch (e) {
            api.logWarn('Currency conversion failed in components default options module.')
        }
    }
    //api.trace("-----------api.global.defaultoptions size",api.global.defaultoptions.size())
// RUUK-47 changes starts here : Caching whole table instead of querying table every time
    if (!api.global.translationTable) {
        api.global.translationTable = api.findLookupTableValues("optionsTranslations")?.collectEntries { [(it.key1.toLowerCase() + "_" + it.key2.toLowerCase()): it] }
    }
    if (!api.global.sortingtable) {
        api.global.sortingtable = api.findLookupTableValues("optionsOrderingNew")?.collectEntries { [(it.name.toLowerCase()): it.attribute1] }
    }
    // Gather the data into the required output format

    def product_filter = Filter.or(
            Filter.equal("attribute1", Product.productCode),
            Filter.equal("sku", Product.productCode)
    )


    List defaultOptions = api.global.defaultoptions?.subMap(Product.market, "all", "All", "ALL")?.values()
            ?.flatten()?.findAll { it ->
        [it.attribute1, it.sku].contains(Product.productCode)
    }


    def formatted_rows = defaultOptions.collect {

        [
                optionGroupKey       : it.attribute3
                , optionGroupLabel   : (api.global.translationTable.getAt(it.attribute3?.toLowerCase() + "_" + Product.language?.toLowerCase()) ?: api.global.translationTable.getAt(it.attribute3?.toLowerCase() + "_" + "en"))?.attribute1 ?: it.attribute3
                , optionGroupSapLabel: api.global.translationTable.getAt(it.attribute3?.toLowerCase() + "_" + "sap")?.attribute1 ?: it.attribute3
                , groupKey           : it.attribute5
                , groupLabel         : (api.global.translationTable.getAt(it.attribute5?.toLowerCase() + "_" + Product.language?.toLowerCase()) ?: api.global.translationTable.getAt(it.attribute5?.toLowerCase() + "_" + "en"))?.attribute1 ?: it.attribute5
                , groupSapLabel      : api.global.translationTable.getAt(it.attribute5?.toLowerCase() + "_" + "sap")?.attribute1 ?: it.attribute5
                , optionKey          : it.attribute7
                , optionLabel        : (api.global.translationTable.getAt(it.attribute7?.toLowerCase() + "_" + Product.language?.toLowerCase()) ?: api.global.translationTable.getAt(it.attribute7?.toLowerCase() + "_" + "en"))?.attribute1 ?: it.attribute7
                , optionSapLabel     : api.global.translationTable.getAt(it.attribute7?.toLowerCase() + "_" + "sap")?.attribute1 ?: it.attribute7
                , colorDescription   : it.attribute7.substring(0, 2) == "col" ? ((api.global.translationTable.getAt(it.attribute7 + "_" + Product.language) ?: api.global.translationTable.getAt(it.attribute7 + "_" + "en")))?.attribute2 : null
                , price              : it.attribute9 as BigDecimal
                , displayPrice       : it.attribute9 ? ((it.attribute9 as BigDecimal) * conversion_factor) : 0
                , default            : true
                , selected           : (it.attribute10 == 1 || it.attribute10 == "1" || it.attribute10 == "1.0") ? true : false
                , special            : (it.attribute11 == "1" || it.attribute11 == "1.0") ? true : false
                , mandatory          : (it.attribute14 == "1" || it.attribute14 == "1.0") ? true : false
                , lowcarbon          : (it?.attribute16 == "true" ? true : false)
                , optionGroupSort    : api.global.sortingtable.getAt(it.attribute3?.toLowerCase()) as Integer ?: 0
                , groupSort          : api.global.sortingtable.getAt(it.attribute5?.toLowerCase()) as Integer ?: 0
                , optionSort         : api.global.sortingtable.getAt(it.attribute7?.toLowerCase()) as Integer ?: 0
                , extraCost          : true
        ]
    }

    // Sort the output alphabetically, not the *Sort attributes are relevant only for frontend
    def output_rows = formatted_rows.toSorted { a, b ->
        a.optionGroupKey <=> b.optionGroupKey ?: a.groupKey <=> b.groupKey ?: a.optionKey <=> b.optionKey
    }
    //api.trace("______output_rows",output_rows)

    if (output_rows.size() < 1) {
        api.logWarn("Default Options empty for request ${Product.dump()}")
    }

    return output_rows ?: []
}


