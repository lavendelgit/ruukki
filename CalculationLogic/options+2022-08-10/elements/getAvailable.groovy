def main(Map Product) {
    /*
     *   Product.productId
     *   Product.market
	 *   Product.currency
     *   Product.language
     *   Product.creationDate
	 *   Product.optionGroup [optionGroup [Group[options]], optionGroup[Group[options]]]
	 */


    def designProfile = (Product.productType == "DESIGN_PROFILE") ? true : false

    // Technical options are valid for any market and there we use the keyword "all"
    def market_filter = Filter.or(Filter.equal("attribute2", Product.market),
            Filter.equal("attribute2", "all"))
    // We do not take any options which are explicitly made hidden
    def hidden_filter = Filter.or(Filter.notEqual("attribute20", "1"),
            Filter.notEqual("attribute20", 1),
            Filter.isNull("attribute20"))
    def hidden_filter_default = Filter.or(Filter.notEqual("attribute13", "1"),
            Filter.notEqual("attribute13", 1),
            Filter.isNull("attribute13"))
    def product_filter = Filter.or(Filter.equal("attribute1", Product.productCode),
            Filter.equal("sku", Product.productCode))


    def name_filter = Filter.equal("name", "AvailableOptions")
    def name_filter_default = Filter.equal("name", "DefaultOptions")
    def received_rows = Product.optionGroup
    received_rows?.removeAll { r -> (r.groupKey).contains("gwp") && r.selected == true }
    def received_rows_copy = received_rows.clone()

    if (!api.global.defaultoptions) {
        def defaultDataStream = api.stream("PX20", null, ["sku", "attribute1", "attribute2", "attribute3",
                                                          "attribute4", "attribute5", "attribute6", "attribute7", "attribute8",
                                                          "attribute9", "attribute10", "attribute11", "attribute12", "attribute14", "attribute16"],
                name_filter_default, hidden_filter_default)
        api.global.defaultoptions = defaultDataStream.collect().groupBy { it.attribute2 }
        defaultDataStream.close()

    }
    api.logInfo("************defaultoptions cache", api.global.defaultoptions)
    if (!api.global.availableoptions) {
        def availabledata = api.stream("PX30", null, ["sku", "attribute1", "attribute2", "attribute3",
                                                      "attribute4", "attribute5", "attribute6", "attribute7", "attribute8",
                                                      "attribute9", "attribute10", "attribute11", "attribute12", "attribute14", "attribute13",
                                                      "attribute15", "attribute16", "attribute17", "attribute18", "attribute19", "attribute20", "attribute21", "attribute22", "attribute24"],
                hidden_filter, name_filter)
        api.global.availableoptions = availabledata.collect().groupBy { it.attribute2 }
        availabledata.close()
    }
    api.logInfo("************availbleoptions cache", api.global.availableoptions)

    def conversion_factor = new BigDecimal("1")

    if (!api.global.translationTable) {
        api.global.translationTable = api.findLookupTableValues("optionsTranslations")?.collectEntries { [(it.key1.toLowerCase() + "_" + it.key2.toLowerCase()): it] }
    }
    if (!api.global.sortingtable) {
        api.global.sortingtable = api.findLookupTableValues("optionsOrderingNew")?.collectEntries { [(it.name.toLowerCase()): it.attribute1] }
    }

//    Change the conversion factor if needed
    if (Product.currency != 'EUR') {
        try {
            conversion_factor = libs.currencyConversion.getFactor.main('EUR', Product.currency)
        } catch (e) {
            api.logWarn('Currency conversion failed in components available options module.')
        }
    }

    List defaultOptions = api.global.defaultoptions?.subMap(Product.market, "ALL", "All", "all")?.values()?.flatten()?.findAll { it -> [it.attribute1, it.sku].contains(Product.productCode)
    }

    def default_rows_data = defaultOptions.collect { defaultOption ->
        [optionGroupKey: defaultOption?.attribute3
         , groupKey    : defaultOption?.attribute5
         , optionKey   : defaultOption?.attribute7
         , price       : (defaultOption?.attribute9) as BigDecimal
         , displayPrice: ((defaultOption?.attribute9 as BigDecimal) ?: 0) * conversion_factor
         , default     : (defaultOption?.attribute10 == "1" || defaultOption?.attribute10 == "1.0" || defaultOption?.attribute10 == 1) ? true : false
         , special     : (defaultOption?.attribute11 == "1" || defaultOption?.attribute11 == "1.0") ? true : false
         , mandatory   : (defaultOption?.attribute14 == "1" || defaultOption?.attribute14 == "1.0") ? true : false
         , lowcarbon   : (defaultOption?.attribute16 == "true" ? true : false)
         , selected    : false
         , extraCost   : (Product.lamellaCladding || designProfile) ? specialMaterial(defaultOption?.attribute7) : false]
    }

    api.trace("default_rows_data from getavailable", default_rows_data)
    List availableOptions = api.global.availableoptions?.subMap(Product.market, "all", "All", "ALL")?.values()?.flatten()?.findAll { it -> [it.attribute1, it.sku].contains(Product.productCode)
    }


    api.trace("_--------availableOptions", availableOptions)
    // Iterate on the received options to find out what to return given the incoming options
    for (received_row in received_rows) {

        // Get the rows that need to be modified
        def new_rows = availableOptions.findAll {
            it.attribute3 == received_row.optionGroupKey && it.attribute7 == received_row.optionKey
        }
        // Iterate on all news rows in case one option impact many others
        for (new_row in new_rows) {
            // If we have an add match, add the row of new_rows to the default_rows
            if (new_row.attribute17 == 'add') {
                def data_to_add = [optionGroupKey: new_row.attribute9
                                   , groupKey    : new_row.attribute11
                                   , optionKey   : new_row.attribute13
                                   , price       : new_row.attribute15 as BigDecimal
                                   , displayPrice: ((new_row.attribute15 as BigDecimal ?: 0) * conversion_factor)
                                   , default     : (new_row.attribute16 == "1.0" || new_row.attribute16 == "1" || new_row.attribute16 == 1) ? true : false
                                   , special     : (new_row.attribute18 == "1.0" || new_row.attribute18 == "1") ? true : false
                                   , mandatory   : (new_row.attribute22 == "1.0" || new_row.attribute22 == "1") ? true : false
                                   , lowcarbon   : (new_row.attribute24 == "true" ? true : false)
                                   , selected    : (new_row.attribute16 == "1.0" || new_row.attribute16 == "1" || new_row.attribute16 == 1) ? true : false
                                   , extraCost   : (Product.lamellaCladding || designProfile) ? specialMaterial(new_row.attribute7) : false]

                default_rows_data.addAll(data_to_add)

            }

            // If we have a del, we remove both ways, the filtered options, and the filtering options in the received data
            if (new_row.attribute17 == 'del') {
                default_rows_data.removeAll { r -> r.optionGroupKey == new_row.attribute9 && r.optionKey == new_row.attribute13 }
                received_rows_copy.removeAll { r -> r.optionGroupKey == new_row.attribute9 && r.optionKey == new_row.attribute13 }
            }
        }

        // Then remove the passed selections from the defaults as well because we will merge the options received
        default_rows_data.removeAll { r -> r.optionGroupKey == received_row.optionGroupKey && r.optionKey == received_row.optionKey
        }
    }

    if (Product.lamellaCladding) {
        BigDecimal received_Optionsprice = 0

        //RUUK-195 changes for cladding options price
        //calculation of extra costs
        List received_options = received_rows_copy?.findAll { (it.optionGroupKey != "dimensions") && (it.extraCost == true) }
        for (received_option in received_options) {
            if (received_option.price > 0) {
                received_Optionsprice = (received_Optionsprice + received_option.price)?.setScale(1, BigDecimal.ROUND_HALF_UP)
            }
        }

        List received_dimensions = received_rows_copy?.findAll { it.optionGroupKey == "dimensions" }
        default_dimensions = default_rows_data?.findAll { it.optionGroupKey == "dimensions" }
        //Combining the received dimensions and the Default Dimensions
        received_dimensions.addAll(default_dimensions)

        //baseMarkup calculation
        def marketFilter = com.googlecode.genericdao.search.Filter.equal("key1", Product.market)
        def productCodeFilter = com.googlecode.genericdao.search.Filter.equal("key2", Product.productCode)
        def baseproductFilter = com.googlecode.genericdao.search.Filter.equal("key2", Product.baseProduct)
        def productCategoryFilter = com.googlecode.genericdao.search.Filter.equal("key2", Product.productCategory)
        def baseMarkup = libs.claddingCOGS.BaseMarkup.getBaseMarkup(marketFilter, productCodeFilter, baseproductFilter, productCategoryFilter, Product.pricingDate)


        for (received_dimension in received_dimensions) {

            def materialKey = received_rows_copy?.find { it.optionGroupKey == "external" && it.groupKey.contains("material") }?.optionKey
            def thicknessKey = received_rows_copy?.find { it.optionGroupKey == "external" && it.groupKey.contains("thickness") }?.optionKey
            def coatingKey = received_rows_copy?.find { it.optionGroupKey == "external" && it.groupKey.contains("coating") }?.optionKey
            def colorKey = received_rows_copy?.find { it.optionGroupKey == "external" && it.groupKey.contains("color") }?.optionKey
            def received_DimensionPrice
            def length
            def height


            if (received_dimension?.standard == false) {

                def cogsPrice
                length = received_dimension?.optionKey.split('/')[0] as BigDecimal
                height = received_dimension?.optionKey.split('/')[1] as BigDecimal
                def coilWidth = api.find("PX", Filter.equal("name", "CladdingProcessingCost"), Filter.equal("sku", Product.productCode))?.getAt(0)?.attribute8


                if (length <= 3900 && height <= coilWidth) {
                    if (received_dimension.dirty == true) {
                        received_dimension.price = conversion_factor * getPriceOfReceivedDimension(length, height, Product.productCode, materialKey, thicknessKey, coatingKey, colorKey, Product.pricingDate, received_dimension?.qty, received_dimension.dirty, received_dimension.price, received_Optionsprice, baseMarkup)
                    } else {
                        received_dimension.price = getPriceOfReceivedDimension(length, height, Product.productCode, materialKey, thicknessKey, coatingKey, colorKey, Product.pricingDate, received_dimension?.qty, received_dimension.dirty, received_dimension.price, received_Optionsprice, baseMarkup)
                    }

                } else if (height <= 3900 && length <= coilWidth) {
                    if (received_dimension.dirty == true) {
                        received_dimension.price = conversion_factor * getPriceOfReceivedDimension(length, height, Product.productCode, materialKey, thicknessKey, coatingKey, colorKey, Product.pricingDate, received_dimension?.qty, received_dimension.dirty, received_dimension.price, received_Optionsprice, baseMarkup)
                    } else {
                        received_dimension.price = getPriceOfReceivedDimension(length, height, Product.productCode, materialKey, thicknessKey, coatingKey, colorKey, Product.pricingDate, received_dimension?.qty, received_dimension.dirty, received_dimension.price, received_Optionsprice, baseMarkup)
                    }
                } else {
                    api.logWarn("Dimensions Length should not be greater than 3900/ height should not be greater than ${coilWidth}")
                    received_dimension.price = 0
                }


            } else if (received_dimension?.standard == true || received_dimension?.standard == null) {


                //read cogsPrice from pricelist: get all params from secondarykey
                if (!thicknessKey) {
                    thicknessKey = "*"
                }
                if (!coatingKey) {
                    coatingKey = "*"
                }
                if (!colorKey) {
                    colorKey = "*"
                }
                def tempDimPrice = received_dimension.price

                def secondaryKey = Product.market + "|" + received_dimension?.optionKey + "|" + materialKey + "|" + thicknessKey + "|" + coatingKey + "|" + colorKey

                def lpgName = "StdDimensionsPrices_Cladding"
                def lpgi = getPriceFromLpg(lpgName, Product.productCode, secondaryKey)

                if (!lpgi) {
                    colorKey = "*"
                    secondaryKey = Product.market + "|" + received_dimension?.optionKey + "|" + materialKey + "|" + thicknessKey + "|" + coatingKey + "|" + colorKey
                    lpgi = getPriceFromLpg(lpgName, Product.productCode, secondaryKey)
                    if (!lpgi) {
                        coatingKey = "*"
                        secondaryKey = Product.market + "|" + received_dimension?.optionKey + "|" + materialKey + "|" + thicknessKey + "|" + coatingKey + "|" + colorKey
                        lpgi = getPriceFromLpg(lpgName, Product.productCode, secondaryKey)
                    }
                    if (!lpgi) {
                        received_dimension.price = 0.0
                    }
                }
                if (lpgi) {
                    def today = new Date().format('yyyy-MM-dd')
                    def matrix = lpgi?.calculationResults?.find { it.resultName == "PriceMatrix" }?.result?.entries


                    if (Product.pricingDate) {
                        matrix = matrix.findAll { it.rawMaterialValidFrom <= Product.pricingDate && it.claddingCostValidFrom <= Product.pricingDate && it.baseMarkupsValidFrom <= Product.pricingDate }
                    } else {
                        matrix = matrix.findAll { it.rawMaterialValidFrom <= today && it.claddingCostValidFrom <= today && it.baseMarkupsValidFrom <= today }
                    }

                    received_DimensionPrice = matrix?.sort { a, b -> b.rawMaterialValidFrom <=> a.rawMaterialValidFrom ?: b.claddingCostValidFrom <=> a.claddingCostValidFrom ?: b.baseMarkupsValidFrom <=> a.baseMarkupsValidFrom }
                            ?.getAt(0)?.cogsPrice?.setScale(1, BigDecimal.ROUND_HALF_UP)


                    //calculation of basePrice by adding received_Optionsprice to cogsPrice and multiplied with (1.0 + baseMarkup)
                    received_dimension.price = ((((received_DimensionPrice ?: 0) + (received_Optionsprice ?: 0)) * (1.0 + (baseMarkup ?: 0))) * conversion_factor)?.setScale(1, BigDecimal.ROUND_HALF_UP)

                }

                if (tempDimPrice > 0 && tempDimPrice != received_dimension.price) {
                    received_dimension.displayPrice = (tempDimPrice * conversion_factor)?.setScale(1, BigDecimal.ROUND_HALF_UP)
                } else {
                    received_dimension.displayPrice = received_dimension.price?.setScale(1, BigDecimal.ROUND_HALF_UP)
                }
            }
        }

    }
// RUUK-178 : Using "en" as fallback language for options translations.
    // Add sorting, selected and labels, this picks up new defaults as well
    def selected_rows = received_rows_copy.collect {
        BigDecimal displayPrice_value = (it.displayPrice) ? (it.displayPrice as BigDecimal) : ((it.price as BigDecimal) ?: 0 as BigDecimal)
        boolean similarPrice = (it.price && displayPrice_value) ? ((it.price == displayPrice_value) ? true : false) : false
        if (it.price) {
            it.price = (similarPrice) ? ((it.price as BigDecimal) * (1 / (conversion_factor as BigDecimal))).setScale(3, BigDecimal.ROUND_HALF_UP) : (it.price as BigDecimal).setScale(3, BigDecimal.ROUND_HALF_UP)
        }
        it << [default              : it.default == true ? true : false
               , selected           : true
               , mandatory          : it.mandatory == true ? true : false
               , displayPrice       : displayPrice_value
               , optionGroupLabel   : api.global.translationTable?.getAt(it.optionGroupKey?.toLowerCase() + "_" + Product.language?.toLowerCase())?.attribute1 ?: api.global.translationTable?.getAt(it.optionGroupKey?.toLowerCase() + "_" + "en")?.attribute1
               , optionGroupSapLabel: api.global.translationTable?.getAt(it.optionGroupKey?.toLowerCase() + "_" + "sap")?.attribute1 ?: it.optionGroupKey
               , groupLabel         : api.global.translationTable?.getAt(it.groupKey?.toLowerCase() + "_" + Product.language?.toLowerCase())?.attribute1 ?: api.global.translationTable?.getAt(it.groupKey?.toLowerCase() + "_" + "en")?.attribute1
               , groupSapLabel      : api.global.translationTable?.getAt(it.groupKey?.toLowerCase() + "_" + "sap")?.attribute1 ?: it.groupKey
               , colorDescription   : it.optionKey.substring(0, 2) == "col" ? (api.global.translationTable?.getAt(it.optionKey?.toLowerCase() + "_" + Product.language) ?: api.global.translationTable?.getAt(it.optionKey?.toLowerCase() + "_" + "en"))?.attribute2 : null
               , optionLabel        : it.special == true ? it.optionLabel : (api.global.translationTable?.getAt(it.optionKey?.toLowerCase() + "_" + Product.language?.toLowerCase()) ?: api.global.translationTable?.getAt(it.optionKey?.toLowerCase() + "_" + "en"))?.attribute1
               , optionSapLabel     : it.special == true ? it.optionLabel : api.global.translationTable?.getAt(it.optionKey?.toLowerCase() + "_" + "sap")?.attribute1 ?: it.optionKey
               , optionGroupSort    : api.global.sortingtable?.getAt(it.optionGroupKey?.toLowerCase()) as Integer ?: 0
               , groupSort          : api.global.sortingtable?.getAt(it.groupKey?.toLowerCase()) as Integer ?: 0
               , optionSort         : api.global.sortingtable.getAt(it.optionKey?.toLowerCase()) as Integer ?: 0
               , lowcarbon          : it.lowcarbon == true ? true : false
               , extraCost          : (Product.lamellaCladding || designProfile) ? specialMaterial(it.optionKey) : false
               , exchangeRate       : conversion_factor]
    }

    def default_and_selected = default_rows_data.collect {


        it << [selected             : it.selected
               , mandatory          : it.mandatory == true ? true : false
               , displayPrice       : it.displayPrice
               , optionGroupLabel   : (api.global.translationTable?.getAt(it.optionGroupKey?.toLowerCase() + "_" + Product.language?.toLowerCase()) ?: api.global.translationTable?.getAt(it.optionGroupKey?.toLowerCase() + "_" + "en"))?.attribute1 ?: it.optionGroupKey
               , optionGroupSapLabel: api.global.translationTable?.getAt(it.optionGroupKey?.toLowerCase() + "_" + "sap")?.attribute1 ?: it.optionGroupKey
               , groupLabel         : (api.global.translationTable?.getAt(it.groupKey?.toLowerCase() + "_" + Product.language?.toLowerCase()) ?: api.global.translationTable?.getAt(it.groupKey?.toLowerCase() + "_" + "en"))?.attribute1 ?: it.groupKey
               , groupSapLabel      : api.global.translationTable?.getAt(it.groupKey?.toLowerCase() + "_" + "sap")?.attribute1 ?: it.groupKey
               , optionLabel        : (api.global.translationTable?.getAt(it.optionKey?.toLowerCase() + "_" + Product.language?.toLowerCase()) ?: api.global.translationTable?.getAt(it.optionKey?.toLowerCase() + "_" + "en"))?.attribute1 ?: it.optionKey
               , optionSapLabel     : api.global.translationTable?.getAt(it.optionKey?.toLowerCase() + "_" + "sap")?.attribute1 ?: it.optionKey
               , colorDescription   : it.optionKey.substring(0, 2) == "col" ? (api.global.translationTable.getAt(it.optionKey?.toLowerCase() + "_" + Product.language) ?: api.global.translationTable.getAt(it.optionKey?.toLowerCase() + "_" + "en"))?.attribute2 : null
               , optionGroupSort    : api.global.sortingtable?.getAt(it.optionGroupKey?.toLowerCase()) as Integer ?: 0
               , groupSort          : api.global.sortingtable?.getAt(it.groupKey?.toLowerCase()) as Integer ?: 0
               , optionSort         : api.global.sortingtable?.getAt(it.optionKey?.toLowerCase()) as Integer ?: 0
               , lowcarbon          : it.lowcarbon == true ? true : false
               , extraCost          : (Product.lamellaCladding || designProfile) ? specialMaterial(it.optionKey) : false
               , exchangeRate       : conversion_factor]
    }

    // Ensure only one option is set to <selected> in a group of options
    // This logic handles cases where a previously <selected>, non-default options gets removed and a default option remains available
    // We possibly would need to cover cases where both the previously selected and default options get removed which means
    // we need to set the <selected> flag to true on the option with the smallest <optionSort> number with .min { it.optionSort }
    def checked_selections = default_and_selected.collect { d ->
        d << [selected: d.selected == true ? (selected_rows.any { o -> o.optionGroupKey == d.optionGroupKey && o.groupKey == d.groupKey && o.selected == true
        } ? false : true) : false]
    }

    // Merge defaults and received rows
    default_and_selected.addAll(selected_rows)

    // Sort the output alphabetically, not the *Sort attributes are relevant only for frontend
    def sorted = default_and_selected.toSorted { a, b -> a.optionGroupKey <=> b.optionGroupKey ?: a.groupKey <=> b.groupKey ?: a.optionKey <=> b.optionKey
    }

    // Sort the attributes of each option object
    def output_rows = sorted.collect {
        it << it.sort()
    }

    output_rows = output_rows.unique { [it.optionGroupKey, it.groupKey, it.optionKey] }

    if (output_rows.size() < 1) {
        api.logWarn("Available Options empty for request ${Product.dump()}")
    }

    return output_rows ?: []
}

def getPriceFromLpg(String lpgName, String productCode, String secondaryKey) {

    return (api.find("XPGI", 0, 1, "sku",
            Filter.equal("sku", productCode),
            Filter.equal("priceGridId", api.find("PG", Filter.equal("label", lpgName))?.getAt(0)?.id),
            Filter.equal("key2", secondaryKey))[0])
}

def getPriceOfReceivedDimension(def length, def height, def productCode, def materialKey, def thicknessKey, def coatingKey, def colorKey, def pricingDate, def quantity, def dirty, def price, def optionsPrice, def baseMarkup) {

    String rawMaterialTableName = "RawMaterialPrice"
    cogsPrice = libs.claddingCOGS.COGSPrice.getOptionsPrice(productCode, length, height, materialKey,
            thicknessKey, coatingKey, colorKey, pricingDate, quantity, rawMaterialTableName)?.cogsPrice

    //calculation of basePrice by adding optionsPrice to cogsPrice and multiplied with (1.0 + baseMarkup)
    received_DimensionPrice = (dirty == true) ? (((cogsPrice ?: 0) + (optionsPrice ?: 0)) * (1.0 + (baseMarkup ?: 0)))?.setScale(1, BigDecimal.ROUND_HALF_UP) : price
    return received_DimensionPrice
}


def specialMaterial(String material) {
    if (!api.global.specialMaterials) {
        api.global.specialMaterials = api.findLookupTableValues("SpecialMaterials")?.key2?.unique() as List
    }
    if (material in api.global.specialMaterials) {
        return true
    }
    return false
}