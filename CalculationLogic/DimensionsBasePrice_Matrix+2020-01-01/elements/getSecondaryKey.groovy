def skus = []
def marketString = out.productID?.attribute5

def market1 = marketString?.replaceAll(']', "")
def markets = market1?.substring(1)
List marketList = markets?.tokenize(", ")

def defaultOptions = api.find("PX20", Filter.equal("name", "DefaultOptions"),
        Filter.equal("sku", out.productID?.sku))

List optionsIterate = defaultOptions.findAll { it.attribute3 == "dimensions" && it.attribute5 == "A/B" }
List defaultMaterial = defaultOptions.findAll { it.attribute3 == "external" && it.attribute5.contains("material") }?.attribute7

def rawMaterialdata = api.stream("PX20", null, Filter.equal("name", "RawMaterialPrice"))
def rawMaterialIterator = rawMaterialdata?.collect()
rawMaterialdata?.close()

rawMaterialIterator.each { row ->

    if (defaultMaterial.contains(row.sku)) {
        optionsIterate.each { it ->

            for (ml in marketList) {

                def SecondaryKey = ml + "|" + it.attribute7 + "|" + row.sku + "|" + row.attribute1 + "|" + row.attribute3 + "|" + row.attribute4
                skus?.add(SecondaryKey)

            }
        }
    }
}
return skus